<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Anton|Audiowide|Montserrat&display=swap" rel="stylesheet">
<div id="wrapper">
	<?php echo do_shortcode('[rev_slider alias="404-error-tesla-starman"][/rev_slider]'); ?>
</div>
<?php get_footer(); ?>
