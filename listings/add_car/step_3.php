<?php

if (!empty($id)) {
	$item_id = $id;
}
wp_localize_script('add-listing', 'imageUploadSettings', [
	'size' => $max_file_size = apply_filters('stm_listing_media_upload_size', 1024 * 4000),
	'images' => gallery_images($id)
]);
wp_enqueue_script('add-listing');

?>

<div class="stm-form-3-photos clearfix">
    <div class="stm-car-listing-data-single stm-border-top-unit ">
        <div class="title heading-font"><?php esc_html_e('Upload Photos', 'motors'); ?></div>
        <span class="step_number step_number_3 heading-font"><?php esc_html_e('step', 'motors'); ?> 3</span>
    </div>

    <div class="row">

		<div id="image_uploader" class="image_uploader" v-cloak>
			<div class="container">
				<div style="display: flex; align-items: center; justify-content: space-between; flex-wrap: wrap;">
					<h5 class="mb-1" style="font-size: 16px;">{{ imageCount }}</h5>
					<h6 v-show="previewsLength" id="delete-all" class="delete-all mb-1" >Delete All</h6>
				</div>
			</div>
			<input type="file" multiple accept="image/*"
				 ref="inputUploader"
				 @change="handleSelects" name="images">
			<div class="col-xs-12 col-sm-12 col-md-5 main-photo" style="padding-right: 0;">
				<div class="image_uploader__main-photo" style="flex-direction: column;">
					<span v-if="featuredImage && featuredImage.preload" style="flex-direction: column;display: flex; align-items: center; justify-content: center; width: 100%; padding: 0 20px;">
						<div class="progress-bar-wrapper" style="width: 100%;">
								<div class="text-center" style="font-size: 25px; display:block;">
									{{ featuredImage.progress + '%' }}
								</div>
								<p :style="{ width: featuredImage.progress + '%' }"
									style="width: 0; display: block; visibility: visible; margin-top: 20px; height: 10px;"
									class="progress-bar">
								</p>
							</div>
							<div class="main-photo__text" style="margin-top: 20px; font-size: 30px">Loading Image</div>
					</span>
					<div v-else-if="featuredImage && featuredImage.src">
						<img :src="featuredImage.src">
					</div>
					<div v-else>
						<button type="button" @click.prevent="clickHandle()">
							Add Photos
						</button>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-7 listing-thumbnails">
				<div class="image_uploader__previews">
					<draggable v-model="previews"
						:move="onMove"
						filter=".image_uploader__item-preview"
			 			:disabled="isDraggable">
	  					<transition-group>
							<div v-for="(preview, index) in previews"
								:key="index"
								:class=" preview.src ? '' : 'image_uploader__item-preview'"
								class="image">
								<v-touch v-if="preview.src"
									 v-on:tap="onTap(preview)"
									 :class="preview.index === featuredIndex ? 'active' : '' "
									 class="image_wrapper">
										<img :src="preview.thumb">
										<div v-if="index === 0" class="image__label" style="opacity: 1; visibility: visible">
											Featured Photo
										</div>
										<div v-else class="image__label">Drag to reorder</div>
										<v-touch v-on:tap="deleteSingle(index)" class="preview__remove">
										  <i class="fa fa-times" aria-hidden="true"></i>
										</v-touch>
								</v-touch>
			  					<div v-else>
									<div v-if="index === uploadBtnPos" @click.prevent="clickHandle(index)">
										<div class="gallery-uploader text-center">
											<i class="fa fa-plus" aria-hidden="true"></i>
											<div class="gallery-uploader__text">Upload Photos</div>
										</div>
									</div>
									<div v-else-if="preview.preload" >
										<v-touch
											v-on:tap="onTap(preview)"
											:class=" preview.index === featuredIndex ? 'active' : '' "
											class="image-preloader">
											<div class="progress-bar-wrapper">
												<div class="text-center" style="display:block;">{{ preview.progress + '%' }}</div>
												<p
													:style="{ width: preview.progress + '%' }"
													style="width: 0"
													class="progress-bar"
												>
												</p>
											</div>
											<div v-if="preview.progress !== 100" class="image-preloader__text">Loading</div>
											<div v-if="preview.progress !== 100" class="image-preloader__text">Image...</div>

											<div v-if="preview.progress == 100" class="image-preloader__text">Compressing</div>
											<div v-if="preview.progress == 100" class="image-preloader__text">Image...</div>
										</v-touch>
									</div>
									<div v-else class="stm-service-icon-photos"></div>
			  					</div>
							</div>

						</transition-group>
					</draggable>
				</div>
			</div>

		</div>

    </div>

</div>

