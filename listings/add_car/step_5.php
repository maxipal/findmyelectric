<?php
$note = '';
if (!empty($id)) {
    $note = get_post_field('post_content', $id);
		$note = $note !== 'N/A' ? $note : '';

    $note_title = array(
        '<',
        '>',
        'div class="stm-car-listing-data-single stm-border-top-unit"',
        'div class="title heading-font"',
        esc_html__('Vehicle Description', 'motors'),
        '/div',
    );
}
wp_enqueue_script('tinymce-cdn');
wp_enqueue_script('tinymce');
?>

<div class="stm-form-5-notes clearfix">
    <div class="stm-car-listing-data-single stm-border-top-unit ">
        <div class="title heading-font"><?php esc_html_e('Vehicle Description', 'motors'); ?></div>
        <span class="step_number step_number_5 heading-font"><?php esc_html_e('step', 'motors'); ?> 4</span>
    </div>
    <div class="row stm-relative">
        <div class="col-md-9 col-sm-9 stm-non-relative">
            <div class="stm-phrases-unit">
							<div class="tinymce" id="tinymce" >
									<?php echo $note ? $note: '' ?>
							</div>
            </div>
        </div>
        <?php if (!empty($stm_phrases)): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">

                <div class="stm-seller-notes-phrases heading-font">
                    <span><?php esc_html_e('Add the Template Phrases', 'motors'); ?></span></div>

            </div>
        <?php endif; ?>
    </div>
</div>
