<ais-hits
	:class-names="{
		'ais-Hits': 'row row-3 car-listing-row car-listing-modern-grid',
		'ais-Hits-list': 'stm-isotope-sorting',
		'ais-Hits-item': 'col-md-4 col-sm-4 col-xs-12 col-xxs-12 stm-isotope-listing-item all',
  }">
<template slot="item" slot-scope="{ item }">
	<a :href="item.url" class="rmv_txt_drctn">
		<div class="gallery-carousel" :class="{'wrap_sold': item.sold}">
			<div class="image">
				<div v-if="item.galleryUrls.length && !item.sold" class="image-carousel" v-carousel>
					<div v-for="(image, index) in item.galleryUrls" :key="index" class="image-item" >
						<a :href="item.url" class="rmv_txt_drctn">
							<div class="image-inner">
								<div v-if="item.sold" class="stm-badge-directory heading-font ">SOLD</div>
								<div v-if="image" :class="{'sold': item.sold}">
									<img :src="image" class="img-responsive" alt="Placeholder"/>
								</div>
								<div v-else :class="{'sold': item.sold}">
									<img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/plchldr255.png'); ?>" class="img-responsive" alt="Placeholder"/>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div v-else>
					<div class="image">
						<a :href="item.url" class="rmv_txt_drctn">
							<div class="image-inner">
								<div v-if="item.sold" class="stm-badge-directory heading-font ">SOLD</div>
								<div v-if="item.thumbnail" :class="{'sold': item.sold}">
									<img :src="item.thumbnail" class="img-responsive" alt="Placeholder"/>
								</div>
								<div v-else :class="{'sold': item.sold}">
									<img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/plchldr255.png'); ?>" class="img-responsive" alt="Placeholder"/>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="hide_on_mobile">
			<div class="image">
				<a :href="item.url" class="rmv_txt_drctn">
					<div class="image-inner">
						<div v-if="item.sold" class="stm-badge-directory heading-font ">SOLD</div>
						<div v-if="item.thumbnail" :class="{'sold': item.sold}">
							<img :src="item.thumbnail" class="img-responsive" alt="Placeholder"/>
						</div>
						<div v-else :class="{'sold': item.sold}">
							<img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/plchldr255.png'); ?>" class="img-responsive" alt="Placeholder"/>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="listing-car-item-meta" :class="{'featured' : item.featured}">
			<div class="car-meta-top heading-font clearfix">
				<div v-if="item.price" class="price">
					<div class="normal-price">$ {{item.price | commaSeparator}}</div>
				</div>
				<div class="car-title">
					<ais-highlight :hit="item" attribute="title"/>
				</div>
			</div>
			<div class="car-meta-bottom">
				<div class="car-meta-bottom">
					<ul>
						<li v-if="item.mileage">
							<i class="stm-icon-road"></i>
							<span>{{item.mileage | commaSeparator}}</span>
						</li>
						<li v-if="item.extColor">
							<i class="fa fa-eyedropper"></i>
							<span>{{item.extColor}}</span>
						</li>
						<li v-if="item.state">
							<i class="fa fa-map-marker"></i>
							<span>{{item.state_two}}</span>
						</li>
						<!-- <li v-if="item.for_faceting.sellerType">
							<i class="stm-service-icon-user-2"></i>
							<span>{{item.for_faceting.sellerType}}</span>
						</li> -->
					</ul>
				</div>
			</div>
		</div>
		<h1></h1>
	</a>
</template>
</ais-hits>
