<?php

$post_id = get_the_id();
$user_added_by = get_post_meta($post_id, 'stm_car_user', true);
$showNumber = get_theme_mod("stm_show_number", false);
$car_location = get_post_meta($post_id, 'stm_car_location', true);
$user_phone = get_user_meta($user->ID, 'stm_phone', true);
$show_user_phone = get_post_meta($post_id, 'show_user_phone', true);
$show_user_name = get_post_meta($post_id, 'show_user_name', true);
$texts_ok = get_post_meta($post_id, 'texts_ok', true);
$calls_ok = get_post_meta($post_id, 'calls_ok', true);

$asSold = get_post_meta(get_the_ID(), 'car_mark_as_sold', true);
$subscription_plan_free = get_theme_mod('free_plan', 9819);
$stm_user_active_subscriptions = stm_user_active_subscriptions(false, $user_added_by);
$plan_id = $stm_user_active_subscriptions["product_id"];

$seller_type = get_post_meta($post_id, 'seller-type', true); //check for seller type

?>

<div id="added-user-info">
	<?php if (!empty($user_added_by)):

		$user_data = get_userdata($user_added_by);

		if ($user_data):

			$user_fields = stm_get_user_custom_fields($user_added_by);
			$is_dealer = stm_get_user_role($user_added_by);

			if ($is_dealer):
				$ratings = stm_get_dealer_marks($user_added_by);
				?>

				<div class="stm-listing-car-dealer-info">
				<a
					class="stm-no-text-decoration"
					href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>">
					<h3 class="title"><?php stm_display_user_name($user_added_by); ?></h3>
				</a>
				<div class="clearfix">
					<div class="dealer-image">
						<div class="stm-dealer-image-custom-view">
							<a href="<?php echo esc_url(stm_get_author_link($user_added_by)); ?>">
								<?php if (!empty($user_fields['logo'])): ?>
									<img src="<?php echo esc_url($user_fields['logo']); ?>"/>
								<?php else: ?>
									<img src="<?php stm_get_dealer_logo_placeholder(); ?>"/>
								<?php endif; ?>
							</a>
						</div>
					</div>
					<?php if (!empty($ratings['average'])): ?>
						<div class="dealer-rating">
							<div class="stm-rate-unit">
								<div class="stm-rate-inner">
									<div class="stm-rate-not-filled"></div>
									<div class="stm-rate-filled" style="width:<?php echo esc_attr($ratings['average_width']); ?>"></div>
								</div>
							</div>
							<div class="stm-rate-sum">(<?php esc_html_e('Reviews',
									'motors'); ?> <?php echo esc_attr($ratings['count']); ?>)</div>
						</div>
					<?php endif; ?>
				</div>

              <div class="dealer-contacts">
                  <?php if (!empty($user_fields['phone']) && $show_user_phone): ?>
					  <div class="dealer-contact-unit phone"
						   <?php if (!stm_is_standard($plan_id)	&& !stm_is_featured($plan_id)){ ?>
							   style="display:none !important"
						   <?php } ?>>
                        <?php if ($texts_ok || $calls_ok): ?>
							<div class="call-or-text">
                              <?php if ($texts_ok && $calls_ok) : ?>
								  <span class="wrapper">
                                  <a href="" class="calls_ok hide">Call</a>
                                  <span class="span calls_ok inactive">Call</span>
                                    <span>or</span>
                                  <span class="span texts_ok inactive">Text</span>
                                  <a href="" class="texts_ok hide">Text</a>
                                </span>
							  <?php elseif ($texts_ok) : ?>
								  <a class="calls_ok hide" href="">Call</a>
								  <span class="span calls_ok inactive" href="">Call</span>
							  <?php elseif ($calls_ok) : ?>
								  <a class="span texts_ok hide" href="">Text</a>
								  <span class="span texts_ok inactive" href="">Text</span>
							  <?php endif; ?>
                          </div>
						<?php endif; ?>

						  <div style="display: flex; justify-content: space-between; width: 100%;">
                        <i class="stm-service-icon-phone_2 <?php echo ($texts_ok || $calls_ok)
							? 'text_call_ok' : '' ?>"></i>
                        <div class="phone heading-font"><?php echo substr_replace($user_fields['phone'],
								"*****", 5, strlen($user_fields['phone'])); ?></div>
                        <span class="stm-show-number" data-id="<?php echo esc_attr($user_fields['user_id']); ?>">
							<?php echo esc_html__("Show number","motors"); ?></span>
                      </div>
                    </div>
				  <?php endif; ?>
				  <?php if ($car_location): ?>
					  <div class="dealer-contact-unit mail">
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                      <div class="stm-label"><?php esc_html_e('Seller Location', 'motors'); ?></div>
                      <div class="address"><?php echo substr($car_location, 0, -5); ?></div>
                    </div>
				  <?php endif; ?>
              </div>
			</div>

			<?php else: ?>
				<div class="stm-listing-car-dealer-info stm-common-user">

				<div class="clearfix stm-user-main-info-c">
					<div class="user-icon">
						<i class="stm-service-icon-user"
							<?php if (!empty($asSold)) echo 'style="visibility:hidden"' ?>></i>
					</div>

					<div class="user-name">
						<?php if (!empty($asSold)): ?>
							<h3 class="title"><?php echo esc_html__('Sold', 'motors'); ?></h3>
						<?php else: ?>
							<?php if ($show_user_name) : ?>
								<h3 class="title"><?php echo display_user_full_name($user_data); ?></h3>

								<?php if ($seller_type == "dealer") : ?>
								<div class="stm-label"><?php esc_html_e('Dealer',
										'motors'); ?></div>
								<?php else: ?>
								<div class="stm-label"><?php esc_html_e('Private Seller',
										'motors'); ?></div>
								<?php endif; ?>
							<?php else: ?>
									<?php if ($seller_type == "dealer") : ?>
									<h3 class="title"><?php esc_html_e('Dealer',
										'motors'); ?></h3>
							<?php else: ?>
								<h3 class="title"><?php esc_html_e('Private Seller',
										'motors'); ?></h3>

								<?php endif; ?>
							<?php endif; ?>
						<?php endif ?>
					</div>
				</div>

				<div class="dealer-contacts">
			  		<?php if (empty($asSold)): ?>
						<?php if (!empty($user_fields['phone']) && $show_user_phone): ?>
							<div class="dealer-contact-unit phone"
								 <?php if (!stm_is_standard($plan_id) && !stm_is_featured($plan_id)){ ?>
									 style="display:none !important"
								 <?php } ?>>
								<?php if ($texts_ok || $calls_ok): ?>
									<div class="call-or-text">
										<?php if ($texts_ok && $calls_ok) : ?>
											<span class="wrapper">
												<a href="" class="calls_ok hide">Call</a>
											<span class="span calls_ok inactive">Call</span>
											<span>or</span>
											<span class="span texts_ok inactive">Text</span>
												<a href="" class="texts_ok hide">Text</a>
											</span>
										<?php elseif ($texts_ok) : ?>
											<a class="calls_ok hide" href="">Call</a>
											<span class="span calls_ok inactive" href="">Call</span>
										<?php elseif ($calls_ok) : ?>
											<a class="span texts_ok hide" href="">Text</a>
											<span class="span texts_ok inactive" href="">Text</span>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<div style="display: flex; justify-content: space-between; width: 100%;">
									<i class="stm-service-icon-phone_2 <?php echo ($texts_ok
										|| $calls_ok) ? 'text_call_ok' : '' ?>"></i>
									<div class="phone heading-font"><?php echo substr_replace($user_fields['phone'],
											"*****", 5, strlen($user_fields['phone'])); ?></div>
									<span class="stm-show-number" data-id="<?php echo esc_attr($user_fields['user_id']); ?>"><?php echo esc_html__("Show number",
											"motors"); ?></span>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
					<?php if ($car_location): ?>
						<div class="dealer-contact-unit mail">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<div class="stm-label"><?php esc_html_e('Seller Location',
									'motors'); ?></div>
							<div class="address"><?php echo substr($car_location, 0, -5); ?></div>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
	<input type="hidden" id="recaptcha-token">
</div>
