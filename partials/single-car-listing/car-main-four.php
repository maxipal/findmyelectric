<div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">
        <div id="single-gallery">
			<div class="stm-single-car-content">
				<h1 class="title h2"><?php the_title(); ?></h1>

				<!--Actions-->
				<div id="actions">
					<?php get_template_part('partials/single-car/car', 'actions'); ?>
				</div>

				<div id="single-car-price">
					<?php get_template_part('partials/single-car/car', 'price') ?>
				</div>
				<!--Gallery-->
				<div id="main-gallery">
					<?php get_template_part('partials/single-car/car', 'gallery'); ?>
				</div>

				<?php if(!stm_is_disable_auto_check(get_the_ID())): ?>
					<?php $data = stm_get_auto_check_arr(get_the_ID());
					$score = get_post_meta(get_the_ID(), 'auto_check_score', true);
					 ?>
					<div id="report-data">
						<div class="stm-car-listing-data-single stm-border-top-unit">
							<div class="title heading-font"><?php esc_html_e('Vehicle History Report', 'motors'); ?></div>
						</div>
						<div class="stm-single-car-listing-data">
							<div class="row">
								<div class="col-sm-7">
									<table class="stm-table-main">
										<tbody>
											<tr>
												<td>
													<table class="inner-table">
														<tbody>
															<tr>
																<td class="label-td">
																	<?php _e("Owners","motors-child") ?>
																</td>
																<td class="heading-font">
																	<?php echo(!empty($data['owners']) ? $data['owners'] : '0') ?>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td>
													<table class="inner-table">
														<tbody>
															<tr>
																<td class="label-td">
																	<?php _e("Title","motors-child") ?>
																</td>
																<td class="heading-font">
																	<?php echo(!empty($data['title']) ? $data['title'] : '') ?>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table class="inner-table">
														<tbody>
															<tr>
																<td class="label-td">
																	<?php _e("Accidents","motors-child") ?>
																</td>
																<td class="heading-font">
																	<?php echo(!empty($data['accidents']) ? $data['accidents'] : '0') ?>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td>
													<table class="inner-table">
														<tbody>
															<tr>
																<td class="label-td">
																	<?php _e("VIN","motors-child") ?>
																</td>
																<td class="heading-font">
																	<?php echo(!empty($data['vin']) ? $data['vin'] : '-') ?>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<div class="wrap_report_links hidden-xs">
										<?php if(!empty($data['owners'])): ?>
											<a
												target="_blank"
												href="<?php echo add_query_arg(['get-report'=>1,'car-id'=> get_the_ID()], get_permalink()) ?>">
												<?php _e("View Your Complimentary AutoCheck Report", "motors-child") ?>
												<span class="fa fa-external-link-alt"></span>
											</a>
										<?php else: ?>
											<a disabled>
												<?php _e("This vehicle is too new to have AutoCheck history data", "motors-child") ?>
											</a>
										<?php endif ?>
									</div>
								</div>
								<div class="col-sm-5">
									<?php if(empty($score)): ?>
										<?php if(!empty($data['owners'])): ?>
											<a
												target="_blank"
												href="<?php echo add_query_arg(['get-report'=>1,'car-id'=> get_the_ID()], get_permalink()) ?>">
												<?php echo stm_auto_check_get(get_the_ID(), true) ?>
											</a>
										<?php else: ?>
											<?php echo stm_auto_check_get(get_the_ID(), true) ?>
										<?php endif ?>
									<?php else: ?>
										<?php if(!empty($data['owners'])): ?>
											<a
												target="_blank"
												href="<?php echo add_query_arg(['get-report'=>1,'car-id'=> get_the_ID()], get_permalink()) ?>">
												<?php echo $score ?>
											</a>
										<?php else: ?>
											<?php echo $score ?>
										<?php endif ?>
									<?php endif ?>
									<div class="wrap_report_links visible-xs">
										<?php if(!empty($data['owners'])): ?>
											<a
												target="_blank"
												href="<?php echo add_query_arg(['get-report'=>1,'car-id'=> get_the_ID()], get_permalink()) ?>">
												<?php _e("View Your Complimentary AutoCheck Report", "motors-child") ?>
												<span class="fa fa-external-link-alt"></span>
											</a>
										<?php else: ?>
											<a disabled>
												<?php _e("This vehicle is too new to have AutoCheck history data", "motors-child") ?>
											</a>
										<?php endif ?>
									</div>

								</div>
							</div>
						</div>
					</div>
				<?php endif ?>

				<div id="car-data">
					<?php if (strpos(get_theme_mod("carguru_style", "STYLE1"), "BANNER") !== false) {
						get_template_part('partials/single-car/car', 'gurus');
					} ?>

					<?php //CAR DATA
					$data = stm_get_single_car_listings();
					if (!empty($data)):
						?>
						<div class="stm-car-listing-data-single stm-border-top-unit">
							<div class="title heading-font"><?php esc_html_e('Vehicle Details', 'motors'); ?></div>
						</div>

						<?php get_template_part('partials/single-car-listing/car-data'); ?>
					<?php endif; ?>


					<?php
					$features = get_post_meta(get_the_id(), 'additional_features', true);
					if (!empty($features)):
						?>
						<div class="stm-car-listing-data-single stm-border-top-unit ">
							<div class="title heading-font"><?php esc_html_e('Features', 'motors'); ?></div>
						</div>
						<?php get_template_part('partials/single-car-listing/car-features'); ?>

					<?php endif; ?>

					<div class="stm-car-listing-data-single stm-border-top-unit">
						<div class="title heading-font"><?php echo esc_html__('Vehicle Description', 'motors'); ?></div>
					</div>
					<?php the_content(); ?>
				</div>

			</div>
			<div class="scrollToTop">
				<i class="fa fa-chevron-up" aria-hidden="true"></i>
			</div>
		</div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="stm-single-car-side">
            <?php
			if (is_active_sidebar('stm_listing_car')) {
				dynamic_sidebar('stm_listing_car');
			}
			else {
				/*<!--Prices-->*/
				get_template_part('partials/single-car/car', 'price');

				/*<!--Data-->*/
				get_template_part('partials/single-car/car', 'data');

				/*<!--Rating Review-->*/
				get_template_part('partials/single-car/car', 'review_rating');

				/*<!--MPG-->*/
				get_template_part('partials/single-car/car', 'mpg');

				/*<!--Calculator-->*/
				get_template_part('partials/single-car/car', 'calculator');
			}
			?>

        </div>
        <div id="actions-2">
            <?php get_template_part('partials/single-car/car', 'actions'); ?>
        </div>
    </div>
</div>
