<?php
require_once get_stylesheet_directory().'/inc/Mobile_Detect.php';
$detect = new Mobile_Detect;
$view_style = $args['view_style'];
$labels = $args['labels'];
$special_text = $args['special_text'];

?>
<div class="listing-car-item-meta">
	<div class="car-meta-top heading-font clearfix">
		<?php if($view_style == 'style_1') :
			$car_price_form_label = get_post_meta(get_the_ID(), 'car_price_form_label', true);
			$price = get_post_meta(get_the_id(),'price',true);
			$sale_price = get_post_meta(get_the_id(),'sale_price',true);
			if(!empty($car_price_form_label)): ?>
				<div class="price">
					<div class="normal-price"><?php echo esc_attr($car_price_form_label); ?></div>
				</div>
			<?php else: ?>
				<?php if(!empty($price) and !empty($sale_price)):?>
					<div class="price discounted-price">
						<div class="regular-price">
							<?php echo esc_attr(stm_listing_price_view($price)); ?>
						</div>
						<div class="sale-price">
							<?php echo esc_attr(stm_listing_price_view($sale_price)); ?>
						</div>
					</div>
				<?php elseif(!empty($price)): ?>
					<div class="price">
						<div class="normal-price">
							<?php echo esc_attr(stm_listing_price_view($price)); ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		<?php if($view_style == 'style_2'):
			$subtitle = '';
			$year = wp_get_post_terms(get_the_ID(), 'ca-year', array("fields" => "names"));
			$body = wp_get_post_terms(get_the_ID(), 'body', array("fields" => "names"));
			$subtitle .= ($year) ? '<span>' . $year[0] . '</span> ' : '';
			$subtitle .= ($body) ? '<span>' . $body[0] . '</span>' : '';
			?>
			<div class="car-subtitle heading-font">
				<?php echo stm_do_lmth($subtitle); ?>
			</div>
		<?php endif; ?>


		<div class="car-title">
			<?php echo esc_attr(trim(preg_replace( '/\s+/', ' ', get_the_title() ))); ?>
		</div>


	</div>


	<div class="car-meta-bottom">
		<?php if(!empty($labels)): ?>
			<?php if($detect->isMobile() && !$detect->isTablet()): ?>
				<div class="meta-middle">
					<?php $color = []; ?>
					<?php foreach($labels as $label): ?>
						<?php
						$label_meta = get_post_meta(get_the_id(),$label['slug'],true);
						$datas = stm_get_car_meta_datas($label);
						if ($label['slug'] == 'exterior-color') {
							$color = $label;
							continue;
						}
						?>
						<div class="meta-middle-unit font-exists <?php echo $label['slug'] ?>">
							<div class="meta-middle-unit-top">
								<div class="icon">
									<?php if(!empty($label['font'])): ?>
										<i class="<?php echo esc_attr($label['font']) ?>"></i>
									<?php endif; ?>
								</div>
								<div class="name"><?php echo $label['single_name'] ?></div>
							</div>
							<div class="value h5">
								<?php if(!empty($label['numeric']) and $label['numeric']): ?>
									<?php echo preg_replace(
										"/\B(?=(\d{3})+(?!\d))/",
										",",
										$label_meta); ?>
								<?php else: ?>
									<?php echo esc_attr(implode(', ', $datas)); ?>
								<?php endif ?>
							</div>
						</div>
					<?php endforeach; ?>

					<?php $location = get_post_meta(get_the_ID(), 'stm_state_car_two_letter_admin', true) ?>
					<?php if(!empty($location)): ?>
						<div class="meta-middle-unit font-exists state">
							<div class="meta-middle-unit-top">
								<div class="icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="name"><?php _e("Location", "motors") ?></div>
							</div>
							<div class="value h5"><?php echo $location ?></div>
						</div>
					<?php endif ?>

					<?php if(!empty($color)): ?>
						<?php
						$label_meta = get_post_meta(get_the_id(),$color['slug'],true);
						$datas = stm_get_car_meta_datas($label);
						?>
						<div class="meta-middle-unit font-exists state">
							<div class="meta-middle-unit-top">
								<div class="icon">
									<i class="<?php echo esc_attr($color['font']) ?>"></i>
								</div>
								<div class="name"><?php echo $color['single_name'] ?></div>
							</div>
							<div class="value h5"><?php echo esc_attr(implode(', ', $datas)); ?></div>
						</div>
					<?php else: ?>
						<ul>
							<?php $color = []; ?>
								<?php foreach($labels as $label): ?>
									<?php if($label['slug'] == 'exterior-color') {$color = $label;continue;} ?>
									<?php $label_meta = get_post_meta(get_the_id(),$label['slug'],true); ?>
									<?php if(!empty($label_meta)): ?>
										<li>
											<?php if(!empty($label['font'])): ?>
												<i class="<?php echo esc_attr($label['font']) ?>"></i>
											<?php endif; ?>

											<?php if(!empty($label['numeric']) and $label['numeric']): ?>
												<?php
												$val = preg_replace(
													"/\B(?=(\d{3})+(?!\d))/",
													",",
													$label_meta); ?>
												<span><?php echo esc_attr($val); ?></span>
											<?php else: ?>
												<?php
												$data_meta_array = explode(',',$label_meta);
												$datas = array();
												if(!empty($data_meta_array)){
													foreach($data_meta_array as $data_meta_single) {
														$data_meta = get_term_by('slug', $data_meta_single, $label['slug']);
														if(!empty($data_meta->name)) {
															$datas[] = esc_attr($data_meta->name);
														}
													}
												}
												?>

												<?php if(!empty($datas)):
													if(count($datas) > 1) { ?>
														<span
															class="stm-tooltip-link"
															data-toggle="tooltip"
															data-placement="top"
															title="<?php echo esc_attr(implode(', ', $datas)); ?>">
														<?php echo stm_do_lmth($datas[0]).'<span class="stm-dots dots-aligned">...</span>'; ?>
													</span>
													<?php } else { ?>
														<span><?php echo implode(', ', $datas); ?></span>
													<?php }
												endif; ?>

											<?php endif; ?>
										</li>
									<?php endif; ?>
								<?php endforeach; ?>
								<?php $location = get_post_meta(get_the_ID(), 'stm_state_car_two_letter_admin', true) ?>
								<?php if(!empty($location)): ?>
									<li>
									<i class="fa fa-map-marker"></i>
									<span>
										<?php echo $location ?>
									</span>
								</li>
								<?php endif ?>

								<?php if(!empty($color)): ?>
									<li>
									<i class="<?php echo esc_attr($color['font']) ?>"></i>
									<span>
										<?php var_dump($color); ?>
									</span>
								</li>
								<?php endif ?>
						</ul>
					<?php endif; ?>

				</div>
			<?php else: ?>
				<ul>
					<?php foreach($labels as $label): ?>
						<?php if ($label['slug'] == 'exterior-color') {$color = $label;continue;} ?>
						<?php $label_meta = get_post_meta(get_the_id(),$label['slug'],true); ?>
						<?php if(!empty($label_meta)): ?>
							<li>
								<?php if(!empty($label['font'])): ?>
									<i class="<?php echo esc_attr($label['font']) ?>"></i>
								<?php endif; ?>

								<?php if(!empty($label['numeric']) and $label['numeric']): ?>
									<?php $val = preg_replace(
										"/\B(?=(\d{3})+(?!\d))/",
										",",
										$label_meta); ?>
									<span><?php echo esc_attr($val); ?></span>
								<?php else: ?>
									<?php
									$data_meta_array = explode(',',$label_meta);
									$datas = array();

									if(!empty($data_meta_array)){
										foreach($data_meta_array as $data_meta_single) {
											$data_meta = get_term_by('slug', $data_meta_single, $label['slug']);
											if(!empty($data_meta->name)) {
												$datas[] = esc_attr($data_meta->name);
											}
										}
									}
									?>

									<?php if(!empty($datas)):
										if(count($datas) > 1) { ?>

											<span
												class="stm-tooltip-link"
												data-toggle="tooltip"
												data-placement="top"
												title="<?php echo esc_attr(implode(', ', $datas)); ?>">
												<?php echo stm_do_lmth($datas[0]).'<span class="stm-dots dots-aligned">...</span>'; ?>
											</span>

										<?php } else { ?>
											<span><?php echo implode(', ', $datas); ?></span>
										<?php }
									endif; ?>

								<?php endif; ?>
							</li>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php $location = get_post_meta(get_the_ID(), 'stm_state_car_two_letter_admin', true) ?>
					<?php if(!empty($location)): ?>
						<li>
							<i class="fa fa-map-marker"></i>
							<span><?php echo $location ?></span>
						</li>
					<?php endif ?>
					<?php if(!empty($color)): ?>
						<?php
						$label_meta = get_post_meta(get_the_id(),$color['slug'],true);
						$datas = stm_get_car_meta_datas($color);
						?>
						<li class="exterior-color">
							<?php if(!empty($color['font'])): ?>
								<i class="<?php echo esc_attr($color['font']) ?>"></i>
							<?php endif; ?>
							<span><?php echo esc_attr(implode(', ', $datas)); ?></span>
						</li>
					<?php endif; ?>
				</ul>
			<?php endif ?>
		<?php else: ?>
			<ul>
				<li>
					<div class="special-text"><?php echo esc_attr($special_text); ?></div>
				</li>
			</ul>
		<?php endif; ?>
	</div>



</div>
