<?php
$listing_id = get_the_ID();
$badge_text = get_post_meta($listing_id,'badge_text',true);
$badge_bg_color = get_post_meta($listing_id,'badge_bg_color',true);
$special_car = get_post_meta($listing_id,'special_car', true);
$taxonomies = stm_get_taxonomies();
$categories = wp_get_post_terms($listing_id, array_values($taxonomies));
$classes = array( 'listing-list-loop-edit', get_post_status( $listing_id ) );
foreach ($categories as $category) {
    $classes[] = $category->slug . '-' . $category->term_id;
}
$car_media = stm_get_car_medias($listing_id);
$show_compare = get_theme_mod('show_listing_compare', true);
$show_favorite = get_theme_mod('enable_favorite_items', true);
$hide_labels = get_theme_mod('hide_price_labels', true);

if ($hide_labels) {
    $classes[] = 'stm-listing-no-price-labels';
}

$car_views = get_post_meta($listing_id, 'stm_custom_car_views', true);
if (empty($car_views)) {
    $car_views = 0;
}

$asSold = get_post_meta($listing_id, 'car_mark_as_sold', true);
if (!empty($asSold)) {
    $classes[] = 'as_sold';
}

$is_free = $is_standard = $is_featured = $no_bought = false;
$user_id = get_current_user_id();
//$subscription = get_post_meta($listing_id, 'listing_plan', true);
if(!$subscription){
	if(function_exists('stm_get_plan_listing')){
		$subscription = stm_get_plan_listing(get_the_ID());
	}
}

$s_plan_free = get_theme_mod('free_plan', 9819 );
if(!empty($subscription['product_id'])){
	if($s_plan_free == (int)$subscription['product_id']) $is_free = true;
	if(stm_is_standard($subscription['product_id'])) $is_standard = true;
	if(stm_is_featured($subscription['product_id'])) $is_featured = true;
}else{
	$no_bought = true;
}

$count_photo = $car_media['car_photos_count'];

if(function_exists('all_packages_page')){
	$packages_page = get_home_url() ."/".all_packages_page() . "/";
}
$status = get_post_meta($listing_id, 'status', true);

if($is_free){
	$standard_page = (int)get_theme_mod('free_to_standard', 9819 );
	$upgrade_url = esc_url(add_query_arg(array('lid' => $listing_id, 'upgrade' => $listing_id), get_permalink($standard_page)));
}else{
	$upgrade_url = esc_url(add_query_arg(array('lid' => $listing_id, 'upgrade' => $listing_id), $packages_page));
}

stm_listings_load_template('loop/start', array('modern' => true, 'listing_classes' => $classes));
?>
<div class="image">
    <!--Hover blocks-->
    <!---Media-->
    <div class="stm-car-medias">
        <?php if(!empty($car_media['car_photos_count'])): ?>
            <div class="stm-listing-photos-unit stm-car-photos-<?php echo get_the_id(); ?>">
                <i class="stm-service-icon-photo"></i>
                <span>
					<?php if($is_free || $no_bought) {
						if ($count_photo >= 3) { ?>
							3
						<?php } else {
							echo $count_photo;
						} ?>
					<?php } elseif ($is_standard) {
						if ($count_photo >= 15) { ?>
							15
						<?php } else {
							echo $count_photo;
						} ?>
					<?php } else {
						echo $count_photo;
					} ?>
				</span>
            </div>

            <script>
                jQuery(document).ready(function(){

                    jQuery(".stm-car-photos-<?php echo get_the_id(); ?>").on('click', function() {
                        jQuery(this).lightGallery({
                            dynamic: true,
                            dynamicEl: [
                                <?php
								if ($is_free || $no_bought) {
                                	$i=0;
                                	foreach($car_media['car_photos'] as $car_photo):
										$i++; if($i==4) break; ?>
										{
											src  : "<?php echo esc_url($car_photo); ?>"
										},
                                	<?php endforeach; ?>
                                <?php } elseif ($is_standard) {
                                	$i=0;
                                	foreach($car_media['car_photos'] as $car_photo):
										$i++; if($i==16) break; ?>
										{
											src  : "<?php echo esc_url($car_photo); ?>"
										},
                                	<?php endforeach; ?>
                                <?php } else {
									$i=0;
									foreach($car_media['car_photos'] as $car_photo):
										$i++; if($i==21) break; ?>
										{
											src  : "<?php echo esc_url($car_photo); ?>"
										},
									<?php endforeach; ?>
                                <?php } ?>
                            ],
                            download: false,
                            mode: 'lg-fade',
                        })
                    });
                });

            </script>
        <?php endif; ?>
        <?php if(!empty($car_media['car_videos_count'])): ?>
            <div class="stm-listing-videos-unit stm-car-videos-<?php echo get_the_id(); ?>">
                <i class="fa fa-film"></i>
                <span><?php echo esc_html($car_media['car_videos_count']); ?></span>
            </div>

            <script>
                jQuery(document).ready(function(){

                    jQuery(".stm-car-videos-<?php echo get_the_id(); ?>").on('click', function() {
                        jQuery(this).lightGallery({
                            dynamic: true,
                            dynamicEl: [
                                <?php foreach($car_media['car_videos'] as $car_video): ?>
                                {
                                    src  : "<?php echo esc_url($car_video); ?>"
                                },
                                <?php endforeach; ?>
                            ],
                            download: false,
                            mode: 'lg-fade',
                        })
                    }); //click
                }); //ready

            </script>
        <?php endif; ?>
    </div>
    <!--Compare-->
    <?php if(!empty($show_compare) and $show_compare): ?>
        <div class="stm-listing-compare"
			 data-id="<?php echo esc_attr(get_the_id()); ?>"
			 data-title="<?php echo stm_generate_title_from_slugs(get_the_id(),false); ?>">
            <i class="stm-service-icon-compare-new"></i>
        </div>
    <?php endif; ?>

    <!--Favorite-->
    <?php if(!empty($show_favorite) and $show_favorite): ?>
        <div class="stm-listing-favorite" data-id="<?php echo esc_attr(get_the_id()); ?>">
            <i class="stm-service-icon-staricon"></i>
        </div>
    <?php endif; ?>

    <div class="stm-car-views" <?php if ( $is_free || $no_bought ) echo 'style="display:none !important"' ?>>
        <i class="fa fa-eye"></i>
        <?php echo esc_attr($car_views); ?>
    </div>

    <?php if(get_post_status(get_the_id()) == 'draft'): ?>
        <!--<div class="stm_car_move_to_trash">
			<a
				class="stm-delete-confirmation"
				href="<?php echo esc_url(add_query_arg(array('stm_move_trash_car' => get_the_ID()), stm_get_author_link(''))); ?>"
				data-title="<?php the_title(); ?>">
				<i class="fa fa-trash-o"></i>
			</a>
		</div>-->
    <?php endif; ?>

    <a href="<?php the_permalink() ?>" class="rmv_txt_drctn">
        <div class="image-inner">
            <?php if(has_post_thumbnail()): ?>
                <?php
                $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'stm-img-796-466');
                ?>
				<?php if(isset($_REQUEST['loca'])){
					$attachment_id = get_post_thumbnail_id(get_the_ID());
					$size = 'stm-img-796-466';
					$image = image_downsize( $attachment_id, $size );
					var_dump($attachment_id);
					var_dump($image);
					var_dump(wp_get_attachment_image_src($attachment_id, $size));
					var_dump(stm_get_thumbnail($attachment_id, $size));
					var_dump(image_get_intermediate_size( $attachment_id, $size ));
					//var_dump(get_post_meta($attach_id));
				} ?>
				<img
					data-original="<?php echo esc_url($img[0]); ?>"
					src="<?php echo esc_url($img[0]); ?>"
					class="lazy img-responsive"
					alt="<?php the_title(); ?>"
                />

            <?php else : ?>
                <img
					src="<?php echo esc_url(get_stylesheet_directory_uri().'/assets/images/plchldr350.png'); ?>"
					class="img-responsive"
					alt="<?php esc_attr_e('Placeholder', 'motors'); ?>"
                />
            <?php endif; ?>
        </div>
    </a>
</div>


<div class="content">

    <?php stm_listings_load_template('loop/classified/list/title_price', array('hide_labels' => $hide_labels)) ?>

    <?php stm_listings_load_template('loop/classified/list/options', array('subscription' => $subscription)); ?>

    <div class="meta-middle">
        <?php get_template_part('partials/listing-cars/listing-directive-list-loop', 'actions'); ?>
    </div>

    <?php if( $status === 'not_paid' ):?>
        <div class="stm_edit_pending_car" style="top: 30%;">
            <h4><?php esc_html_e('Listing Process Incomplete', 'stm_vehicles_listing'); ?></h4>
            <a href="<?php if(function_exists('stm_get_add_edit_page_url')) echo esc_url(stm_get_add_edit_page_url('edit', $id)); ?>">
                <?php esc_html_e('Edit Listing', 'stm_vehicles_listing'); ?>
            </a>
            <a  id="pay_now_bk"
                style="margin-top: 10px"
                href="<?php echo $upgrade_url; ?>"
                data-title="<?php the_title(); ?>"
                data-car-id="<?php the_ID();?>"
            >
                <?php esc_html_e('Choose a Package', 'stm_vehicles_listing'); ?>
            </a>
        </div>
    <?php elseif(get_post_status(get_the_id()) != 'pending'): ?>
        <div class="stm_edit_disable_car heading-font">
			<?php if($asSold == 'on'): ?>
			<div>
				<a
					href="<?php echo esc_url(add_query_arg(array('stm_unmark_as_sold_car' => get_the_ID()), stm_get_author_link(''))); ?>"
					class="as_sold">
					<?php esc_html_e('Unmark as Sold', 'motors'); ?>
					<i class="fa fa-check-square-o" aria-hidden="true"></i>
				</a>
			</div>
			<?php else: ?>
				<div>
					<a href="<?php echo esc_url(add_query_arg(array('stm_mark_as_sold_car' => get_the_ID()), stm_get_author_link(''))); ?>">
						<?php esc_html_e('Mark as Sold', 'motors'); ?>
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
					</a>
				</div>
			<?php endif; ?>

            <?php if(get_theme_mod('dealer_payments_for_featured_listing', false)) :?>
                <div>
                    <?php
                    $featuredStatus = get_post_meta(get_the_ID(), 'car_make_featured_status', true);
                    if(!$special_car && (empty($featuredStatus) || $featuredStatus == 'in_cart')) :
                        ?>
                        <a href="<?php echo esc_url(add_query_arg(array('stm_make_featured' => get_the_ID()), stm_get_author_link(''))); ?>" class="make_featured">
                            <?php esc_html_e('Make Featured', 'motors'); ?>
							<i class="fa fa-star" aria-hidden="true"></i>
                        </a>
                    <?php else :
                        $featuredText = (($special_car && ($featuredStatus == 'completed' || $featuredStatus == 'processing')) || $special_car && empty($featuredClass)) ? 'Featured' : 'Featured (pending)';
                        $featuredClass = (($special_car && ($featuredStatus == 'completed' || $featuredStatus == 'processing')) || $special_car && empty($featuredClass)) ? 'featured' : 'featured_pending';
                        ?>
                        <span class="<?php echo esc_attr($featuredClass); ?>">
							<?php stm_dynamic_string_translation_e('Featured Text', $featuredText); ?>
							<i class="fa fa-star" aria-hidden="true"></i>
						</span>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div>
				<a href="<?php if(function_exists('stm_get_add_edit_page_url')) echo esc_url(stm_get_add_edit_page_url('edit', get_the_ID())); ?>">
					<?php esc_html_e('Edit Listing', 'motors'); ?><i class="fa fa-pencil"></i>
				</a>
			</div>
            <?php if(get_post_status(get_the_id()) == 'draft'): ?>
				<div>
					<a href="<?php echo esc_url(add_query_arg(array('stm_enable_user_car' => get_the_ID()), stm_get_author_link(''))); ?>"
					   class="enable_list"><?php esc_html_e('Restart Listing', 'motors'); ?><i class="fa fa-play"></i></a>
				</div>
            <?php else: ?>
				<div>
					<a href="<?php echo esc_url(add_query_arg(array('stm_disable_user_car' => get_the_ID()), stm_get_author_link(''))); ?>"
					class="disable_list" data-id="<?php esc_attr(get_the_ID()); ?>"><?php esc_html_e('Pause Listing', 'motors'); ?><i class="fa fa-pause"></i></a>
				</div>
            <?php endif; ?>
			<?php if( !$is_featured ): ?>
				<div>
					<a href="<?php echo $upgrade_url; ?>"
					   class="upgrade_plan"
					   data-id="<?php esc_attr(get_the_ID()); ?>"><?php esc_html_e('Upgrade Listing', 'motors'); ?>
						<i class="fa fa-shopping-cart"></i>
					</a>
				</div>
			<?php endif ?>

        </div>
    <?php elseif($status === 'expired'): ?>
        <div class="stm_edit_pending_car stm_edit_expired_car">
            <h4><?php esc_html_e('Expired', 'motors'); ?></h4>
            <div class="stm-dots"><span></span><span></span><span></span></div>
			<a href="<?php echo $upgrade_url; ?>"
			   class="upgrade_plan"
			   data-id="<?php esc_attr(get_the_ID()); ?>"><?php esc_html_e('Upgrade Listing', 'motors'); ?>
				<i class="fa fa-shopping-cart"></i>
			</a>
			<?php if(false): ?>
				<a href="<?php if(function_exists('stm_get_add_edit_page_url')) echo esc_url(stm_get_add_edit_page_url('edit', get_the_ID())); ?>">
					<?php esc_html_e('Edit Listing', 'motors'); ?>
					<i class="fa fa-pencil"></i>
				</a>
				<!--<a class="stm-delete-confirmation" href="<?php echo esc_url(add_query_arg(array('stm_move_trash_car' => get_the_ID()), stm_get_author_link(''))); ?>" data-title="<?php the_title(); ?>">
					<?php esc_html_e('Delete', 'motors'); ?>
					<i class="fa fa-trash-o"></i>
				</a>-->
			<?php endif ?>
        </div>
    <?php else: ?>
        <div class="stm_edit_pending_car">
            <h4><?php esc_html_e('Pending', 'motors'); ?></h4>
            <div class="stm-dots"><span></span><span></span><span></span></div>
            <a href="<?php if(function_exists('stm_get_add_edit_page_url')) echo esc_url(stm_get_add_edit_page_url('edit', get_the_ID())); ?>">
                <?php esc_html_e('Edit Listing', 'motors'); ?>
                <i class="fa fa-pencil"></i>
            </a>
            <!--<a class="stm-delete-confirmation" href="<?php echo esc_url(add_query_arg(array('stm_move_trash_car' => get_the_ID()), stm_get_author_link(''))); ?>" data-title="<?php the_title(); ?>">
                <?php esc_html_e('Delete', 'motors'); ?>
                <i class="fa fa-trash-o"></i>
            </a>-->
        </div>
    <?php endif; ?>

	<?php if((get_post_status(get_the_id()) == 'pending') && $status !== 'expired' && $status !== 'not_paid'): ?>
		<div class="meta-middle">
			<div class="alert-box alert-box-expired notice">
				<span><?php _e("Listing Pending", "motors") ?>: </span>
				<?php _e("To ensure listing consistency and accuracy for our users, all listings are manually reviewed and published by our team — typically within 24 hours.", "motors") ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if($status == 'expired'): ?>
		<div class="meta-middle">
			<div class="alert-box alert-box-expired notice">
				<span><?php _e("Notice", "motors") ?>: </span>
				<?php _e("This listing has expired. To reactivate it, click the \"Upgrade Listing\" button above.", "motors") ?>
			</div>
    	</div>
	<?php endif ?>

	<?php if( $status === 'not_paid' ):?>
	<div class="meta-middle">
			<div class="alert-box alert-box-expired notice">
				<span><?php _e("Notice", "motors") ?>: </span>
				<?php _e("It looks like you didn't complete the listing process. Click \"Edit Listing\" above to make changes to your ad, or click \"Choose a Package\" to finish the listing process.", "motors") ?>
			</div>
    	</div>
	<?php endif ?>

</div>

</div>
