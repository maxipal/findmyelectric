<?php $car_photos = stm_get_car_medias(get_the_id())['car_photos'];

$show_compare = get_theme_mod('show_listing_compare', true);

$show_favorite = get_theme_mod('enable_favorite_items', true);

$gallery = get_post_meta(get_the_id(), 'gallery', true);

$asSold = get_post_meta(get_the_ID(), 'car_mark_as_sold', true);
if (!empty($asSold)) $gallery = array_slice($gallery, 0, 1);

$dynamicClassPhoto = 'stm-car-photos-' . get_the_id() . '-' . rand();
$dynamicClassVideo = 'stm-car-videos-' . get_the_id() . '-' . rand();
?>
<div class="image">

    <!--Video-->
    <div class="stm-car-medias">
        <?php if (!empty($car_media['car_photos_count'])): ?>
            <div class="stm-listing-photos-unit stm-car-photos-<?php echo get_the_id(); ?> <?php echo esc_attr($dynamicClassPhoto); ?>">
                <i class="stm-service-icon-photo"></i>
                <span><?php echo esc_html($car_media['car_photos_count']); ?></span>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery(".<?php echo esc_attr($dynamicClassPhoto); ?>").on('click', function () {
                        jQuery(this).lightGallery({
                            dynamic: true,
                            dynamicEl: [
                                <?php foreach($car_media['car_photos'] as $car_photo): ?>
                                {
                                    src: "<?php echo esc_url($car_photo); ?>"
                                },
                                <?php endforeach; ?>
                            ],
                            download: false,
                            mode: 'lg-fade',
                        })
                    });
                });

            </script>
        <?php endif; ?>
        <?php if (!empty($car_media['car_videos_count'])): ?>
            <div class="stm-listing-videos-unit stm-car-videos-<?php echo get_the_id(); ?> <?php echo esc_attr($dynamicClassVideo); ?>">
                <i class="fa fa-film"></i>
                <span><?php echo esc_html($car_media['car_videos_count']); ?></span>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery(".<?php echo esc_attr($dynamicClassVideo); ?>").on('click', function () {

                        jQuery(this).lightGallery({
                            selector: 'this',
                            dynamic: true,
                            dynamicEl: [
                                <?php foreach($car_media['car_videos'] as $car_video): ?>
                                {
                                    src: "<?php echo esc_url($car_video); ?>",
                                    thumb: ''
                                },
                                <?php endforeach; ?>
                            ],
                            download: false,
                            mode: 'lg-video',
                        })
                    }); //click
                }); //ready

            </script>
        <?php endif; ?>
    </div>

    <?php if (!empty($show_compare) and $show_compare): ?>
        <div
                class="stm-listing-compare"
                data-id="<?php echo esc_attr(get_the_id()); ?>"
                data-title="<?php echo stm_generate_title_from_slugs(get_the_id(), false); ?>"
                data-toggle="tooltip" data-placement="auto left"
                title="<?php esc_attr_e('Add to compare', 'motors') ?>">
            <i class="stm-service-icon-compare-new"></i>
        </div>
    <?php endif; ?>

    <!--Favorite-->
    <?php if (!empty($show_favorite) and $show_favorite): ?>
        <div
                class="stm-listing-favorite"
                data-id="<?php echo esc_attr(get_the_id()); ?>"
                data-toggle="tooltip" data-placement="auto left"
                title="<?php esc_attr_e('Add to favorites', 'motors') ?>">
            <i class="stm-service-icon-staricon"></i>
        </div>
    <?php endif; ?>

    <?php if (has_post_thumbnail()):
        $car_photos = getPostGalleryUrls($gallery, $size);
        ?>
        <?php if (count($car_photos) > 1):  ?>
            <?php stm_listings_load_template('loop/default/list/badge'); ?>
            <div class="image-carousel">
                <?php foreach ($car_photos as $car_photo): ?>
                    <div class="image-item">
                        <a href="<?php the_permalink() ?>">
                            <img
                                    data-original="<?php echo $car_photo; ?>"
                                    src="<?php echo $car_photo; ?>"
                                    class="lazy img-responsive"
                                    alt="<?php echo stm_generate_title_from_slugs(get_the_id()); ?>"
                            />
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <a href="<?php the_permalink() ?>" class="rmv_txt_drctn">
                <div class="image-inner">

                    <!--Badge-->
                    <?php stm_listings_load_template('loop/default/list/badge'); ?>
                    <?php
                    $size = (stm_is_listing_two()) ? 'stm-img-255-160' : 'medium';
                    the_post_thumbnail($size, array('class' => 'img-responsive'));
                    ?>
                </div>
            </a>
        <?php endif; ?>
    <?php else: ?>
        <a href="<?php the_permalink() ?>" class="rmv_txt_drctn">
            <div class="image-inner">
                <!--Badge-->
                <?php stm_listings_load_template('loop/default/list/badge'); ?>
                <img
                        src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/plchldr350.png'); ?>"
                        class="img-responsive"
                        alt="<?php esc_attr_e('Placeholder', 'motors'); ?>"
                />
            </div>
        </a>
    <?php endif; ?>
</div>
