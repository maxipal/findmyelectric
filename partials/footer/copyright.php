<?php $footer_copyright_enabled = get_theme_mod('footer_copyright', true); ?>

<?php
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		),
		'span' => array(
			'class' => array()
		)
	);
?>

<?php
if(stm_is_listing()) {
	$footer_bg = '#153e4d';
} else {
	$footer_bg = '#232628';
}

$footer_bg = get_theme_mod('footer_copyright_color', $footer_bg);
$style = '';

if(!empty($footer_bg)) {
	$style = 'style=background-color:'.$footer_bg;
}
?>

<?php if($footer_copyright_enabled): ?>
	<?php $footer_copyright_text = get_theme_mod('footer_copyright_text', '&copy; 2015 <a target="_blank" href="https://themeforest.net/item/motors-automotive-cars-vehicle-boat-dealership-classifieds-wordpress-theme/13987211">Stylemix Themes</a><span class="divider"></span>Trademarks and brands are the property of their respective owners.'); ?>
	<?php if(stm_is_boats()): ?>
		<div id="footer-copyright" <?php echo esc_attr($style); ?>>

			<?php if(stm_is_listing()) {
				get_template_part('partials/listing-layout-parts/footer-copyright', 'top');
			} ?>

			<div class="container footer-copyright">
				<div class="row">
					<div class="col-md-12">
						<div class="clearfix">
							<?php if($footer_copyright_text): ?>
								<div class="copyright-text text-center"><?php echo wp_kses($footer_copyright_text, $allowed_html); ?></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else: ?>
		<div id="footer-copyright" <?php echo esc_attr($style); ?>>

			<?php if(stm_is_listing()) {
				get_template_part('partials/listing-layout-parts/footer-copyright', 'top');
			} ?>

			<div class="container footer-copyright">
				<div class="row">
                    <?php if(!stm_is_auto_parts()): ?>
					<div class="col-md-8 col-sm-8">
						<div class="clearfix">
							<?php if($footer_copyright_text): ?>
								<div class="copyright-text"><?php echo wp_kses($footer_copyright_text, $allowed_html); ?><br><span><a href="https://www.findmyelectric.com/about-us/founding-vision/#investment-funding" target="_blank" rel="noopener">Funding &#38; Investment</a> | <a href="https://www.findmyelectric.com/about-us/founding-vision/#legal-information" target="_blank" rel="noopener">Legal</a> | <a href="https://www.findmyelectric.com/policies/terms-and-conditions/" rel="noopener">Terms &#38; Conditions</a> | <a href="https://www.findmyelectric.com/policies/privacy-policy/" rel="noopener">Privacy Policy</a> | <a href="https://www.findmyelectric.com/policies/fraud-scams-prevention/" rel="noopener">Fraud Prevention</a> | <a href="https://www.findmyelectric.com/sitemap_index.xml" target="_blank" rel="noopener">Sitemap</a></span><br><br><span>
		<span>Find My Electric is an independent organization and is not affiliated with or endorsed by Tesla, Inc. All marks featured or referred to within Find My Electric internet sites are subject to the trademark rights of Tesla, Inc., including, but not limited to, model name plates and corporate logos and emblems.</span></span></div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="clearfix">
							<div class="pull-right xs-pull-left">
								<?php $socials = stm_get_header_socials('footer_socials_enable'); ?>
								<!-- Header top bar Socials -->
								<?php if(!empty($socials)): ?>
									<div class="pull-right">
										<div class="copyright-socials">
											<ul class="clearfix">
												<?php foreach ( $socials as $key => $val ): ?>
													<li>
														<a href="<?php echo esc_url($val) ?>" target="_blank">
															<i class="fa fa-<?php echo esc_attr($key); ?>"></i>
														</a>
													</li>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="copyright-text Minneapolis-love">
						<span>Made with <span class="made-with-love"><i class="fa fa-heart"></i></span> in Minneapolis, MN
							<img role="img" src="https://d2q97jj8nilsnk.cloudfront.net/images/Minnesota.svg" alt="Minnesota" width="30px" /></span>
						</div>
					</div>
                    <?php else : ?>
                        <div class="col-md-12 col-sm-12">
                            <div class="clearfix">
                                <?php if($footer_copyright_text): ?>
                                    <div class="copyright-text"><?php echo wp_kses($footer_copyright_text, $allowed_html); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>
