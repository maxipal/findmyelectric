<?php
//Getting gallery list
$post_id = get_the_ID();
$user_added_by = get_post_meta($post_id, 'stm_car_user', true);
$gallery = get_post_meta($post_id, 'gallery', true) ? get_post_meta($post_id, 'gallery', true) : [];
$video_preview = get_post_meta($post_id, 'video_preview', true);
$gallery_video = get_post_meta($post_id, 'gallery_video', true);
$special_car = get_post_meta($post_id, 'special_car', true);

$badge_text = get_post_meta($post_id, 'badge_text', true);
$badge_bg_color = get_post_meta($post_id, 'badge_bg_color', true);

$is_free = $is_standard = $is_featured = $no_bought = false;
$subscription = stm_get_plan_listing($post_id);
$s_plan_free = (int)get_theme_mod('free_plan', 9819 );
$s_plan_standard = (int)get_theme_mod('single_plan', 6043 );
if(!empty($subscription['product_id'])){
	if($s_plan_free == $subscription['product_id'])	$is_free = true;
	if($s_plan_standard == $subscription['product_id'])	$is_standard = true;
	if(stm_is_featured($subscription['product_id']))	$is_featured = true;
}else{
	$no_bought = true;
}
if(isset($_REQUEST['show_plans'])){
	var_dump($subscription);
	var_dump([$is_free,$is_standard,$is_featured,$no_bought]);
}

if (empty($badge_text)) {
    $badge_text = 'Featured';
}

$badge_style = '';
if (!empty($badge_bg_color)) {
    $badge_style = 'style=background-color:' . $badge_bg_color . ';';
}

if (has_post_thumbnail()) {
    $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
    array_unshift($gallery, $post_thumbnail_id);
}

$asSold = get_post_meta(get_the_ID(), 'car_mark_as_sold', true);
if (!empty($asSold)) $gallery = array_slice($gallery, 0, 1);

$image_size = 'full';//'stm-img-771-450';//'stm-img-1110-577';//'stm-img-796-466';
$image_size_thumb = 'stm-img-796-466';//'stm-img-350-205';
$photos = formPhotoSwipeGallery($gallery, 'stm-img-1600-1200');
if(isset($_REQUEST['get_img_s'])) var_dump(get_intermediate_image_sizes());
?>

<?php if (!has_post_thumbnail() and stm_check_if_car_imported($post_id)): ?>
    <img
		src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/automanager_placeholders/plchldr798automanager.png'); ?>"
		class="img-responsive"
		alt="<?php esc_attr_e('Placeholder', 'motors'); ?>"
    />
<?php endif; ?>

<div class="stm-car-carousels" <?php if (!empty($asSold)) echo 'style="opacity:0.5;"' ?>>

    <!--New badge with videos-->
    <?php $car_media = stm_get_car_medias($post_id); ?>
    <?php if (!empty($car_media['car_videos_count']) and $car_media['car_videos_count'] > 0): ?>
        <div class="stm-car-medias">
            <div class="stm-listing-videos-unit stm-car-videos-<?php echo get_the_id(); ?>">
                <i class="fa fa-film"></i>
                <span><?php echo esc_html($car_media['car_videos_count']); ?><?php esc_html_e('Video', 'motors'); ?></span>
            </div>
        </div>

        <script>
            jQuery(document).ready(function () {

                jQuery(".stm-car-videos-<?php echo get_the_id(); ?>").on('click', function () {
                    jQuery(this).lightGallery({
                        dynamic: true,
                        dynamicEl: [
                            <?php foreach($car_media['car_videos'] as $car_video): ?>
                            {
                                src: "<?php echo esc_url($car_video); ?>",
                            },
                            <?php endforeach; ?>
                        ],
                        download: false,
                        mode: 'lg-fade',
                    })
                }) //click
            }) //ready

        </script>
    <?php endif; ?>

    <?php if (!empty($special_car) and $special_car == 'on'): ?>
        <div class="special-label h5" <?php echo esc_attr($badge_style); ?>>
            <?php stm_dynamic_string_translation_e('Special Badge Text', $badge_text); ?>
        </div>
    <?php endif; ?>
    <div class="stm-big-car-gallery single-car <?php if(!empty($asSold)) echo 'owl-carousel owl-loaded' ?>">
        <?php if (!empty($video_preview) and !empty($gallery_video)): ?>
			<?php $src = wp_get_attachment_image_src($video_preview, $image_size); ?>
            <?php if (!empty($src[0])): ?>
                <div class="stm-single-image video-preview"
                     data-id="big-image-<?php echo esc_attr($video_preview); ?>">
					<?php if (!empty($asSold)): ?>

					<?php else: ?>
						<a data-fancybox="gallery" class="fancy-iframe" data-iframe="true"
						   data-src="<?php echo esc_url($gallery_video); ?>">
							<img src="<?php echo esc_url($src[0]); ?>" class="img-responsive"
								 alt="<?php esc_attr_e('Video preview', 'motors'); ?>"/>
						</a>
					<?php endif ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php
        if ($is_free) {
            $k = 0;
            $photos = array_slice($photos, 0, 3);
            foreach ($gallery as $gallery_image): $k++;
            if ($k == 4) { break; }?>
                <?php $src = wp_get_attachment_image_src($gallery_image, $image_size); ?>
                <?php $full_src = wp_get_attachment_image_src($gallery_image, 'full'); ?>
				<?php $img_meta = wp_get_attachment_metadata( $gallery_image ); ?>
                <div data-w="<?php echo $img_meta['width'] ?>"
					 data-h="<?php echo $img_meta['height'] ?>"
					 class="item <?php echo $img_meta['width'] > $img_meta['height'] ? 'landscape' : 'portrait' ?>">
					<?php if (!empty($asSold)): ?>
						<img src="<?php echo esc_url($src[0]); ?>"
							 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
					<?php else: ?>
						<a href="<?php if(empty($asSold)) echo esc_url($full_src[0]); else echo '#' ?>">
							<img src="<?php echo esc_url($src[0]); ?>"
								 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
						</a>
					<?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php } elseif ($is_standard) {
            $k = 0;
            $photos = array_slice($photos, 0, 15);
            foreach ($gallery as $gallery_image): $k++; if ($k == 16) { break; } ?>
                <?php $src = wp_get_attachment_image_src($gallery_image, $image_size); ?>
                <?php $full_src = wp_get_attachment_image_src($gallery_image, 'full'); ?>
				<?php $img_meta = wp_get_attachment_metadata( $gallery_image ); ?>
                <div data-w="<?php echo $img_meta['width'] ?>"
					 data-h="<?php echo $img_meta['height'] ?>"
					 class="item <?php echo $img_meta['width'] > $img_meta['height'] ? 'landscape' : 'portrait' ?>">
					<?php if (!empty($asSold)): ?>
						<img src="<?php echo esc_url($src[0]); ?>"
							 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
					<?php else: ?>
						<a href="<?php if(empty($asSold)) echo esc_url($full_src[0]); else echo '#' ?>">
							<img src="<?php echo esc_url($src[0]); ?>"
								 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
						</a>
					<?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php } else {
            $k = 0;
            $photos = array_slice($photos, 0, 20);
            foreach ($gallery as $gallery_image): $k++; if ($k == 21) { break; } ?>
                <?php $src = wp_get_attachment_image_src($gallery_image, $image_size); ?>
                <?php $full_src = wp_get_attachment_image_src($gallery_image, 'full'); ?>
				<?php $img_meta = wp_get_attachment_metadata( $gallery_image ); ?>
                <div data-w="<?php echo $img_meta['width'] ?>"
					 data-h="<?php echo $img_meta['height'] ?>"
					class="item <?php echo $img_meta['width'] > $img_meta['height'] ? 'landscape' : 'portrait' ?>">
					<?php if (!empty($asSold)): ?>
						<img src="<?php echo esc_url($src[0]); ?>"
							 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
					<?php else: ?>
						<a href="<?php if(empty($asSold)) echo esc_url($full_src[0]); else echo '#' ?>">
							<img src="<?php echo esc_url($src[0]); ?>"
								 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
						</a>
					<?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php } ?>


        <?php if (!empty($car_media['car_videos_posters']) and !empty($car_media['car_videos'])): ?>
            <?php foreach ($car_media['car_videos_posters'] as $k => $val):
                $src = wp_get_attachment_image_src($val, 'stm-img-350-205');
                $videoSrc = (isset($car_media['car_videos'][$k])) ? $car_media['car_videos'][$k] : '';
                if (!empty($src[0])): ?>
                    <div class="stm-single-image video-preview" data-id="big-image-<?php echo esc_attr($val); ?>">
                        <a class="fancy-iframe" data-iframe="true" data-src="<?php echo esc_url($videoSrc); ?>">
                            <img src="<?php echo esc_url($src[0]); ?>" class="img-responsive"
                                 alt="<?php esc_attr_e('Video preview', 'motors'); ?>"/>
                        </a>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

    </div>

	<?php if(false): ?><div class="carousel_preloader"></div><?php endif ?>
</div>
<?php if (has_post_thumbnail()): $i = 0;?>
	<?php if (empty($asSold)): ?>
		<div class="custom-thumbnails">
			<?php if ($is_free) {
				foreach ($gallery as $gallery_image_id): $i++; if ($i == 4) { break; }
					$gallery_image = wp_get_attachment_image_src($gallery_image_id, $image_size_thumb)[0]; ?>
					<div class="image_wrapper <?php echo ($i - 1 == 0) ? 'active' : ''; ?>">
						<div>
							<img src="<?php echo $gallery_image; ?>" alt="">
						</div>
					</div>
				<?php endforeach; ?>

			<?php } elseif ($is_standard) {
				foreach ($gallery as $gallery_image_id): $i++; if ($i == 16) { break; }
					$gallery_image = wp_get_attachment_image_src($gallery_image_id, $image_size_thumb)[0]; ?>
					<div class="image_wrapper <?php echo ($i - 1 == 0) ? 'active' : ''; ?>">
						<div>
							<img src="<?php echo $gallery_image; ?>" alt="">
						</div>
					</div>
				<?php endforeach; ?>
			<?php } else {
				foreach ($gallery as $gallery_image_id): $i++; if ($i == 21) { break; }
					$gallery_image = wp_get_attachment_image_src($gallery_image_id, $image_size_thumb)[0]; ?>
					<div class="image_wrapper <?php echo ($i - 1 == 0) ? 'active' : ''; ?>">
						<div>
							<img src="<?php echo $gallery_image; ?>" alt="">
						</div>
					</div>
				<?php endforeach; ?>
			<?php } ?>
		</div>
	<?php endif ?>
    <div id="contact-seller" class="contact-seller">
        <button type="button" class="btn contact-seller__btn">Contact Seller</button>
    </div>
<?php endif; ?>

<?php if (empty($asSold)): ?>
	<?php
	$browser = '';
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		if (strlen(strstr($agent, 'Firefox')) > 0) {
			$browser = 'firefox';
		}
	}
	?>
	<script>
		jQuery(document).ready(function () {
			var asSold = '<?php echo $asSold ?>';
			var owlOptions = {
					items: 1,
					smartSpeed: 250,
					dots: false,
					nav: false,
					margin: 0,
					autoplay: false,
					loop: false,
					responsiveRefreshRate: 1000
				},
				pswpOptions = {
					closeEl: true,
					fullscreenEl: false,
					history: false,
					shareEl: false,
					closeOnScroll: false,
					bgOpacity: 0.9,
					tapToClose: true,
					preload: [2, 4],
					showAnimationDuration: 250,
					hideAnimationDuration: <?php echo ($browser ? 150 : 100); ?>,
					loadingIndicatorDelay: 1000,
					clickToCloseNonZoomable: false,
					showHideOpacity:true,
					zoomEl: false,
					//getThumbBoundsFn:false
				};

			var closeElClasses = ['item', 'caption', 'zoom-wrap', 'ui', 'top-bar']
			var big = $('.stm-big-car-gallery')
			var customImages = $('.custom-thumbnails .image_wrapper')
			var index = 0
			var gallery;

			function openGallery($gallery, index, items, pswpOptions) {
				var $pswp = $(".pswp"),
					owl = $gallery.data("owlCarousel")

				// Options
				var options = $.extend(true, {
					// index
					index: index,

					// Thumbnail
					getThumbBoundsFn: function (index) {
						var $thumbnail = $('.item').eq(index).find("img"),
							offset = $thumbnail.offset();
						// let fix = 0;
						// let args = [
						// 	offset,
						// 	$thumbnail.outerWidth(),
						// 	$thumbnail.outerHeight(),
						// 	$('.item').eq(index).data('w'),
						// 	$('.item').eq(index).data('h'),
						// 	window.pageYOffset,
						// 	$('.item').eq(index)[0].getBoundingClientRect().top,
						// 	$('.pswp__zoom-wrap .pswp__img').outerHeight(),
						// 	$('.pswp__zoom-wrap .pswp__img').innerHeight(),
						// 	$('.pswp__zoom-wrap .pswp__img').height()
						// ];
						// let height_img = $('.pswp__zoom-wrap .pswp__img').outerHeight();
						// console.log(args);
						// if($('.item').eq(index).hasClass('portrait') && height_img){
						// 	let half_height = height_img / 2;
						// 	let offset_top = $('.item').eq(index)[0].getBoundingClientRect().top;
						// 	fix = offset_top - half_height + 450/2 + ($('.item').eq(index).data('h') - height_img)/2;
						// 	console.log(fix);
						// 	offset.top = fix;
						// }else if(height_img){
						// 	offset.top = offset.top - 64;
						// }
						let res = {
							x: offset.left,
							y: offset.top,
							w: $thumbnail.outerWidth()
						};
						return res;
					},
					getDoubleTapZoom: function (isMouseClick, item) {
						return item.initialZoomLevel;
					},
					zoomEl: false
				}, pswpOptions);

				// PhotoSwipe
				gallery = new PhotoSwipe($pswp.get(0), PhotoSwipeUI_Default, items, options);
				gallery.listen('afterInit', function() {
					calculateArrowPosition()
					//calculateCloseBtnPosition()
				});

				gallery.init();
				gallery.items.forEach(function (el, i) {
					if(el.fitRatio * el.w > 1500){
						gallery.items[i].fitRatio = 1;
					}
				});

				$('.pswp__button--prev').unbind('click').click(function(){
					gallery.prev()
				});

				$('.pswp__button--next').unbind('click').click(function(){
					gallery.next()
				});

				gallery.listen('afterChange', function () {
					calculateArrowPosition()
					//calculateCloseBtnPosition()
				})

				gallery.listen("close", function () {
					$('.owl-stage').addClass('active')
					big.trigger('to.owl.carousel', [this.getCurrentIndex(), 0, true])
					this.currItem.initialLayout = options.getThumbBoundsFn(this.getCurrentIndex());
				});
			}
			$(window).resize(function(){
				calculateArrowPosition()
				//calculateCloseBtnPosition()
			})

			function calculateArrowPosition(){
				const index =  gallery.getCurrentIndex();
				const currentActiveItem = gallery.items.find((el, index2) => index2 == index)

				const { container, w } =  currentActiveItem
				const img = $(container).find('img')
				const arrowBtn = $('.pswp__button--arrow');
				let imgclientWidth = w;

				imgclientWidth = 1600;
				const diff = ($(window).width() - imgclientWidth) / 2

				var args = [
					$(window).width(),
					imgclientWidth,
					arrowBtn.width(),
					diff,
					diff - arrowBtn.width() - 10,
					diff - arrowBtn.width() - 10,
					img.outerWidth(),
					img.outerHeight()
				];
				// console.log(args);

				let left_right = diff - arrowBtn.width();
				if(left_right < 0) left_right = 0;

				$('.pswp__button--prev').css({
					left: left_right + 'px'
				})
				$('.pswp__button--next').css({
					right: left_right + 'px'
				})
			}

			function calculateCloseBtnPosition(){
				setTimeout(() => {
					const currentActiveItem = gallery.currItem,
						{ container } =  currentActiveItem,
						img = container.querySelector('img'),
						windowWidth = $(window).width(),
						windowHeight = $(window).height(),
						closeBtn = $('.pswp__button.pswp__button--close')

					let imgWidth, imgHeight;
					if(typeof  img.naturalWidth == 'undefined'){
						imgWidth =  img.style.width.replace('px', '')
						imgHeight = img.style.height.replace('px', '')
					}else{
						imgWidth =  Math.min(img.style.width.replace('px', ''), img.naturalWidth)
						imgHeight = Math.min(img.style.height.replace('px', ''), img.naturalHeight)
					}

					closeBtn.css({
						top: windowHeight - ( (windowHeight - imgHeight + closeBtn.height() + 40 ) / 2 ) + 'px',
						left: windowWidth - ( (windowWidth - imgWidth + closeBtn.width() + 40 ) / 2 ) + 'px'
					});
				}, 400)

			}

			function initializeGallery($elem, owlOptions, pswpOptions) {
				console.log(owlOptions);
				$elem.each(function (i) {
					var $gallery = $(this),
						uid = i + 1,
						items = <?php echo json_encode($photos)?>,
						options = $.extend(true, {}, pswpOptions);

					// OwlCarousel
					$gallery.owlCarousel(owlOptions);

					// Options
					options.galleryUID = uid;
					$gallery.attr("data-pswp-uid", uid);

					// PhotoSwipe
					$gallery.find(".owl-item").on("click", function (e) {
						e.preventDefault()
						if (!$(e.target).is("img")) return;

						// items PhotoSwipe.init()
						if(asSold === '') openGallery($gallery, $(this).index(), items.concat(), options);
						return false;
					});
				});
			}

			initializeGallery(big, owlOptions, pswpOptions);

			big.on('drag.owl.carousel', function () {
				$(this).find('.owl-stage').removeClass('active')
			})

			customImages.click(function (event) {
				$('.custom-thumbnails .image_wrapper').removeClass('active')
				$(this).addClass('active')
				index = $(this).index()
				$('.owl-stage').addClass('active')
				big.trigger('to.owl.carousel', [index, 0, true])
			});

			$('#contact-seller').click(function(){
				$([document.documentElement, document.body]).animate({
					scrollTop: $("#stm_dealer_car_info-2").offset().top - 40
				}, 800);
			})
		})
	</script>
<?php endif ?>
