<?php
$user = wp_get_current_user();
$user_fields = stm_get_user_custom_fields( $user->ID );
$link = add_query_arg(array('page' => 'settings'), stm_get_author_link(''));
$user_id = get_current_user_id();

//$stm_user_active_subscriptions = stm_user_active_subscriptions(false, $userid );
//$subscription_id = $stm_user_active_subscriptions["product_id"];
//$subscription_title = $stm_user_active_subscriptions["plan_name"];
//$subscription_plan_free = get_theme_mod('free_plan', 9819 );
//$subscription_plan_standart = get_theme_mod('single_plan', 6043 );
//$all_packages_page_id =  (int) get_theme_mod('all_packages');
?>

<div class="stm-user-private">
	<div class="container">
		<div class="row">

			<div class="col-md-3 col-sm-3 hidden-sm hidden-xs stm-sticky-user-sidebar">
				<div class="stm-user-private-sidebar">

					<div class="clearfix stm-user-top">

						<!-- <div class="stm-user-avatar">
							<a href="<?php echo esc_url($link); ?>"
								class="stm-image-avatar image <?php echo esc_attr(empty($user_fields['image']) ? 'hide-photo' : 'hide-empty'); ?>">
								<img class="img-responsive img-avatar" src="<?php echo (!empty(get_user_meta($user->ID, 'wsl_current_user_image', true))) ? esc_url(get_user_meta($user->ID, 'wsl_current_user_image', true)) : esc_url($user_fields['image']); ?>" />
								<div class="stm-empty-avatar-icon"><i class="fa fa-camera"></i></div>
							</a>
						</div> -->

						<div class="stm-user-profile-information">
							<a href="<?php echo esc_url($link); ?>" class="title heading-font">
								<?php echo esc_attr(stm_display_user_name($user->ID)); ?>
							</a>
							<div class="title-sub"><?php esc_html_e('Seller', 'motors'); ?></div>
							<?php if(!empty($user_fields['socials'])): ?>
								<div class="socials clearfix">
									<?php foreach($user_fields['socials'] as $social_key => $social): ?>
										<a href="<?php echo esc_url($social); ?>">
											<?php
												if($social_key == 'youtube') {
													$social_key = 'youtube-play';
												}
											?>
											<i class="fa fa-<?php echo esc_attr($social_key); ?>"></i>
										</a>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>

					</div>

<!--					--><?php //if(!stm_pricing_enabled()): ?>
<!--						<div class="stm-became-dealer">-->
<!--							<a href="--><?php //echo esc_url(add_query_arg(array('become_dealer' => 1), stm_get_author_link(''))); ?><!--" class="button stm-dp-in">--><?php //esc_html_e('Become a dealer', 'motors'); ?><!--</a>-->
<!--						</div>-->
<!--					--><?php //endif; ?>

					<?php get_template_part( 'partials/user/private/navigation' ) ?>

					<?php if(!empty($user_fields['phone'])): ?>
						<div class="stm-dealer-phone">
							<i class="stm-service-icon-phone"></i>
							<div class="phone-label heading-font"><?php esc_html_e('Seller Contact Phone', 'motors'); ?></div>
							<div class="phone"><?php echo esc_attr($user_fields['phone']); ?></div>
						</div>
					<?php endif; ?>

					<div class="stm-dealer-mail">
						<i class="fa fa-envelope-o"></i>
						<div class="mail-label heading-font"><?php esc_html_e('Seller Email', 'motors'); ?></div>
						<div class="mail">
								<?php echo esc_attr($user->data->user_email); ?>
						</div>
					</div>
		<?php if(false): ?>
                    <?php
                    if(stm_is_free_plan()) { ?>
                        <div class="stm-user-current-plan-info heading-font">
                            <div class="sub-title"><?php esc_html_e('Current Plan', 'motors'); ?></div>
                            <div class="stm-plan-name"><?php esc_html_e("Free plan", "motors"); ?></div>
							<div class="stm-plan-renew">
								<a href="https://www.findmyelectric.com/free-upgrade/" class="button stm-dp-in">
									<?php esc_html_e('Upgrade plan', 'motors'); ?>
								</a>
							</div>
						</div>
                    <?php } else { ?>
						<?php if(!$subscription_id): ?>
							<div class="stm-user-current-plan-info heading-font">
								<div class="stm-plan-renew">
									<a href="<?php echo get_permalink($all_packages_page_id) ?>" class="button stm-dp-in">
										<?php esc_html_e('Buy plan', 'motors'); ?>
									</a>
								</div>
							</div>
						<?php else: ?>
							<div class="stm-user-current-plan-info heading-font">
								<div class="sub-title"><?php esc_html_e('Current Plan', 'motors'); ?></div>
								<div class="stm-plan-name"><?php echo $subscription_title; ?></div>
								<?php if(stm_is_free_plan()): ?>
									<div style="display:none">Expired: <?php var_dump(stm_get_free_expired_date());  ?></div>
								<?php else: ?>
									<div style="display:none">Expired: <?php   ?></div>
								<?php endif; ?>
								<?php
								if($subscription_id == $subscription_plan_standart): ?>
									<div class="stm-plan-renew">
										<a href="https://www.findmyelectric.com/standard-upgrade/" class="button stm-dp-in">
											<?php esc_html_e('Upgrade plan', 'motors'); ?>
										</a>
									</div>
								<?php endif; ?>
							</div>
						<?php endif ?>
                    <?php } ?>
		<?php endif ?>
				</div>
			</div>

			<div class="col-md-9 col-sm-12">

				<?php get_template_part( 'partials/user/private/main' ) ?>

			</div>

		</div>
	</div>
</div>
