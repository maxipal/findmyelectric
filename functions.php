<?php

require_once 'inc/assets.php';
require_once 'inc/breadcrumbs.php';
require_once 'inc/helpers.php';
require_once 'inc/custom.php';
require_once 'inc/field-options.php';
require_once 'inc/inventory.php';
require_once 'inc/stm-child-ajax.php';
require_once 'inc/customizer.php';
require_once 'inc/custom-url.php';
require_once 'inc/subscriptions.php';
require_once 'inc/vc-elements.php';
require_once 'inc/woocomerce.php';
require_once 'inc/algolia.php';
require_once 'inc/custom_parent-tax.php';
require_once 'inc/is_login.php';
require_once 'inc/recaptcha.php';
require_once 'inc/app-ajax.php';
require_once 'inc/cron.php';
require_once 'inc/widgets/car-contact-form.php';
require_once 'inc/redirects.php';
include_once 'inc/filter-by-ip.php';
include_once 'inc/migrate_cloud.php';
include_once 'inc/autocheck.php';

if(!function_exists('emailTemplateView')){
	require_once( 'inc/email_template_manager/email_template_manager.php' );
}


/* Auto Check T&C on WooCommerce Checkout */

function check_wc_terms( $terms_is_checked ) {
	return true;
}
add_filter( 'woocommerce_terms_is_checked', 'check_wc_terms', 10 );
add_filter( 'woocommerce_terms_is_checked_default', 'check_wc_terms', 10 );
