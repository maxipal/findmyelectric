window.tinymce = tinymce;
if(tinymce){
	tinymce.init({
		selector: '.tinymce',
		plugins: 'lists advlist paste',
		menubar:false,
		paste_as_text: true,
		toolbar: ' bold italic underline | alignleft aligncenter ' +
			' alignright alignjustify |  bullist numlist',
		height : "300",
		content_css: '/wp-content/themes/motors-child/assets/css/tiny.css',
	});
}

