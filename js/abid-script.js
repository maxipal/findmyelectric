function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this, args = arguments;
    var later = function () {
      timeout = null;
      if ( !immediate ) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if ( callNow ) func.apply(context, args);
  };
}

jQuery(document).ready(function () {

  var openSpeed = 600; // filters container opening animation speed
  var closeSpeed = 200; // filters container closing animation speed
  var doneCloseSpeed = 900; // done button close speed

  var filterIcon = $('.abd-filter-icon-button');
  var closeSearchFilterButton = $('.abd-close-search-filter-button');
  var mobileFiltersContainer = $('.col-md-3.col-sm-12.classic-filter-row.sidebar-sm-mg-bt.mobile-filters');

  // modern filters
  var modernFilterIcon = $('.filter-button');
  var closeSearchModernFilterButton = $('.abd-close-modern-search-filter-button');
  var mobileModernFiltersContainer = $('.abd-mobile-modern-filters');
  // var fullModernFiltersContainer = $('.abd-full-modern-filters');

  // classic filters
  filterIcon.on('click', function () {
    if ( mobileFiltersContainer.css('display').toLowerCase() === 'none' ) {
      mobileFiltersContainer.slideDown(openSpeed);
    } else {
      mobileFiltersContainer.slideUp(closeSpeed);
    }
  });

  // modern filters
  // modernFilterIcon.on('click', function () {
  //   if ( mobileModernFiltersContainer.css('display').toLowerCase() === 'none' ) {
  //     mobileModernFiltersContainer.show("slide", { direction: "left" }, 800);
  //   } else {
  //     mobileModernFiltersContainer.hide("slide", { direction: "left" }, 800);
  //   }
  // });

  var width = window.innerWidth;
  var resizeFunction = debounce(function () {
    if ( window.innerWidth !== width ) {
      width = window.innerWidth;
      if ( window.innerWidth > 767 ) {
        // classic filters
        mobileFiltersContainer.show();

        //modern filters
        mobileModernFiltersContainer.show();
      } else if ( window.innerWidth <= 767 ) {
        //classic filters
        mobileFiltersContainer.hide();

        //modern filters
        mobileModernFiltersContainer.hide();
      }
    }
  }, 150);

  // Done button click function - classic filters
  closeSearchFilterButton.on('click', function () {
    mobileFiltersContainer.slideUp(doneCloseSpeed);
  });

  // Done button click function - Modern filters
  closeSearchModernFilterButton.on('click', function () {
    mobileModernFiltersContainer.slideUp(doneCloseSpeed);
  });

  $(window).on('resize', resizeFunction);

});
