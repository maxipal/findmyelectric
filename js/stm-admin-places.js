(function($) {
	"use strict";

	function initialize_stm_places_search() {


		var input = document.getElementById('stm_car_location');

		if(input !== null) {


			var autocomplete = new google.maps.places.Autocomplete(input);

			google.maps.event.addListener(autocomplete, 'place_changed', function (e) {
				var place = autocomplete.getPlace();
				console.log(place);
				var state = '';
				var state_two_letter = '';
				var city = place.address_components[0].long_name;
				if(typeof place.address_components[2] == 'undefined'){
					state = place.address_components[0].long_name;
					state_two_letter = place.address_components[0].short_name;
				}else{
					state = place.address_components[2].long_name;
					state_two_letter = place.address_components[2].short_name; //add variable for two letter state
				}

				var lat = '';
				var lng = '';

				if (typeof(place.geometry) != 'undefined') {
					lat = place.geometry.location.lat();
					lng = place.geometry.location.lng();
				} else {
					lat = '';
					lng = '';
				}



				$('#stm_city').val(city);
				$('#stm_state').val(state);
				$('#stm_state_car_two_letter_admin').val(state_two_letter);
				$('#stm_city_car_admin').val(place.formatted_address);

				$('#stm_lat_car_admin').val(lat);
				$('#stm_lng_car_admin').val(lng);
			});

			google.maps.event.addDomListener(input, 'keydown', function (e) {
				if (e.keyCode == 13) {
					e.preventDefault();
				}
			});
		}

	}

	google.maps.event.addDomListener(window, 'load', initialize_stm_places_search);

})(jQuery);
