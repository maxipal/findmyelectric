import Vue from 'vue';
import draggable from 'vuedraggable';
import VueTouch from 'vue-touch';

Vue.use(VueTouch, {name: 'v-touch'});

/*globals jQuery, window */
(function($) {
	$.retryAjax = function (ajaxParams) {
		var errorCallback;
		ajaxParams.tryCount = (!ajaxParams.tryCount) ? 0 : ajaxParams.tryCount;
		ajaxParams.retryLimit = (!ajaxParams.retryLimit) ? 2 : ajaxParams.retryLimit;
		ajaxParams.suppressErrors = true;

		if (ajaxParams.error) {
			errorCallback = ajaxParams.error;
			delete ajaxParams.error;
		} else {
			errorCallback = function () {

			};
		}

		ajaxParams.complete = function (jqXHR, textStatus) {
			if ($.inArray(textStatus, ['timeout', 'abort', 'error']) > -1) {
				this.tryCount++;
				if (this.tryCount <= this.retryLimit) {

					// fire error handling on the last try
					if (this.tryCount === this.retryLimit) {
						this.error = errorCallback;
						delete this.suppressErrors;
					}

					//try again
					$.ajax(this);
					return true;
				}

				console.log('There was a server error.  Please refresh the page.  If the issue persists, give us a call. Thanks!');
				return true;
			}
		};

		$.ajax(ajaxParams);
	};
}(jQuery));


const confirmationDelete = $(
	'.stm-delete-confirmation-popup, .stm-delete-confirmation-overlay');
window.imageUploader = new Vue({
	el: '#image_uploader',
	data: {
		minPhotosLimit: 10,
		maxPhotosLimit: 20,
		files: '',
		featuredIndex: 0,
		isDraggable: false,
		lastUploadedIndex: 0,
		uploadBtnPos: 0,
		totalUploaded: 0,
		images: imageUploadSettings.images,
		previews: [],
		allowedExt: /(\.jpg|\.jpeg|\.png)$/i,
		a: '',
	},
	components: {
		draggable,
	},
	methods: {
		onMove(e) {
			return !e.related.classList.contains('image_uploader__item-preview');
		},

		handleSelects(event) {
			event.preventDefault();
			this.files = [];
			this.files = Array.prototype.slice.call(event.target.files).reverse();

			//give an opportunity to upload the same images
			event.target.value = '';

			const diff = this.totalUploaded + this.files.length - this.maxPhotosLimit;
			if (diff > 0) {
				for (let i = 0; i < diff; i++) {
					this.files.splice(this.files.length - i, 1);
				}
				alert(
					`Sorry, but some files will not be uploaded due to limit of ${this.maxPhotosLimit} photos`);
			}

			this.files.forEach((file, index) => {

				if (!this.allowedExt.exec(file.name)) {
					alert(`Sorry, invalid image format: ${file.name}`);
					return;
				}

				if (file.size > imageUploadSettings.size) {
					alert(`Sorry, image is too large: ${file.name}`);
					return;
				}

				if (this.previews[this.lastUploadedIndex]) {
					this.resizeImage(file).then((file) => {
						this.uploadImages(file);
					})
				}
			});

			this.files = [];
		},

		clickHandle(index) {
			//if (index === this.triggerIndex) {
				this.$refs.inputUploader.click();
			//}
		},
		makeThumbFromFile: function(imageFile) {
			return {url: URL.createObjectURL(imageFile)};
		},
		dataURItoBlob(dataURI) {
			// convert base64/URLEncoded data component to raw binary data held in a string
			var byteString;
			if (dataURI.split(',')[0].indexOf('base64') >= 0)
				byteString = atob(dataURI.split(',')[1]);
			else
				byteString = unescape(dataURI.split(',')[1]);
			// separate out the mime component
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
			// write the bytes of the string to a typed array
			var ia = new Uint8Array(byteString.length);
			for (var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}
			return new Blob([ia], {type:mimeString});
		},
		resizeImage(orig_file) {
			return new Promise((resolve, reject) => {
				let self = this;
				// Load the image
				var reader = new FileReader();
				reader.onload = function (readerEvent) {
					var image = new Image();
					image.onload = function (imageEvent) {

						// Resize the image
						var canvas = document.createElement('canvas'),
							max_size_w = 1600, // Max width
							max_size_h = 1200, // Max height
							width = image.width,
							height = image.height;

						if (width > height) {
							if (width > max_size_w) {
								height *= max_size_w / width;
								width = max_size_w;
							}
						} else {
							if (height > max_size_h) {
								width *= max_size_h / height;
								height = max_size_h;
							}
						}

						canvas.width = width;
						canvas.height = height;
						canvas.getContext('2d').drawImage(image, 0, 0, width, height);
						var resizedImage = canvas.toDataURL('image/jpeg');
						var _blob = self.dataURItoBlob(resizedImage);
						var opt = {
							type: orig_file.type,
							lastModifiedDate: orig_file.lastModifiedDate
						}

						var file = new File([_blob], orig_file.name, opt);
						// End resize the image
						resolve(file);
					}
					image.src = readerEvent.target.result;
				}
				reader.readAsDataURL(orig_file);
			});
		},
		uploadImages(file) {
			if(this.uploadBtnPos >= 20) return;
			++this.uploadBtnPos;
			let index = this.uploadBtnPos ? this.uploadBtnPos - 1 : 0;
			const lastUploadedIndex = this.previews[index] ? this.previews[index].index : 0;
			const fd = new FormData();
			const blob = this.makeThumbFromFile(file);
			let item = this.getPreviewByOriginalIndex(lastUploadedIndex);
			item.preload = true;

			// console.log({
			// 	IndexPreview: index,
			// 	featuredIndex: this.featuredIndex,
			// 	lastUploadedIndex: this.lastUploadedIndex,
			// 	totalUploaded: this.totalUploaded,
			// 	lastUploadedIndex2: lastUploadedIndex,
			// 	previews: this.previews,
			// 	uploadBtnPos: this.uploadBtnPos,
			// 	item: this.getPreviewByOriginalIndex(lastUploadedIndex)
			// });


			fd.append('action', 'custom_photo_upload');
			fd.append('lastUploadedIndex', lastUploadedIndex);
			fd.append('files[' + 1 + ']', file);
			fd.append('blob', blob.url)

			let ajax = {
				xhr: () => {
					var xhr = $.ajaxSettings.xhr();
					xhr.upload.addEventListener('progress', (evt) => {
						if (evt.lengthComputable) {
							const percentComplete = Math.ceil(evt.loaded / evt.total * 100);
							const item = this.getPreviewByOriginalIndex(lastUploadedIndex);
							item.progress = percentComplete;
						}
					}, false);
					return xhr;
				},
				target: '#stm_sell_a_car_form',
				type: 'POST',
				url: ajaxurl,
				data: fd,
				contentType: false,
				processData: false,
				context: this,
				timeout: 300000,
				cache: false,
				success: (response) => {
					let item = this.getPreviewByOriginalIndex(response.index);
					item.src = blob.url;
					item.thumb = blob.url;
					item.preload = false;
					item.id = response.id;
					++this.lastUploadedIndex;
					++this.totalUploaded;
					if (this.totalUploaded === 1) {
						this.featuredIndex = this.previews[0].index;
					}
					// console.log({
					// 	index: index,
					// 	featuredIndex: this.featuredIndex,
					// 	lastUploadedIndex: this.lastUploadedIndex,
					// 	totalUploaded: this.totalUploaded,
					// 	lastUploadedIndex2: lastUploadedIndex,
					// 	previews: this.previews,
					// 	uploadBtnPos: this.uploadBtnPos,
					// 	preload: this.previews[lastUploadedIndex].preload,
					// 	item: this.getPreviewByOriginalIndex(lastUploadedIndex)
					// });

				},
			}
			$.ajax(ajax).fail(function(e) {
				console.log("Cant send image '"+file.name+"'. Retry: 2");
				setTimeout(function(){
					$.ajax(ajax).fail(function(e) {
						console.log("Cant send image '"+file.name+"'. Retry: 3");
						setTimeout(function(){
							$.ajax(ajax).fail(function(e) {
								console.log("Cant send image '"+file.name+"'. Failed");
							});
						},2000);
					})
				},1000);
			})
		},
		onTap(item) {
			let index = this.previews.indexOf(item);
			this.featuredIndex = this.previews[index].index;
		},
		deleteAll() {
			this.images = [];
			this.previews.forEach(el => {
				el.src = '';
			});
			this.lastUploadedIndex = 0;
			this.totalUploaded = 0;
			this.uploadBtnPos = 0;
			confirmationDelete.addClass('stm-disabled');
		},
		deleteSingle(index) {
			this.isDraggable = true;
			this.previews.splice(index, 1);
			this.previews.push(this.singleImage());
			this.lastUploadedIndex = this.lastUploadedIndex ? --this.lastUploadedIndex : 0;
			this.totalUploaded = --this.totalUploaded;
			--this.uploadBtnPos;
			if (index === this.featuredIndex && index !== 0) {
				this.featuredIndex = 0;
			}
			// console.log({
			// 	featuredIndex: this.featuredIndex,
			// 	lastUploadedIndex: this.lastUploadedIndex,
			// 	totalUploaded: this.totalUploaded,
			// 	previews: this.previews,
			// 	index: index
			// });
			this.isDraggable = false;
		},
		singleImage() {
			return {
				src: '',
				thumb: '',
				preload: false,
				index: this.previews[this.previews.length - 1]
					? this.previews[this.previews.length - 1].index + 1
					: '',
				progress: 0,
			};
		},
		generatePreviews() {
			this.previews = [];
			for (let i = 0; i < this.maxPhotosLimit; i++) {
				let item = this.singleImage();
				if (this.images[i]) {
					const {src, thumb, id} = this.images[i];

					item.src = src
					item.thumb = thumb
					item.id = id
				}
				this.previews.push({
					...item,
					index: i,
				});
			}
		},
		getPreviewByOriginalIndex(originalIndex) {
			return this.previews.find(el => {
				return el.index == originalIndex;
			});
		},
	},
	computed: {
		imageCount() {
			let limit;
			let wordCount;

			if ( this.previewsLength === 0 ) {
				return 'We recommend adding at least 10 photos';
			}

			if (this.previewsLength === this.maxPhotosLimit) {
				return 'Photo limit reached';
			}

			if (this.previewsLength < this.minPhotosLimit) {
				limit = this.minPhotosLimit - this.previewsLength;
			} else {
				limit = this.maxPhotosLimit - this.previewsLength;
			}

			if ( this.previewsLength === 19 ) {
				return 'Add up to 1 more photo';
			}
			if ( this.previewsLength === 9 ) {
				return 'We recommend adding at least 1 more photo';
			}

			if ( this.previewsLength < 10 ) {
				return 'We recommend adding at least ' + limit + ' more photos';
			}
			if ( this.previewsLength >= 10 && this.previewsLength <= 18 ) {
				return 'Add up to ' + limit + ' more photos';
			}


			return 'We recommend adding at least ' + limit + ' photos';
		},
		imagesLength() {
			return this.images.length;
		},
		previewsLength() {
			return this.previews.filter(el => el.src).length;
		},
		// triggerIndex() {
		// 	console.log({
		// 		title: 'triggerIndex',
		// 		featuredIndex: this.featuredIndex,
		// 		lastUploadedIndex: this.lastUploadedIndex,
		// 		totalUploaded: this.totalUploaded,
		// 		previews: this.previews,
		// 		preload: this.previews[this.lastUploadedIndex].preload,
		// 		src: this.previews[this.lastUploadedIndex].src
		// 	});
		//
		// 	if (!this.previews[this.lastUploadedIndex]) {
		// 		return;
		// 	}
		// 	if (this.previews[this.lastUploadedIndex].preload ||
		// 		this.previews[this.lastUploadedIndex].src) {
		// 		return ++this.lastUploadedIndex;
		// 	}
		//
		// 	return this.lastUploadedIndex;
		// },
		featuredImage() {
			return this.getPreviewByOriginalIndex(this.featuredIndex);
		},
	},

	created() {
		this.generatePreviews();
		this.lastUploadedIndex = this.previewsLength;
		this.totalUploaded = this.previewsLength;
		this.uploadBtnPos = this.previewsLength;
	},
	mounted() {
		// let index = this.uploadBtnPos ? this.uploadBtnPos - 1 : 0;
		// const lastUploadedIndex = this.previews[index] ? this.previews[index].index : 0;
		//
		// console.log({
		// 	IndexPreview: index,
		// 	featuredIndex: this.featuredIndex,
		// 	lastUploadedIndex: this.lastUploadedIndex,
		// 	totalUploaded: this.totalUploaded,
		// 	lastUploadedIndex2: lastUploadedIndex,
		// 	previews: this.previews,
		// 	uploadBtnPos: this.uploadBtnPos,
		// 	item: this.getPreviewByOriginalIndex(lastUploadedIndex)
		// });
	}
});

$(document).ready(function() {

	$('#delete-all').click(function() {
		confirmationDelete.removeClass('stm-disabled');
	});

	confirmationDelete.find('.actions a, .fa.fa-close').click(function(event) {
		event.preventDefault();
		if (event.target.id === 'delete') {
			imageUploader.deleteAll();
		} else {
			confirmationDelete.addClass('stm-disabled');
		}
	});

});
