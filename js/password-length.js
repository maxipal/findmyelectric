var myInput = document.getElementById("psw");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var submit = document.getElementById("change-pas-btn")

if(myInput){
    // When the user clicks on the password field, show the message box
    myInput.onfocus = function() {
        document.getElementById("message").style.display = "block";
    }
    myInput.onblur = function() {
        document.getElementById("message").style.display = "none";
    }
}

if(submit){
    // When the user clicks outside of the password field, hide the message box
    submit.onclick = function() {
        document.getElementById("message").style.display = "none";
    }
}

if(myInput) {

    // When the user starts to type something inside the password field
    myInput.onkeyup = function () {
        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (myInput.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (myInput.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
        }

        // Validate length
        if (myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
        }
    }
}

(function ($) {
    $(document).ready(function() {
        STMListings.save_user_settings_success = function (data) {
            $(this).find('.stm-listing-loader').removeClass('visible');
            $('.stm-user-message').text(data.error_msg);
            let phone = $('.main-info-settings input[name="stm_phone"]').val();
            let email = $('.main-info-settings input[name="stm_email"]').val();
            let first_name = $('.main-info-settings input[name="stm_first_name"]').val();
            let last_name = $('.main-info-settings input[name="stm_last_name"]').val();
            console.log([phone,email,first_name,last_name]);
            $('.stm-dealer-phone .phone').html(phone);
            $('.stm-dealer-mail .mail').html(email);
            if(first_name !== '' || last_name !== ''){
                $('.stm-user-profile-information a').html(first_name + ' ' + last_name);
            }

            $('.stm-image-avatar img, .stm-dropdown-user-small-avatar img').attr('src', data.new_avatar);

            if (typeof data.new_avatar === 'undefined' || data.new_avatar === '') {
                $('.stm-image-avatar').removeClass('hide-empty').addClass('hide-photo');
            } else {
                $('.stm-image-avatar').addClass('hide-empty').removeClass('hide-photo');
            }

        };
    });
}(jQuery))
