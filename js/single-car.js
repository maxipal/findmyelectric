import Vue from 'vue'
import { PhotoSwipe, PhotoSwipeGallery } from 'v-photoswipe'

var $ = jQuery

$(document).ready(function(){

	const app = new Vue({
		el: '#single-gallery',
		components: {
			'v-photoswipe': PhotoSwipe,
			'v-photoswipe-gallery': PhotoSwipeGallery
		},
		data () {
			return {
				isOpen: false,
				optionsGallery: {
					closeElClasses: ['bg','caption', 'zoom-wrap', 'ui', 'top-bar'],
					maxSpreadZoom: 1,
					preload: [5,5],
					getDoubleTapZoom: function (isMouseClick, item) {
						return item.initialZoomLevel;
					},
					zoomEl: false,
					tapToClose: true,
				},
				items: stm_photos
			}
		},
		methods: {
			showPhotoSwipe (index) {
				this.isOpen = true
				this.$set(this.optionsGallery, 'index', index)
			},
			hidePhotoSwipe () {
				this.isOpen = false
			}
		}
	})


	$(document).ready(function () {
		$('.owl-stage').data('lightGallery').destroy(true)
	});

	var big = $('.stm-big-car-gallery')
	var flag = false
	var duration = 800
	var index = 0
	var customImages = $('.custom-thumbnails .image_wrapper')
	var fancyBox = $('[data-fancybox="gallery"]')

	// do not showing fancybox on page reload
	//window.history.replaceState({}, 'listing', '<?php the_permalink()?>');

	// fancyBox.fancybox({
	// 	hash: null,
	// 	loop: true,
	// 	buttons: [
	// 		'close',
	// 	],
	// 	mobile: {
	// 		arrows: false,
	// 		buttons: [
	// 			'close',
	// 			'fullscreen'
	// 		]
	// 	},
	// 	touch: {
	// 		momentum: true,
	// 	},
	// 	beforeClose: function () {
	// 		customImages.eq(index).click()
	// 		setTimeout(function () {
	// 			$('.owl-stage').removeClass('active')
	// 		}, 1)
	// 	},
	// 	beforeShow: function () {
	// 		index = this.index
	// 		$('.owl-stage').addClass('active')
	// 	},
	// 	clickContent: "nextOrClose",
	// });

	big.owlCarousel({
			items: 1,
			smartSpeed: 250,
			dots: false,
			nav: false,
			margin: 0,
			autoplay: false,
			loop: false,
			responsiveRefreshRate: 1000
		})
		.on('drag.owl.carousel', function () {
			$(this).find('.owl-stage').removeClass('active')
		})

	customImages.click(function (event) {
		$('.custom-thumbnails .image_wrapper').removeClass('active')
		$(this).addClass('active')
		index = $(this).index()
		$('.owl-stage').addClass('active')
		big.trigger('to.owl.carousel', [index, 0, true])
	});
})