<?php
$val = (get_option('add_car_pending_template', '') != '') ? stripslashes(get_option('add_car_pending_template', '')) :
    '<table>
        <tr>
            <td>User Added car.</td>
            <td></td>
        </tr>
        <tr>
            <td>User - </td>
            <td>[first_name]</td>
        </tr>
        <tr>
            <td>Listing - </td>
            <td>[listing_title]</td>
        </tr>
        <tr>
            <td>Listing URL - </td>
            <td>[listing_url]</td>
        </tr>
    </table>';

$subject = (get_option('add_car_pending_subject', '') != '') ? get_option('add_car_pending_subject', '') : 'Car await approval';
?>
<div class="etm-single-form">
    <h3>Add A Car Pending Template</h3>
    <input type="text" name="add_car_pending_subject" value="<?php echo esc_html($subject);?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
            $sc_arg = array(
                'textarea_rows' => apply_filters( 'etm-aac-sce-row', 10 ),
                'wpautop' => true,
                'media_buttons' => apply_filters( 'etm-aac-sce-media_buttons', false ),
                'tinymce' => apply_filters( 'etm-aac-sce-tinymce', true ),
            );

            wp_editor( $val, 'add_car_pending_template', $sc_arg );
            ?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
                foreach (getTemplateShortcodes('addACarPending') as $k => $val) {
                    echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
                }
                ?>
            </ul>
        </div>
		<?php $disabled = get_option('add_car_pending_disabled', ''); ?>
		<label for="add_car_pending_disabled" style="margin-top:20px;">
			<input
				type="checkbox"
				id="add_car_pending_disabled"
				name="add_car_pending_disabled"
				<?php if ( 'on' == $disabled ) echo 'checked="checked"'; ?>
				class="" />
			<span><?php _e("Disable Template", "motors");?></span>
		</label>
    </div>
</div>
