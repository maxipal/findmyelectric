<?php
$val = (get_option('listing_published_template', '') != '') ? stripslashes(get_option('listing_published_template', '')) :
    '<table>
        <tr>
            <td>New user Registered. Nickname: </td>
            <td>[user_login]</td>
        </tr>
    </table>'; //default table stuff goes here

$subject = (get_option('listing_published_subject', '') != '') ? get_option('listing_published_subject', '') : 'Listing Published';
?>
<div class="etm-single-form">
    <h3>Listing Published Template</h3>
    <input type="text" name="listing_published_subject" value="<?php echo esc_html($subject);?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
            $sc_arg = array(
                'textarea_rows' => apply_filters( 'etm-aac-sce-row', 10 ),
                'wpautop' => true,
                'media_buttons' => apply_filters( 'etm-aac-sce-media_buttons', false ),
                'tinymce' => apply_filters( 'etm-aac-sce-tinymce', true ),
            );

            wp_editor( $val, 'listing_published_template', $sc_arg );
            ?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
                foreach (getTemplateShortcodes('listingPublished') as $k => $val) {
                    echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
                }
                ?>
            </ul>
        </div>
		<?php $disabled = get_option('listing_published_disabled', ''); ?>
		<label for="listing_published_disabled" style="margin-top:20px;">
			<input
				type="checkbox"
				id="listing_published_disabled"
				name="listing_published_disabled"
				<?php if ( 'on' == $disabled ) echo 'checked="checked"'; ?>
				class="" />
			<span><?php _e("Disable Template", "motors");?></span>
		</label>
    </div>
</div>
