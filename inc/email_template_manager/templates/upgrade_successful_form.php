<?php
$val = (get_option('upgrade_successful_template', '') != '') ? stripslashes(get_option('upgrade_successful_template', '')) :
	'<h3>Hello [first_name], your upgrade from [old_package] to [new_package] was successful.</h3>
	<table>
        <tr>
            <td>Listing - </td>
            <td>[listing_title]</td>
        </tr>
        <tr>
            <td>Listing URL - </td>
            <td>[listing_url]</td>
        </tr>
    </table>';

$subject = (get_option('upgrade_successful_subject', '') != '') ? get_option('upgrade_successful_subject', '') : 'Your Listing Upgrade Successful';
?>
<div class="etm-single-form">
    <h3>Listing Upgrade Successful Template</h3>
    <input type="text" name="upgrade_successful_subject" value="<?php echo esc_html($subject);?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
			$sc_arg = array(
				'textarea_rows' => apply_filters( 'etm-aac-sce-row', 10 ),
				'wpautop' => true,
				'media_buttons' => apply_filters( 'etm-aac-sce-media_buttons', false ),
				'tinymce' => apply_filters( 'etm-aac-sce-tinymce', true ),
			);

			wp_editor( $val, 'upgrade_successful_template', $sc_arg );
			?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
				foreach (getTemplateShortcodes('upgradeSuccessful') as $k => $val) {
					echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
				}
				?>
            </ul>
        </div>
		<?php $disabled = get_option('upgrade_successful_disabled', ''); ?>
		<label for="upgrade_successful_disabled" style="margin-top:20px;">
			<input
				type="checkbox"
				id="upgrade_successful_disabled"
				name="upgrade_successful_disabled"
				<?php if ( 'on' == $disabled ) echo 'checked="checked"'; ?>
				class="" />
			<span><?php _e("Disable Template", "motors");?></span>
		</label>
    </div>
</div>
