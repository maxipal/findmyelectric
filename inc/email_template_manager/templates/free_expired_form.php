<?php
$val = (get_option('free_expired_template', '') != '') ? stripslashes(get_option('free_expired_template', '')) :
	'<table>
        <tr>
            <td>Your listing was expired</td>
            <td></td>
        </tr>
        <tr>
            <td>User - </td>
            <td>[first_name]</td>
        </tr>
        <tr>
            <td>Listing - </td>
            <td>[listing_title]</td>
        </tr>
        <tr>
            <td>Upgrade from free plan URL - </td>
            <td>[free_upgrade_link]</td>
        </tr>
    </table>';

$subject = (get_option('free_expired_subject', '') != '') ? get_option('free_expired_subject', '') : 'Your listing was expired';
?>
<div class="etm-single-form">
    <h3>Free Expired Listing Template</h3>
    <input type="text" name="free_expired_subject" value="<?php echo esc_html($subject);?>" class="full_width" />
    <div class="lr-wrap">
        <div class="left">
            <?php
			$sc_arg = array(
				'textarea_rows' => apply_filters( 'etm-aac-sce-row', 10 ),
				'wpautop' => true,
				'media_buttons' => apply_filters( 'etm-aac-sce-media_buttons', false ),
				'tinymce' => apply_filters( 'etm-aac-sce-tinymce', true ),
			);

			wp_editor( $val, 'free_expired_template', $sc_arg );
			?>
        </div>
        <div class="right">
            <h4>Shortcodes</h4>
            <ul>
                <?php
				foreach (getTemplateShortcodes('freeExpired') as $k => $val) {
					echo "<li id='{$k}'><input type='text' value='{$val}' class='auto_select' /></li>";
				}
				?>
            </ul>
        </div>
		<?php $disabled = get_option('free_expired_disabled', ''); ?>
		<label for="free_expired_disabled" style="margin-top:20px;">
			<input
				type="checkbox"
				id="free_expired_disabled"
				name="free_expired_disabled"
				<?php if ( 'on' == $disabled ) echo 'checked="checked"'; ?>
				class="" />
			<span><?php _e("Disable Template", "motors");?></span>
		</label>
    </div>
</div>
