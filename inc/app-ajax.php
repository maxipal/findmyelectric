<?php

remove_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');
remove_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');
add_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_child_ajax_get_seller_phone');
add_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_child_ajax_get_seller_phone');

function stm_child_ajax_get_seller_phone()
{
	check_ajax_referer('stm_ajax_get_seller_phone', 'security');
	$recaptchaValidation = recaptchaValidation($_GET['recaptcha-token']);

	if ( $recaptchaValidation['success'] ) {
		$response['phone'] = get_user_meta($_GET["phone_owner_id"], 'stm_phone', true);
        $response['recaptcha'] = $recaptchaValidation;
	} else {
		$response['error'] = __('Recaptcha validation failed!', 'motors-child');
	}

	wp_send_json($response);
	exit;
}


function stm_check_upload_image_exists($filename) {
	global $wpdb;
	$count = 0;
	$path = pathinfo($filename);
	$query = $wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE guid LIKE %s", "%{$path['filename']}%");
	$id = intval($wpdb->get_var($query));
	return $id;
}


add_action( 'wp_ajax_custom_photo_upload', 'custom_photo_upload' );
add_action( 'wp_ajax_nopriv_custom_photo_upload', 'custom_photo_upload' );
function custom_photo_upload()
{
    $updating = !empty($_POST['stm_edit']) and $_POST['stm_edit'] == 'update';
    $error = false;
	$response = [];

    $files_approved = array();

    if (!empty($_FILES)) {

        $max_file_size = apply_filters('stm_listing_media_upload_size', 1024 * 10000); /*4mb is highest media upload here*/

        foreach ($_FILES['files']['name'] as $f => $name) {
           if ($_FILES['files']['error'][$f] != UPLOAD_ERR_OK) {
                $error = true;
            } else {
                // Check if the file being uploaded is in the allowed file types
                $check_image = @getimagesize($_FILES['files']['tmp_name'][$f]);
                if ($_FILES['files']['size'][$f] > $max_file_size) {
                    $response['message'] = esc_html__('Sorry, image is too large', 'stm_vehicles_listing') . ': ' . $name;
                    $error = true;
                } elseif (empty($check_image)) {
                    $response['message'] = esc_html__('Sorry, image has invalid format', 'stm_vehicles_listing') . ': ' . $name;
                    $error = true;
                } else {
                    $tmp_name = $_FILES['files']['tmp_name'][ $f ];
                    $error = $_FILES['files']['error'][ $f ];
                    $type = $_FILES['files']['type'][ $f ];
                    $files_approved[$f] = compact('name', 'tmp_name', 'type', 'error');
                }
            }
        }

    }

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );


//	require_once(get_stylesheet_directory()."/vendor/autoload.php");
//	\Tinify\setKey("vqxd1qsFpg436v1pYkmqT61zvlHQx1dC");


    foreach ($files_approved as $f => $file) {
    	// Optimized Image with tinyPNG
//		$source = \Tinify\fromFile($file['tmp_name']);
//		$source->toFile($file['tmp_name']."_optimized");
//		$file['tmp_name'] = $file['tmp_name']."_optimized";
//    	$exists = stm_check_upload_image_exists($file['name']);
//    	if($exists){
//			$response = [
//				'src' => wp_get_attachment_image_src($exists, 'stm-img-796-466'),
//				'thumb' => wp_get_attachment_image_src($exists, 'stm-img-255-160'),
//				'index' => $_POST['lastUploadedIndex'],
//				'id'   => $exists,
//				'exists' => true
//			];
//			wp_send_json($response);
//		}else{
			$uploaded = wp_handle_upload($file, array('action' => 'custom_photo_upload'));
//		}

        if ($uploaded['error']) {
            $response['errors'][ $file['name'] ] = $uploaded;
            continue;
        }

        $filetype = wp_check_filetype(basename($uploaded['file']), null);

        // Insert attachment to the database
        $attach_id = wp_insert_attachment(array(
            'guid' => $uploaded['url'],
            'post_mime_type' => $filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($uploaded['file'])),
            'post_content' => '',
            'post_status' => 'inherit',
        ), $uploaded['file']);

		$attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );

        add_post_meta($attach_id, 'attachment_to_delete', $attach_id);

        update_post_meta($attach_id, 'blob', $_POST['blob']);

		$response = [
			'src' => wp_get_attachment_image_src($attach_id, 'stm-img-796-466'),
			'thumb' => wp_get_attachment_image_src($attach_id, 'stm-img-255-160'),
			'index' => $_POST['lastUploadedIndex'],
			'id'   => $attach_id
		];
    }

    wp_send_json($response);
    exit;
}



if ( !function_exists( 'stm_get_add_edit_page_url' ) ) {
	function stm_get_add_edit_page_url( $edit = '', $post_id = '' )
	{
		if ( $edit == 'edit' ) {
			$stm_user_active_subscriptions = stm_user_active_subscriptions(false, get_current_user_id() );
			if( !empty($stm_user_active_subscriptions) ){
				$page_id = get_theme_mod( 'edit_listing_page', 1755 );
			}else{
				$page_id = get_theme_mod( 'user_add_car_page', 1755 );
			}
		}

		$page_link = '';

		if ( !empty( $page_id ) ) {
			$page_id = stm_motors_wpml_is_page( $page_id );

			$page_link = get_permalink( $page_id );
		}

		if ( $edit == 'edit' and !empty( $post_id ) ) {
			return esc_url( add_query_arg( array( 'edit_car' => '1', 'item_id' => intval( $post_id ) ), $page_link ) );
		} else {
			return esc_url( $page_link );
		}
	}
}
