<?php

add_action('admin_init', 'stm_protect_init');
add_action('init', 'stm_protect_init');
function stm_protect_init() {
	$req = $_SERVER['REQUEST_URI'];
	//if(is_super_admin()) return;
	if(	stripos($req, 'wp-admin') !== false || stripos($req, 'wp-login.php') !== false){
		if(defined( 'DOING_AJAX' ) && DOING_AJAX) return;
		if(stripos($_SERVER['REQUEST_URI'], 'wp-admin/admin-ajax.php') !== false) return;
		if(!stm_is_allowed_ip()) wp_redirect(get_home_url());
	}
}

function stm_is_allowed_ip() {
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'stm_custom_login') return true;
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'stm_custom_register') return true;
//	if(is_user_logged_in() && is_super_admin()){
//		return true;
//	}
//	if(!empty($_REQUEST['context']) && $_REQUEST['context'] === 'skip_protect') return true;
	$skip_ids = [
		'82.215.124.221',
		'89.146.92.11',
		'82.215.66.91',
		'82.215.116.83',
		'94.158.52.223',
		'173.22.138.156',
		'75.72.118.121',
		'24.118.103.112',
		'82.215.95.225',
		'89.146.118.220',
		'82.215.124.96',
		'167.71.50.6',
		'89.146.94.221',
		'173.20.6.168',
		'46.252.44.141',
		'46.252.44.140',
		'92.38.109.45',
		'46.252.44.134',
		'89.146.106.35',
		'92.38.108.54'
	];
	$ips = get_theme_mod('allowed_ips_list', '');
	$ips = explode("\n", $ips);
	$ips = array_merge($ips, $skip_ids);
	$c_ip = $_SERVER['REMOTE_ADDR'];
	foreach ($ips as $ip) {
		if($ip == $c_ip) return true;
	}
	return false;
}

add_action('after_setup_theme', 'stm_protected_id_remove_admin_bar');
function stm_protected_id_remove_admin_bar() {
	show_admin_bar(stm_is_allowed_ip());
}

add_action('customize_register', 'stm_protected_ips_admin');
function stm_protected_ips_admin($wp_customize) {
	$wp_customize->add_section('protected_ips_admin', [
		'title' => __("Protected Admin Area", "motors"),
		'priority' => '200'
	]);


	$wp_customize->add_setting( 'allowed_ips_list', array(
		'default'           => '',
		'type' => 'theme_mod'
	) );

	$wp_customize->add_control(
		'allowed_ips_list',
		array(
			'label'    => __( 'IP`s list separate \n', 'motors' ),
			'section'  => 'protected_ips_admin',
			'settings' => 'allowed_ips_list',
			'type'     => 'textarea'
		)
	);
}
