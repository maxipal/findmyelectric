<?php

if (function_exists('vc_map')) {
    add_action('init', 'register_child_elements');
}

function register_child_elements(){


    $plan_args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish'
    );

    $products = new WP_Query($plan_args);
    $products_array = array(__('Choose plan', 'motors') => '');
    if ($products->have_posts()) {
        while ($products->have_posts()) {
            $products->the_post();
            $title = get_the_title();
            $id = get_the_ID();
            $products_array[$title] = $id;
        }
    }

    $stm_pt_params = array();

	$stm_pt_params[] = [
		'type' => 'textfield',
		'heading' => __('Features title', 'motors'),
		'param_name' => 'features_title',
	];

    for ($i = 1; $i <= 4; $i++) {
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Title', 'motors'),
            'param_name' => 'pt_' . $i . '_title',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Subtitle', 'motors'),
            'param_name' => 'pt_' . $i . '_subtitle',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );
		$stm_pt_params[] = array(
			'type' => 'textfield',
			'heading' => __('Badge', 'motors'),
			'param_name' => 'pt_' . $i . '_badge',
			'group' => sprintf(__("Table %s", 'motors'), $i)
		);
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Plan Price', 'motors'),
            'param_name' => 'pt_' . $i . '_price',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );


        $stm_pt_params[] = array(
            'type' => 'param_group',
            'heading' => __('Features', 'motors'),
            'param_name' => 'pt_' . $i . '_features',
            'value' => urlencode(json_encode(array(
                array(
                    'label' => __('Value', 'motors'),
                    'value' => '',
                ),
                array(
                    'label' => __('Label', 'motors'),
                    'value' => '',
                )
            ))),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Value', 'motors'),
                    'param_name' => 'pt_' . $i . '_feature_title',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Label', 'motors'),
                    'param_name' => 'pt_' . $i . '_feature_text',
                    'admin_label' => true,
                )
            ),
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );

        $stm_pt_params[] = array(
            'type' => 'dropdown',
            'heading' => __('Plan add to cart (Plan ID)', 'motors'),
            'param_name' => 'pt_' . $i . '_add_to_cart',
            'value' => $products_array,
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );

		$stm_pt_params[] = array(
			'type' => 'textfield',
			'heading' => __('Button caption', 'motors'),
			'param_name' => 'pt_' . $i . '_btn_caption',
			'group' => sprintf(__("Table %s", 'motors'), $i)
		);
		$stm_pt_params[] = array(
			'type' => 'checkbox',
			'heading' => __('Grey Button', 'motors'),
			'param_name' => 'pt_' . $i . '_grey_btn',
			'group' => sprintf(__("Table %s", 'motors'), $i)
		);
		$stm_pt_params[] = array(
			'type' => 'checkbox',
			'heading' => __('Featured?', 'motors'),
			'param_name' => 'pt_' . $i . '_featured_chk',
			'group' => sprintf(__("Table %s", 'motors'), $i)
		);
    }

    vc_map(array(
        'name' => __('STM Tesla Pricing Tables', 'motors'),
        'base' => 'stm_tesla_pricing',
        'category' => __('', 'motors'),
        'params' => $stm_pt_params
    ));

	vc_map(array(
		'name' => __('STM Tesla Pricing Tables as Elf', 'motors'),
		'base' => 'stm_tesla_pricing_elf',
		'category' => __('', 'motors'),
		'params' => $stm_pt_params
	));

    vc_map(array(
        'name' => __('Algolia Search Filter', 'motors-child'),
        'base' => 'stm_algolia_filter',
        'category' => __('STM', 'motors'),
        'params' => array(
			array(
				'type' => 'param_group',
				'heading' => __('Items', 'motors'),
				'param_name' => 'items',
				'params' => array(
					array(
						'type' => 'stm_autocomplete_vc',
						'heading' => __('Pre-Selected category', 'motors'),
						'param_name' => 'taxonomy',
						'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)', 'motors')
					),
				),
			),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'motors'),
                'param_name' => 'css',
                'group' => __('Design options', 'motors')
            )
        )
    ));
    vc_map(array(
        'name' => __('Register Form', 'motors-child'),
        'base' => 'stm_register_form',
        'icon' => 'stm_add_a_car',
        'params' => array(
            array(
                'type' => 'vc_link',
                'heading' => __('Link', 'motors'),
                'param_name' => 'link'
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose redirect page', 'motors-child' ),
                'param_name'  => 'redirect_page',
                'description' => 'Note* if field is empty it will automatically redirect to author page after successful sign up.',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose Sing-In page', 'motors-child' ),
                'param_name'  => 'login_redirect_page',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
        ),
    ));

    vc_map(array(
        'name' => __('Login Form', 'motors-child'),
        'base' => 'stm_login_form',
        'icon' => 'stm_add_a_car',
        'params' => array(
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose redirect page', 'motors-child' ),
                'param_name'  => 'redirect_page',
                'description' => 'Note* if field is empty it will automatically redirect to author page after successful sign in.',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose Sing-Up page', 'motors-child' ),
                'param_name'  => 'login_redirect_page',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
        ),
    ));
	$stm_filter_options = [];
	//Car listing Tabs MODEL S3XY
    vc_map(array(
        'name' => __('STM Car Listing S3XY', 'motors'),
        'base' => 'stm_car_listing_tabbed_s3xy',
        'icon' => 'stm_car_listing_tabbed_s3xy',
        'category' => __('STM', 'motors'),
        'params' => array(
            array(
                'type' => 'colorpicker',
                'heading' => __('Top part background color', 'motors'),
                'param_name' => 'top_part_bg',
                'value' => '#232628'
            ),
            array(
                'type' => 'stm_autocomplete_vc',
                'heading' => __('Select category', 'motors'),
                'param_name' => 'taxonomy',
                'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Tab affix', 'motors'),
                'param_name' => 'tab_affix',
                'value' => __('cars', 'motors'),
                'description' => __('This will appear after category name', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Tab preffix', 'motors'),
                'param_name' => 'tab_preffix',
                'value' => '',
                'description' => __('This will appear before category name', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Cars per load', 'motors'),
                'param_name' => 'per_page',
                'description' => __('-1 will show all cars from category', 'motors')
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Enable ajax loading', 'motors'),
                'param_name' => 'enable_ajax_loading',
            ),
            array(
                'type' => 'textarea_html',
                'heading' => __('Text', 'motors'),
                'param_name' => 'content'
            ),
	        array(
		        'type' => 'textfield',
		        'heading' => __('Found cars prefix', 'motors'),
		        'param_name' => 'found_cars_prefix',
		        'value' => __('cars', 'motors'),
		        'description' => __('This will appear after found cars count', 'motors')
	        ),
            //Search tab Start
            array(
                'type' => 'checkbox',
                'heading' => __('Enable search', 'motors'),
                'param_name' => 'enable_search',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Search label', 'motors'),
                'param_name' => 'search_label',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Search label icon', 'motors'),
                'param_name' => 'search_icon',
                'value' => '',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Number of filter columns', 'motors'),
                'param_name' => 'filter_columns_number',
                'value' => array(
                    '6' => '6',
                    '4' => '4',
                    '3' => '3',
                    '2' => '2',
                    '1' => '1'
                ),
                'std' => '2',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Select Filter options', 'motors'),
                'param_name' => 'filter_selected',
                'value' => $stm_filter_options,
                'group' => __('Search Options', 'motors')
            ),
            //Call to action
            array(
                'type' => 'checkbox',
                'heading' => __('Enable call-to-action', 'motors'),
                'param_name' => 'enable_call_to_action',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Call to action label', 'motors'),
                'param_name' => 'call_to_action_label',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Call to action label icon', 'motors'),
                'param_name' => 'call_to_action_icon',
                'value' => '',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Call to action label right', 'motors'),
                'param_name' => 'call_to_action_label_right',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Call to action label icon right', 'motors'),
                'param_name' => 'call_to_action_icon_right',
                'value' => '',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __('Call to action background color', 'motors'),
                'param_name' => 'call_to_action_color',
                'value' => '#fab637',
                'group' => __('Search Options', 'motors')
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __('Call to action text color', 'motors'),
                'param_name' => 'call_to_action_text_color',
                'value' => '#fff',
                'group' => __('Search Options', 'motors')
            ),
            //Search tab End

            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'motors'),
                'param_name' => 'css',
                'group' => __('Design options', 'motors')
            ),
        ),
    ));
	//Car listing Tabs MODEL EXTENDED
	vc_map(array(
		'name' => __('STM Car Listing tabs 2', 'motors'),
		'base' => 'stm_car_listing_tabbed_ex',
		'icon' => 'stm_car_listing_tabbed_ex',
		'category' => __('STM', 'motors'),
		'params' => array(
			array(
				'type' => 'colorpicker',
				'heading' => __('Top part background color', 'motors'),
				'param_name' => 'top_part_bg',
				'value' => '#232628'
			),
			array(
				'type' => 'stm_autocomplete_vc',
				'heading' => __('Based category', 'motors'),
				'param_name' => 'based_taxonomy',
				'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)', 'motors')
			),
			array(
				'type' => 'param_group',
				'heading' => __('Tabs', 'motors'),
				'param_name' => 'tabs',
				'params' => array(
					array(
						'type' => 'stm_autocomplete_vc',
						'heading' => __('Select category', 'motors'),
						'param_name' => 'taxonomy',
						'description' => __('Type slug of the category (don\'t delete anything from autocompleted suggestions)', 'motors')
					),
				),
			),
			array(
				'type' => 'textfield',
				'heading' => __('Tab affix', 'motors'),
				'param_name' => 'tab_affix',
				'value' => __('cars', 'motors'),
				'description' => __('This will appear after category name', 'motors')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Tab preffix', 'motors'),
				'param_name' => 'tab_preffix',
				'value' => '',
				'description' => __('This will appear before category name', 'motors')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Cars per load', 'motors'),
				'param_name' => 'per_page',
				'description' => __('-1 will show all cars from category', 'motors')
			),
			array(
				'type' => 'checkbox',
				'heading' => __('Enable ajax loading', 'motors'),
				'param_name' => 'enable_ajax_loading',
			),
			array(
				'type' => 'textarea_html',
				'heading' => __('Text', 'motors'),
				'param_name' => 'content'
			),
			array(
				'type' => 'textfield',
				'heading' => __('Found cars prefix', 'motors'),
				'param_name' => 'found_cars_prefix',
				'value' => __('cars', 'motors'),
				'description' => __('This will appear after found cars count', 'motors')
			),
			//Search tab Start
			array(
				'type' => 'checkbox',
				'heading' => __('Enable search', 'motors'),
				'param_name' => 'enable_search',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Search label', 'motors'),
				'param_name' => 'search_label',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'iconpicker',
				'heading' => __('Search label icon', 'motors'),
				'param_name' => 'search_icon',
				'value' => '',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'dropdown',
				'heading' => __('Number of filter columns', 'motors'),
				'param_name' => 'filter_columns_number',
				'value' => array(
					'6' => '6',
					'4' => '4',
					'3' => '3',
					'2' => '2',
					'1' => '1'
				),
				'std' => '2',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'checkbox',
				'heading' => __('Select Filter options', 'motors'),
				'param_name' => 'filter_selected',
				'value' => $stm_filter_options,
				'group' => __('Search Options', 'motors')
			),
			//Call to action
			array(
				'type' => 'checkbox',
				'heading' => __('Enable call-to-action', 'motors'),
				'param_name' => 'enable_call_to_action',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Call to action label', 'motors'),
				'param_name' => 'call_to_action_label',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'iconpicker',
				'heading' => __('Call to action label icon', 'motors'),
				'param_name' => 'call_to_action_icon',
				'value' => '',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'textfield',
				'heading' => __('Call to action label right', 'motors'),
				'param_name' => 'call_to_action_label_right',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'iconpicker',
				'heading' => __('Call to action label icon right', 'motors'),
				'param_name' => 'call_to_action_icon_right',
				'value' => '',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'colorpicker',
				'heading' => __('Call to action background color', 'motors'),
				'param_name' => 'call_to_action_color',
				'value' => '#fab637',
				'group' => __('Search Options', 'motors')
			),
			array(
				'type' => 'colorpicker',
				'heading' => __('Call to action text color', 'motors'),
				'param_name' => 'call_to_action_text_color',
				'value' => '#fff',
				'group' => __('Search Options', 'motors')
			),
			//Search tab End

			array(
				'type' => 'css_editor',
				'heading' => __('Css', 'motors'),
				'param_name' => 'css',
				'group' => __('Design options', 'motors')
			),
		),
	));

    $attributes = array(
        'type' => 'dropdown',
        'heading' => __('Count of listings', 'motors'),
        'param_name' => 'count_of_items',
        'value' => array(
            __('4', 'motors') => '4',
            __('8', 'motors') => '8',
            __('12', 'motors') => '12',
        ),
        'std' => '4'
    );
    vc_add_param('stm_special_offers', $attributes);


}
add_filter( 'vc_autocomplete_stm_login_form_login_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_redirect_page_render', 'all_post_suggester_render', 10, 1 );

add_filter( 'vc_autocomplete_stm_register_form_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_register_form_login_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );

add_filter( 'vc_autocomplete_stm_register_form_redirect_page_render', 'all_post_suggester_render', 10, 1 );
add_filter( 'vc_autocomplete_stm_register_form_login_redirect_page_render', 'all_post_suggester_render', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_login_redirect_page_render', 'all_post_suggester_render', 10, 1 );

function posts_auto_complete_suggetster( $query) {
    global $wpdb;
    $post_id = (int) $query;
    $post_results = $wpdb->get_results( $wpdb->prepare( "SELECT a.ID AS id, a.post_title AS title FROM {$wpdb->posts} AS a
WHERE a.post_type = 'page'  AND a.post_status != 'trash' AND ( a.ID = '%d' OR a.post_title LIKE '%%%s%%' )", $post_id > 0 ? $post_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );
    $results = array();
    if ( is_array( $post_results ) && ! empty( $post_results ) ) {
        foreach ( $post_results as $value ) {
            $data = array();
            $data['value'] = $value['id'];
            $data['label'] = $value['title'];
            $results[] = $data;
        }
    }
    return $results;
}

function all_post_suggester_render( $query ) {
    $query = trim( $query['value'] );

    // get value from requested
    if ( ! empty( $query ) ) {
        $post_object = get_post( (int) $query );
        if ( is_object( $post_object ) ) {
            $post_title = $post_object->post_title;
            $post_id = $post_object->ID;
            $data = array();
            $data['value'] = $post_id;
            $data['label'] = $post_title;
            return ! empty( $data ) ? $data : false;
        }
        return false;
    }
    return false;
}


if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Stm_Tesla_Pricing extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Algolia_Filter extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Register_Form extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Login_Form extends WPBakeryShortCode
    {
    }
	class WPBakeryShortCode_Stm_Car_Listing_Tabbed_S3xy extends WPBakeryShortCode
    {
    }
	class WPBakeryShortCode_Stm_Car_Listing_Tabbed_Ex extends WPBakeryShortCode
	{
	}
}
