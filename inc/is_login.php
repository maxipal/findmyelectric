<?php

remove_action('wp_ajax_stm_custom_login', 'stm_custom_login');
remove_action('wp_ajax_nopriv_stm_custom_login', 'stm_custom_login');
add_action('wp_ajax_stm_custom_login', 'stm_ajax_account_login');
add_action('wp_ajax_nopriv_stm_custom_login', 'stm_ajax_account_login');
remove_action('wp_ajax_stm_custom_register', 'stm_custom_register');
remove_action('wp_ajax_nopriv_stm_custom_register', 'stm_custom_register');
add_action('wp_ajax_stm_custom_register', 'stm_account_register');
add_action('wp_ajax_nopriv_stm_custom_register', 'stm_account_register');
remove_action( 'wp_ajax_stm_restore_password', 'stm_restore_password' );
remove_action( 'wp_ajax_nopriv_stm_restore_password', 'stm_restore_password' );
add_action( 'wp_ajax_stm_restore_password', 'stm_child_restore_password' );
add_action( 'wp_ajax_nopriv_stm_restore_password', 'stm_child_restore_password' );


function stm_ajax_account_login()
{
	$errors = array();
	$recaptcha = $_POST['recaptcha_token'] ? $_POST['recaptcha_token'] : '';
	if (empty($_POST['stm_user_login'])) {
		$errors['stm_user_login'] = true;
	} else {
		$username = sanitize_text_field($_POST['stm_user_login']);
	}

	if (empty($_POST['stm_user_password'])) {
		$errors['stm_user_password'] = true;
	} else {
		$password = $_POST['stm_user_password'];
	}

	if(isset($_POST['redirect_path'])) {
		$redirect_path = $_POST['redirect_path'];
	}

	if(isset($_POST['redirect_page'])){
		$redirect_path = get_permalink($_POST['redirect_page']);
	}

	$remember = false;

	if (!empty($_POST['stm_remember_me']) and $_POST['stm_remember_me'] == 'on') {
		$remember = true;
	}

	if (!empty($_POST['redirect']) and $_POST['redirect'] == 'disable') {
		$redirect = false;
	} else {
		$redirect = true;
	}

	$recaptcha_validation = recaptchaValidation($recaptcha);
	if (!$recaptcha_validation['success']) {
		$errors['captcha'] = true;
		$response['message'] = recaptcha_error_message();
	}else {
        $response['recaptcha'] = $recaptcha_validation;
    }
	//AUTH
	$errors = apply_filters("stm_user_login", $errors, $username);

	if (empty($errors)) {
		if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
			$user = get_user_by('email', $username);
		} else {
			$user = get_user_by('login', $username);
		}

		if ($user) {
			$username = $user->data->user_login;
		}

		$creds = array();
		$creds['user_login'] = $username;
		$creds['user_password'] = $password;
		$creds['remember'] = $remember;
		$secure_cookie = is_ssl() ? true : false;


		$user = wp_signon($creds, $secure_cookie);
		if (is_wp_error($user)) {
			$response['message'] = esc_html__('Wrong Username or Password', 'stm_vehicles_listing');
		} else {
			if ($redirect) {
				$response['message'] = esc_html__('Successfully logged in. Redirecting...', 'stm_vehicles_listing');

				$wpmlUrl = (!empty($redirect_path)) ? $redirect_path : get_author_posts_url($user->ID);

				if (class_exists('PMXI_Plugin') && isset($_POST['current_lang'])) {
					$wpmlUrl = apply_filters( 'wpml_permalink', $wpmlUrl, $_POST['current_lang'], true );
				}

				$response['redirect_url'] = $wpmlUrl;
			} else {
				ob_start();
				stm_add_a_car_user_info('', '', '', $user->ID);
				$restricted = false;
				$restrictions = stm_get_post_limits($user->ID);

				if ($restrictions['posts'] < 1) {
					$restricted = true;
				}

				$response['restricted'] = $restricted;
				$response['user_html'] = ob_get_clean();
			}
		}
	} else {
		$response['message'] = esc_html__('Please fill in the required fields. (Items marked with * are required.)', 'stm_vehicles_listing');
		if (!$recaptcha_validation['success']) {
			$response['message'] = recaptcha_error_message();
		}
	}

	$response['errors'] = $errors;

	$response = json_encode($response);
	echo apply_filters('stm_filter_custom_login', $response);
	exit;
}

//function for the register page below
function stm_account_register()
{
	$response = array();
	$errors = array();
	$recaptcha = $_POST['recaptcha_token'] ? $_POST['recaptcha_token'] : '';

	if (empty($_POST['stm_nickname'])) {
		$errors['stm_nickname'] = true;
	} else {
		$user_login = sanitize_text_field($_POST['stm_nickname']);
	}

	if (empty($_POST['stm_user_first_name'])) {
		$errors['stm_user_first_name'] = true;
	} else {
		$user_name = sanitize_text_field($_POST['stm_user_first_name']);
	}

	if (empty($_POST['stm_user_last_name'])) {
		$errors['stm_user_last_name'] = true;
	} else {
		$user_lastname = sanitize_text_field($_POST['stm_user_last_name']);
	}

	$recaptcha_validation = recaptchaValidation($recaptcha);
	if (!$recaptcha_validation['success']) {
		$errors['captcha'] = true;
		$response['message'] = recaptcha_error_message();
	}else {
        $response['recaptcha'] = $recaptcha_validation;
    }

	if (empty($_POST['stm_user_phone'])) {
		$errors['stm_user_phone'] = true;
	} else {
		$user_phone = sanitize_text_field($_POST['stm_user_phone']);
	}

	if (!is_email($_POST['stm_user_mail'])) {
		$errors['stm_user_mail'] = true;
	} else {
		$user_mail = sanitize_email($_POST['stm_user_mail']);
	}

	if (empty($_POST['stm_user_password'])) {
		$errors['stm_user_password'] = true;
	} else {
		$user_pass = $_POST['stm_user_password'];
	}

	if (!empty($_POST['redirect']) and $_POST['redirect'] == 'disable') {
		$redirect = false;
	} else {
		$redirect = true;
	}

	$demo = stm_is_site_demo_mode();
	if ($demo) {
		$errors['demo'] = true;
	}

	if (empty($errors)) {
		$user_data = array(
			'user_login' => $user_login,
			'user_pass' => $user_pass,
			'first_name' => $user_name,
			'last_name' => $user_lastname,
			'user_email' => $user_mail
		);

		$user_id = wp_insert_user($user_data);

		if (!is_wp_error($user_id)) {
			update_user_meta($user_id, 'stm_phone', $user_phone);
			update_user_meta($user_id, 'stm_show_email', 'show');

			// When using caching plugins user sessions are not working properly
			// deleting user meta cache should solve this issue
			wp_cache_delete( $user_id, 'user_meta');

			wp_set_current_user($user_id, $user_login);
			wp_set_auth_cookie($user_id);
			do_action('wp_login', $user_login, new WP_User($user_id));


			if ($redirect) {
				$response['message'] = esc_html__('Congratulations! You have been successfully registered. Redirecting...', 'stm_vehicles_listing');

				$redirect_path = $_POST['redirect_page'] ? get_permalink($_POST['redirect_page']) : get_author_posts_url($user_id);

				$response['redirect_url'] = $redirect_path . '?user_id='. $user_id;
			} else {
				ob_start();
				stm_add_a_car_user_info($user_login, $user_name, $user_lastname, $user_id);
				$restricted = false;
				$restrictions = stm_get_post_limits($user_id);

				if ($restrictions['posts'] < 1 && stm_enablePPL()) {
					$restricted = true;
				}

				$response['restricted'] = $restricted;
				$response['user_html'] = ob_get_clean();
			}

			//AUTH
			do_action( 'stm_register_new_user', $user_id );
			if((int)get_option('users_can_register')){
				$response['message'] = esc_html__('Congratulations! You have been successfully registered. Please, activate your account.', 'stm_vehicles_listing');
			}else{
				wp_set_current_user($user_id, $user_login);
				wp_set_auth_cookie($user_id);
				do_action('wp_login', $user_login, new WP_User($user_id));
				if ($redirect) {
					$response['message'] = esc_html__('Congratulations! You have been successfully registered. Redirecting...', 'stm_vehicles_listing');

					$redirect_path = $_POST['redirect_page'] ? get_permalink($_POST['redirect_page']) .'?user_id='. $user_id : get_author_posts_url($user_id);

                    $response['redirect_url'] = $redirect_path;
				} else {
					ob_start();
					stm_add_a_car_user_info($user_login, $user_name, $user_lastname, $user_id);
					$restricted = false;
					$restrictions = stm_get_post_limits($user_id);

					if ($restrictions['posts'] < 1 && stm_enablePPL()) {
						$restricted = true;
					}

					$response['restricted'] = $restricted;
					$response['user_html'] = ob_get_clean();
				}
			}

			add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');

			/*Mail admin*/
			$to = get_bloginfo('admin_email');
			$subject = generateSubjectView('new_user', array('user_login' => $user_login));
			$body = generateTemplateView('new_user', array('user_login' => $user_login, 'email' => $user_mail, 'first_name' => $user_name, 'last_name' => $user_lastname));

			wp_mail($to, $subject, $body);

			/*Mail user*/
			$subjectUser = generateSubjectView('welcome', array('user_login' => $user_login));
			$bodyUser = generateTemplateView('welcome', array('user_login' => $user_login));
			wp_mail($user_mail, $subjectUser, $bodyUser);

			remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');

		} else {
			$response['message'] = $user_id->get_error_message();
			$response['user'] = $user_id;
		}
	} else {

		if ($demo) {
			$response['message'] = esc_html__('Site is on demo mode', 'stm_vehicles_listing');
		} else {
			$response['message'] = esc_html__('Please fill in the required fields. (Items marked with * are required.)', 'stm_vehicles_listing');
			if (!$recaptcha_validation['success']) {
				$response['message'] = recaptcha_error_message();
			}
		}
	}


	$response['errors'] = $errors;
	$response = json_encode($response);
	echo apply_filters('stm_filter_custom_register', $response);
	exit;
}

function stm_child_restore_password()
{
	check_ajax_referer( 'stm_restore_password', 'security' );

	$response = array();
	$recaptcha = $_POST['recaptcha_token'] ? $_POST['recaptcha_token'] : '';
	$errors = array();

	if ( empty( $_POST['stm_user_login'] ) ) {
		$errors['stm_user_login'] = true;
	} else {
		$username = sanitize_text_field( $_POST['stm_user_login'] );
	}

	$stm_link_send_to = '';

	if ( !empty( $_POST['stm_link_send_to'] ) ) {
		$stm_link_send_to = esc_url( $_POST['stm_link_send_to'] );
	}

	$demo = stm_is_site_demo_mode();

	if ( $demo ) {
		$errors['demo'] = true;
	}

	$recaptcha_validation = recaptchaValidation($recaptcha);
	if (!$recaptcha_validation['success']) {
		$errors['captcha'] = true;
		$response['message'] = recaptcha_error_message();
	}else {
        $response['recaptcha'] = $recaptcha_validation;
    }

	if ( empty( $errors ) ) {
		if ( filter_var( $username, FILTER_VALIDATE_EMAIL ) ) {
			$user = get_user_by( 'email', $username );
		} else {
			$user = get_user_by( 'login', $username );
		}

		if ( !$user ) {
			$response['message'] = esc_html__( 'User not found', 'motors' );
		} else {
			$hash = stm_media_random_affix( 20 );
			$user_id = $user->ID;
			$stm_link_send_to = add_query_arg( array( 'user_id' => $user_id, 'hash_check' => $hash ), $stm_link_send_to );
			update_user_meta( $user_id, 'stm_lost_password_hash', $hash );

			/*Sending mail*/
			$to = $user->data->user_email;

			$args = array(
				'password_content' => $stm_link_send_to
			);

			$subject = generateSubjectView( 'password_recovery', $args );
			$body = generateTemplateView( 'password_recovery', $args );

			do_action( 'stm_wp_mail', $to, $subject, $body, '' );

			$response['message'] = esc_html__( 'Instructions send on your email', 'motors' );
		}

	} else {
		if ( $demo ) {
			$response['message'] = esc_html__( 'Site is on demo mode.', 'motors' );
		} else {
			$response['message'] = esc_html__( 'Please fill in the required fields. (Items marked with * are required.)', 'motors' );
			if (!$recaptcha_validation['success']) {
				$response['message'] = recaptcha_error_message();
			}
		}
	}

	$response['errors'] = $errors;

	wp_send_json( $response );
	exit;
}

function display_user_full_name(WP_User $user) {
	if (!$user->first_name || !$user->last_name) {
		 $name = $user->user_nicename;
	}else{
		$name = $user->first_name . ' ' . substr($user->last_name, 0, 1) . '.';
	}

	return $name;
}

remove_action('wp_ajax_stm_listings_ajax_save_user_data', 'stm_listings_ajax_save_user_data');
add_action('wp_ajax_stm_listings_ajax_save_user_data', 'stm_child_listings_ajax_save_user_data');

function stm_child_listings_ajax_save_user_data()
{

    $response = array();

    if (!is_user_logged_in()) {
        die('You are not logged in');
    }

    $got_error_validation = false;
    $error_msg = esc_html__('Settings Saved.', 'stm_vehicles_listing');

    $user_current = wp_get_current_user();
    $user_id = $user_current->ID;
    $user = stm_get_user_custom_fields($user_id);

    /*Get current editing values*/
    $user_mail = stm_listings_input('stm_email', $user['email']);
    $user_mail = sanitize_email($user_mail);
    /*Socials*/
    $socs = array('facebook', 'twitter', 'linkedin', 'youtube');
    $socials = array();
    if (empty($user['socials'])) {
        $user['socials'] = array();
    }
    foreach ($socs as $soc) {
        if (empty($user['socials'][$soc])) {
            $user['socials'][$soc] = '';
        }
        $socials[$soc] = stm_listings_input('stm_user_' . $soc, $user['socials'][$soc]);
    }

    $password_check = true;

    $demo = stm_is_site_demo_mode();

    if ($password_check and !$demo) {
        //Editing/adding user filled fields
        /*Image changing*/
        $allowed = array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF');
        if (!empty($_FILES['stm-avatar'])) {
            $file = $_FILES['stm-avatar'];
            if (is_array($file) and !empty($file['name'])) {
                $ext = pathinfo($file['name']);
                $ext = $ext['extension'];
                if (in_array($ext, $allowed)) {

                    $upload_dir = wp_upload_dir();
                    $upload_url = $upload_dir['url'];
                    $upload_path = $upload_dir['path'];


                    /*Upload full image*/
                    if (!function_exists('wp_handle_upload')) {
                        require_once(ABSPATH . 'wp-admin/includes/file.php');
                    }
                    $original_file = wp_handle_upload($file, array('test_form' => false));

                    if (!is_wp_error($original_file)) {
                        $image_user = $original_file['file'];
                        /*Crop image to square from full image*/
                        $image_cropped = image_make_intermediate_size($image_user, 160, 160, true);

                        /*Delete full image*/
                        if (file_exists($image_user)) {
                            unlink($image_user);
                        }

                        if (!$image_cropped) {
                            $got_error_validation = true;
                            $error_msg = esc_html__('Error, please try again', 'stm_vehicles_listing');

                        } else {

                            /*Get path and url of cropped image*/
                            $user_new_image_url = $upload_url . '/' . $image_cropped['file'];
                            $user_new_image_path = $upload_path . '/' . $image_cropped['file'];

                            /*Delete from site old avatar*/

                            $user_old_avatar = get_the_author_meta('stm_user_avatar_path', $user_id);
                            if (!empty($user_old_avatar) and $user_new_image_path != $user_old_avatar and file_exists($user_old_avatar)) {

                                /*Check if prev avatar exists in another users except current user*/
                                $args = array(
                                    'meta_key' => 'stm_user_avatar_path',
                                    'meta_value' => $user_old_avatar,
                                    'meta_compare' => '=',
                                    'exclude' => array($user_id),
                                );
                                $users_db = get_users($args);
                                if (empty($users_db)) {
                                    unlink($user_old_avatar);
                                }
                            }

                            /*Set new image tmp*/
                            $user['image'] = $user_new_image_url;


                            /*Update user meta path and url image*/
                            update_user_meta($user_id, 'stm_user_avatar', $user_new_image_url);
                            update_user_meta($user_id, 'stm_user_avatar_path', $user_new_image_path);

                            $response = array();
                            $response['new_avatar'] = $user_new_image_url;

                        }

                    }

                } else {
                    $got_error_validation = true;
                    $error_msg = esc_html__('Please load image with right extension (jpg, jpeg, png and gif)', 'stm_vehicles_listing');
                }
            }
        }

        /*Check if delete*/
        if (empty($_FILES['stm-avatar']['name'])) {
            if (!empty($_POST['stm_remove_img']) and $_POST['stm_remove_img'] == 'delete') {
                $user_old_avatar = get_the_author_meta('stm_user_avatar_path', $user_id);
                /*Check if prev avatar exists in another users except current user*/
                $args = array(
                    'meta_key' => 'stm_user_avatar_path',
                    'meta_value' => $user_old_avatar,
                    'meta_compare' => '=',
                    'exclude' => array($user_id),
                );
                $users_db = get_users($args);
                if (empty($users_db)) {
                    unlink($user_old_avatar);
                }
                update_user_meta($user_id, 'stm_user_avatar', '');
                update_user_meta($user_id, 'stm_user_avatar_path', '');

                $response['new_avatar'] = '';
            }
        }

        /*Change email*/
        $new_user_data = array(
            'ID' => $user_id,
            'user_email' => $user_mail
        );

        /*Change email visiblity*/
        if (!empty($_POST['stm_show_mail']) and $_POST['stm_show_mail'] == 'on') {
            update_user_meta($user_id, 'stm_show_email', 'show');
        } else {
            update_user_meta($user_id, 'stm_show_email', '');
        }

        if (!empty($_POST['stm_new_password']) and !empty($_POST['stm_new_password_confirm'])) {
            if ($_POST['stm_new_password_confirm'] == $_POST['stm_new_password']) {
                $new_user_data['user_pass'] = $_POST['stm_new_password'];
            } else {
                $got_error_validation = true;
                $error_msg = esc_html__('New password not saved, because of wrong confirmation.', 'stm_vehicles_listing');
            }
        }

        $user_error = wp_update_user($new_user_data);
        if (is_wp_error($user_error)) {
            $got_error_validation = true;
            $error_msg = $user_error->get_error_message();
        }

        /*Change fields with secondary privilegy*/
        /*POST key => user_meta_key*/
        $changed_info = array(
            'stm_first_name' => 'first_name',
            'stm_last_name' => 'last_name',
            'stm_phone' => 'stm_phone',
            'stm_user_facebook' => 'stm_user_facebook',
            'stm_user_twitter' => 'stm_user_twitter',
            'stm_user_linkedin' => 'stm_user_linkedin',
            'stm_user_youtube' => 'stm_user_youtube',
        );

        $changed_info_empty  = array(
            'stm_user_facebook' => 'stm_user_facebook',
            'stm_user_twitter' => 'stm_user_twitter',
            'stm_user_linkedin' => 'stm_user_linkedin',
            'stm_user_youtube' => 'stm_user_youtube',
        );

        foreach ($changed_info as $change_to_key => $change_info) {
            if (!empty($_POST[$change_to_key]) OR $changed_info_empty[$change_to_key]) {
                $escaped_value = sanitize_text_field($_POST[$change_to_key]);
                update_user_meta($user_id, $change_info, $escaped_value);
            }
        }

    } else {
        if ($demo) {
            $got_error_validation = true;
            $error_msg = esc_html__('Site is on demo mode', 'stm_vehicles_listing');
        }
    }

    $response['error'] = $got_error_validation;
    $response['error_msg'] = $error_msg;

    $response = json_encode($response);
    echo $response;
    exit;
}


add_filter('stm_account_navigation', function ($nav) {
    $nav['settings']['label'] = esc_html__( 'Account Settings', 'motors' );
    return $nav;
});
