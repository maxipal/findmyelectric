<?php

add_filter('stm_listings_page_options_filter', 'make_required');

function make_required($options){

    $options['required_field'] = [
        'label' => 'Is Required ?',
        'description' => 'Check if you want make this field required',
        'value' => '',
        'type' => 'checkbox'
    ];

    return $options;
}