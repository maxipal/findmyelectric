<?php


add_filter('stm_listings_build_query_args', function ($args, $source) {

    /*only publish cars aviable for searching*/
    $args['post_status'] = 'publish';

    /*adding key word searchin*/
    if (isset($_GET['s']) && $_GET['s']) {
        $args['s'] = $_GET['s'];
    }

    return $args;
}, 10, 2);

add_filter('stm_get_filter_badges', function ($filter_badges) {

    /*add search badge*/
    if ($_GET['s']) {
        $filter_badges['keyword'] = array(
            'slug' => 'keyword',
            'name' => __('Keyword', 'motors-child'),
            'value' => $_GET['s'],
            'origin' => '',
            'type' => 'text',
            'url' => remove_query_arg('s', stm_get_filter_badge_url($filter_badges))
        );
    }

    return $filter_badges;
});