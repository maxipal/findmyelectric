<?php

define( 'AUTOCHECK_LIVETIME', 3600*24*60 );
add_action('init', function () {
	if (class_exists('STM_Customizer')) {
		STM_Customizer::setSection('autocheck_settings', [
			'title' => esc_html__('AutoCheck Settings', 'motors-child'),
			'priority' => 35,
			'fields' => [
				'disable_autocheck' => [
					'label' => esc_html__('Disable AutoCheck', 'motors'),
					'type' => 'stm-checkbox',
					'default' => false
				],
				'credentials_cid' => [
					'label' => esc_html__('CID', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
				'credentials_pwd' => [
					'label' => esc_html__('Password', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
				'credentials_sid' => [
					'label' => esc_html__('SID', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
				'credentials_cid_xml' => [
					'label' => esc_html__('CID for XML', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
				'credentials_pwd_xml' => [
					'label' => esc_html__('Password for XML', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
				'credentials_sid_xml' => [
					'label' => esc_html__('SID for XML', 'motors-child'),
					'type' => 'text',
					'default' => ''
				],
			]
		]);
	}
});


function stm_auto_check_get_xml($vin_code) {
	$post_data = array();
	$post_data['id'] = get_theme_mod('credentials_cid_xml');
	$post_data['password'] = get_theme_mod('credentials_pwd_xml');
	$post_data['level'] = 'full';
	$post_data['vinlist'] = strtoupper($vin_code);
	//build the post string
	$post_string = '';
	foreach($post_data AS $key => $val){
		$post_string .= urlencode($key) . "=" . urlencode($val) . "&";
	}
	// strip off trailing ampersand
	$post_string = substr($post_string, 0, -1);
	// create a new CURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_URL, "http://www.experian.com/ais/servlets/VHRXML/");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// grab URL and pass it to the browser
	$res = curl_exec($ch);
	// close CURL resource, and free up system resources
	curl_close($ch);

	update_post_meta(get_the_ID(), 'autocheck_xml', $res);
	$vehicle = new SimpleXMLElement($res);
	return [
		'owners' => (string)$vehicle->VEHICLE->SCORE['OWNER_COUNT'][0],
		'title' => ((string)$vehicle->VEHICLE->HISTORY['ASSURED'][0] == 'N' ? __("Issue Detected", "motors-child") : __("Clean","motors-child")),
		'accidents' => (string)$vehicle->VEHICLE->HISTORY['ACCIDENT_COUNT'][0],
		'vin' => (string)$vehicle->VEHICLE['VID'][0]
	];
}

function stm_auto_check_get($car_id, $score = false) {
	$post_data = array();
	$vin_code = get_post_meta($car_id, 'vin_number', true);
	$post_data['VIN'] = strtoupper($vin_code);
	$post_data['CID'] = get_theme_mod('credentials_cid');
	$post_data['PWD'] = get_theme_mod('credentials_pwd');
	$post_data['SID'] = get_theme_mod('credentials_sid');
	if($score){
		$post_data['REPORT'] = 'R';
		$post_data['TEMPLATE'] = '1';
	}
	$post_data['LANG'] = 'en';
	//build the post string
	$post_string = '';
	foreach($post_data AS $key => $val){
		$post_string .= urlencode($key) . "=" . urlencode($val) . "&";
	}
	// strip off trailing ampersand
	$post_string = substr($post_string, 0, -1);
	// create a new CURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_URL, "https://www.autocheck.com/DealerWebLink.jsp");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// grab URL and pass it to the browser
	$res = curl_exec($ch);
	if(!empty($res)){
		if($score) {
			update_post_meta($car_id, 'auto_check_score', $res);
		}else{
			update_post_meta($car_id, 'auto_check_report', $res);
		}
	}
	// close CURL resource, and free up system resources
	curl_close($ch);
	return $res;
}

function stm_is_disable_auto_check($car_id) {
	// If car new and disabled auto check
	$meta = get_post_meta($car_id, 'auto_check_arr', true);
	if(empty($meta) && get_theme_mod('disable_autocheck')){
		return true;
	}
	$asSold = get_post_meta($car_id, 'car_mark_as_sold', true);
	// Disable on SOLD
	if (!empty($asSold)) {
		return true;
	}
	// Disable if not STANDARD OR FEATURED
	$subscription = stm_get_plan_listing($car_id);
	if(!empty($subscription['product_id'])){
		if(stm_is_standard($subscription['product_id']) || stm_is_featured($subscription['product_id'])){
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
	return false;
}

function stm_get_auto_check_arr($car_id) {
	$meta = get_post_meta($car_id, 'auto_check_arr', true);
	$vin_code = get_post_meta($car_id, 'vin_number', true);
	if(!$meta || !is_array($meta)){
		if(!empty($vin_code)){
			$meta = stm_auto_check_get_xml($vin_code);
			if(!empty($meta['owners'])){
				$meta['updated'] = time();
				update_post_meta($car_id, 'auto_check_arr', $meta);
			}
		}
	}else{
		$meta = stm_auto_check_get_xml($vin_code);
		if(!empty($meta['owners'])){
			update_post_meta($car_id, 'auto_check_arr', $meta);
		}

	}
	return $meta;
}

add_action('template_redirect', function(){
	if(isset($_REQUEST['get-report']) && !empty($_REQUEST['car-id'])){
		if(!stm_is_disable_auto_check($_REQUEST['car-id'])){
			$report = get_post_meta($_REQUEST['car-id'], 'auto_check_report', true);
			$meta = get_post_meta($_REQUEST['car-id'], 'auto_check_arr', true);
			if(!empty($report)) echo $report;
			else echo stm_auto_check_get($_REQUEST['car-id']);
			die;
		}
	}
	if(isset($_REQUEST['replace-reports'])){
		if(!is_user_admin()) return;
		$args = [
			'post_type' => 'listings',
			'posts_per_page' => -1,
		];
		$posts = get_posts($args);
		foreach ($posts as $post) {
			$check = get_post_meta($post->ID, 'auto_check_arr', true);
			if($check){
				$check['title'] = str_replace('Major Issue', 'Issue Detected', $check['title']);
				update_post_meta($post->ID, 'auto_check_arr', $check);
			}
		}
		die;
	}

	if(isset($_REQUEST['test-report'])){
		var_dump(stm_is_disable_auto_check(30972));
		die;
	}
});
