<?php

//add_action( 'wp', 'delete_unused_listing_images' );
//add_action('delete_unused_listing_images', 'delete_unused_listing_images');
//add_action( 'wp', function () {
//    if( ! wp_next_scheduled( 'delete_unused_listing_images' ) ) {
//        wp_schedule_event( time(), 'hourly', 'delete_unused_listing_images');
//    }
//});

function delete_unused_listing_images() {
    $current_attachments = get_posts(array(
        'fields' => 'ids',
        'post_type' => 'attachment',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
        'meta_key' => 'attachment_to_delete'
    ));

    if (count($current_attachments) <= 0) {
        return;
    }

    foreach ($current_attachments as $delete_attachment) {
        wp_delete_attachment($delete_attachment, true);
    }
}
// Expired status
function expired_listing() {
	$args = array(
		'post_type' => 'listings',
		'posts_per_page'  => -1,
		'post_status' => ['publish', 'draft', 'pending']
	);
    $posts = get_posts($args);
	$gmt = time();
    foreach ($posts as $posts_info) {
        $author_id = $posts_info->post_author;
        $post_id = $posts_info->ID;
		$post_time = get_post_time( $post_id );

		if (stm_is_free_plan($author_id)) {
			$order = stm_get_free_expired_date($author_id)['order'];
			$expired_timestamp = strtotime($order->post_date) + (7 * 24 * 3600);
			if ($expired_timestamp < $gmt) {
				wp_update_post(['ID' => $post_id, 'post_status' => 'pending']);
				update_post_meta($post_id, 'status', 'Expired');
//				$current_user = get_userdata($author_id);
//				var_dump($current_user->user_nicename);
//				var_dump('Expired:'.date('d.m.Y H:i:s', $expired_timestamp));
			}
		}
    }
}

// Expired cron
//add_action( 'wp', 'expired_activation' );
//function expired_activation() {
//    if( ! wp_next_scheduled( 'expired_listing' ) ) {
//        wp_schedule_event( time(), 'daily', 'expired_listing');
//    }
//}

function stm_disable_listings_without_plan() {
	$args = array(
		'post_type' => 'listings',
		'posts_per_page'  => -1,
		'post_status' => ['publish', 'draft']
	);
	$posts = get_posts($args);
	foreach ($posts as $post) {
		$sub = stm_get_plan_listing($post->ID);
		if(empty($sub['product_id'])){
			update_post_meta($post->ID, 'status', 'not_paid');
			wp_update_post(['ID' => $post->ID, 'post_status' => 'pending']);
		}else{
			var_dump($post->post_title);
			var_dump($sub);
		}
	}
	die;
}

add_action('template_redirect', function() {
	if(isset($_REQUEST['test_expire'])){
		//expired_listing();
		stm_disable_listings_without_plan();
	}
});

