<?php
add_action( 'init', 'stm_child_local_theme_setup',1000 );
function stm_child_local_theme_setup()
{
	add_image_size('stm-img-300-180', 300, 180, true);
	//add_image_size('stm-img-771-450', 771, 450, true);
	add_image_size('stm-img-1600-1200', 1600, 1200, true);
	remove_image_size('medium_large');
	remove_image_size('1536x1536');
	remove_image_size('woocommerce_thumbnail');
	remove_image_size('woocommerce_single');
	remove_image_size('woocommerce_gallery_thumbnail');
	remove_image_size('shop_catalog');
	remove_image_size('shop_single');
	remove_image_size('shop_thumbnail');
	remove_image_size('2048x2048');

	remove_image_size('stm-img-1024-768');
	remove_image_size('stm-img-1110-577');
	remove_image_size('stm-img-790-404');
	remove_image_size('stm-img-200-200');
	remove_image_size('stm-img-350-216');
	remove_image_size('stm-img-350-356');
	remove_image_size('stm-img-350-181');
	//remove_image_size('stm-img-398-206');
	remove_image_size('stm-img-398-223');
	remove_image_size('stm-img-275-205');
	remove_image_size('stm-mag-img-472-265');

	remove_image_size('stm-img-280-165');
	remove_image_size('stm-img-255-135-x-2');
	remove_image_size('stm-img-350-181');
	remove_image_size('stm-img-350-255');
	remove_image_size('stm-img-445-255');
	remove_image_size('stm-img-635-255');
	remove_image_size('stm-img-445-540');

}
//Remove Coupon Field from Checkout Page
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

add_filter( 'manage_listings_posts_columns', 'set_custom_edit_book_columns', 10, 1);

function set_custom_edit_book_columns($columns) {

    $columns['status'] = __( 'Status', 'motors-child' );
	$columns['plan'] = __( 'Plan', 'motors-child' );

    return $columns;
}

add_action( 'manage_listings_posts_custom_column' , 'custom_book_column', 10, 2 );
function custom_book_column( $column, $post_id ) {
    if($column === 'status'){
        $status = get_post_meta($post_id, 'status', true);
        echo '<strong style="text-transform: capitalize;">'. str_replace('_', ' ', $status) . '</strong>';
    }
	if($column === 'plan'){
		$subscription = stm_get_plan_listing($post_id);
		if($subscription){
			$title = $subscription['product_title'];
			$link = get_edit_post_link($subscription['product_id']);
			echo '<a href="'.$link.'" style="text-transform: capitalize;"><strong>'. $title . '</strong></a>';
		}else{
			echo '<strong style="text-transform: capitalize;">'. __("None","motors") . '</strong>';
		}
	}
}


add_action('save_post_listings', 'change_url', 10, 3);
function change_url($post_id, $response, $update)
{
    $post = get_post($post_id);

	$post_name = sanitize_title($post->post_name);
	//2020-model-s-60-3-6260
	$new_url = $post_name . '-' . $post_id;

    if (!$post->post_name) {
      return;
    }
    if (!(strpos($post->post_name, '-' . $post_id) === false)) {
      return;
    }

    $post_args = [];
    $post_args['ID'] = $post_id;
    $post_args['post_name'] = $new_url;

    wp_update_post($post_args);

}

add_action('wp_footer', function(){
	if(!function_exists('all_packages_page')) return;
	?>
    <script>
    var packages_url = '<?php echo get_home_url() . '/' . all_packages_page();?>';
    </script>
<?php });

add_action( 'wp_footer', function () {
    $all_packages_page_id =  (int) get_theme_mod('all_packages');
    $user_add_car_page =  (int) get_theme_mod('user_add_car_page');

    if (is_cart() || is_page([$all_packages_page_id, $user_add_car_page]) || is_checkout())  {
        return;
    }
    ?>
    <script>
      jQuery(function($) {
        $(document).ready(function() {
          $.cookie("listing_id", '', { path: '/'});
        })
      })
    </script>
<?php } );

add_action('wp_footer', function(){
	if(is_singular('listings')) {
		get_template_part('partials/single-car/photoswipe-layout');
	}
	if(is_page((int) get_theme_mod('user_add_car_page'))) {
	  get_template_part('partials/modals/confirmation-delete');
	}
});

/* add /blog/ base url slug to all blog posts with "Blog" template */

add_filter( 'post_link', 'blog_permalink', 10, 3 );
 function blog_permalink( $permalink, $post, $leavename) {
	$template = get_post_meta( $post->ID, '_wp_page_template', true );
		if ($template == 'single-blog.php') {
        $permalink = trailingslashit( home_url('/blog/' . $post->post_name  ) );
    }
    return $permalink;
}

add_action( 'init', 'rewrite_blog_permalink', 10);
function rewrite_blog_permalink() {
		add_rewrite_rule( '^blog/([^/]+)/?$', 'index.php?name=$matches[1]', 'top' );
}

// Sub contact form submissions with permission to MailChimp from contact form page only

function chimp_subscribe( $contact_form ) {

    $submission = WPCF7_Submission::get_instance();

	if ($submission && $contact_form->id() == '717') { //if the form has been submitted and is id 717 - contact page only

        $data = $submission->get_posted_data();

		// Handle the form data
		$email = $data['your-email'];
		$phone = $data['your-tel'];

		// BEGIN manipulating names
		$names = $data['your-name']; //pull CF7 names here

		$names_one_space = preg_replace('/\s\s+/', ' ', $names);

		$names_arr = explode(' ', $names_one_space);

			$firstName = $names_arr[0];

			if (count($names_arr) > 1){ //if user inputs more than just a first name
			// maniuplate last name
			$lastName = array_slice($names_arr, 1);
			$lastName2 = implode(" ", $lastName);

			} else {

			$lastName2 = " ";

			}
		//END manipulating names

		$mergeVars = array(
		'FNAME'=>$firstName,
		'LNAME'=>$lastName2,
		'PHONE'=>$phone
		);

		$checkbox = $data['mailchimp-checker'][0]; //get checkbox state

		//if checkbox is checked
		if ($checkbox != ""){

		include( get_stylesheet_directory() .'/inc/MailChimp.php');

		$MailChimp = new \DrewM\MailChimp\MailChimp('c17bf2146b8815783d5db9ec2e08c022-us20'); //instantiate new object with MailChimp API key

		$list_id = 'a30c69d175'; //newsletter bucket ID

		// send data to MailChimp using Drew PHP API wrapper

		$result = $MailChimp->post("lists/$list_id/members", [
				'email_address' => $email,
				'status'        => 'subscribed',
				'merge_fields' => $mergeVars,
				]);
		}

	return $contact_form;	//return form data to function

	}

}

add_action( 'wpcf7_mail_sent', 'chimp_subscribe' );


//Only load Mailchimp on specifc pages

function mailchimp_should_add_script_on_this_page($load_chimp) {

	$load_chimp = true;

	if( !is_page( array( 22, 23, 6329, 8587 )) ){

	$load_chimp = false;

	}

	return $load_chimp;
}

add_filter('mailchimp_add_inline_footer_script', 'mailchimp_should_add_script_on_this_page');

//Kill MailChimp WooCommerce script on all but specific pages

function kill_last_chimp_script() {

	if( !is_page( array( 22, 23, 6329, 8587 )) ){

		wp_dequeue_script( 'mailchimp-woocommerce' );

	}

}

add_action( 'wp_enqueue_scripts', 'kill_last_chimp_script', 98 );


//Remove Add-to-Any plugin JS on all but specific pages

function remove_add_to_any() {

    if (!is_singular('listings') && !is_page_template('single-blog.php') ) { //if not single listings page or blog post

		wp_dequeue_script( 'addtoany' );
		wp_dequeue_style( 'addtoany' );

	}

}

add_action( 'wp_enqueue_scripts', 'remove_add_to_any', 97 );


//Updated function for 2021 to remove all CF7 scripts from all pages except single listings or contact

function cf7_optimize_2021() {

    if (!is_singular('listings') && !is_page ( 8587 ) ) {

        wp_dequeue_script( 'contact-form-7' );
		wp_dequeue_script( 'google-recaptcha' );
		wp_dequeue_script( 'wpcf7-recaptcha' );

        wp_dequeue_style( 'contact-form-7' );
		wp_dequeue_style( 'cf7-confirmation-addon' );

    }

}

add_action( 'wp_enqueue_scripts', 'cf7_optimize_2021', 99 );


//function to dequeue wc cart scripts sitewide and eneuque on checkout/cart pages only

add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);

function dequeue_woocommerce_cart_fragments() {

//dequeue cart fragments for all pages
wp_dequeue_script('wc-cart-fragments');

//if is cart or checkout then enqueue script
if( is_page( array( 22, 23, 26, 6033 )) ){
                wp_enqueue_script('wc-cart-fragments');
    }
} //end function

//send when post is changed from pending to publish
//add_action( "pending_to_publish", "listing_published_email", 10, 1 );
function listing_published_email($post = null){
	//send email to user when listing is published
	if(!$post){
		global $post;
	}
	listing_published_email_callback($post);
}
function listing_published_email_callback ($post) {
	$author_id = $post->post_author;
	$postid = $post->ID;

	if ( !user_can( $author_id, 'administrator' ) ) { // if not admin

		//get author name
		$first_name = get_the_author_meta( 'first_name', $author_id );

		//get author email
		$email = get_the_author_meta( 'user_email', $author_id );

		//generate permalink to post URL
		$listing_url = get_post_permalink( $postid );

		$listing_url_minus_slash = substr($listing_url, 0, -1);

		$slash = "/";

		$full_listing_url = $listing_url_minus_slash . '-' . $postid . $slash;

		//apply first name and email to array shortcodes
		$args = array(
        'first_name' => $first_name,
        'listing_url' => $full_listing_url
      );

		//get subject from template
		$subject = generateSubjectView( 'listing_published', $args );

		//get body from template
		$body = generateTemplateView( 'listing_published', $args );

		//send mail needs to be STM for HTML
		do_action( 'stm_wp_mail', $email, $subject, $body, '' );
	}
}

add_action(  'transition_post_status',  'listing_published_email_trans', 10, 3);
function listing_published_email_trans( $new_status, $old_status, $post ) {
	// Check if we are transitioning from pending to publish
	if ( $old_status == 'pending'  &&  $new_status == 'publish' ) {
		// Check whether or not the meta_key exists already with our value
		listing_published_email_callback($post);
	}
}

add_action('butterbean_register', 'stm_listings_register_manager_child_first', 1, 2);
function stm_listings_register_manager_child_first($butterbean, $post_type) {
	$listings = stm_listings_post_type();
	if ($post_type !== $listings) return;

	// Register managers, sections, controls, and settings here.
	$butterbean->register_manager(
		'stm_car_manager',
		array(
			'label' => esc_html__('Listing manager', 'stm_vehicles_listing'),
			'post_type' => $listings,
			'context' => 'normal',
			'priority' => 'high'
		)
	);

	$manager = $butterbean->get_manager('stm_car_manager');

	//$manager->unregister_control( 'stock_number' ); // deregister
	$manager->register_control(
		'stm_city_car_admin',
		array(
			'type' => 'hidden',
			'section' => 'stm_options',
			'label' => esc_html__('City', 'stm_vehicles_listing'),
			'attr' => array('class' => 'widefat', 'id' => 'stm_city')
		)
	);
	$manager->register_setting(
		'stm_city_car_admin',
		array(
			'sanitize_callback' => 'wp_filter_nohtml_kses',
		)
	);

	$manager->register_control(
		'stm_state_car_admin',
		array(
			'type' => 'hidden',
			'section' => 'stm_options',
			'label' => esc_html__('State', 'stm_vehicles_listing'),
			'attr' => array('class' => 'widefat', 'id' => 'stm_state')
		)
	);
	$manager->register_setting(
		'stm_state_car_admin',
		array(
			'sanitize_callback' => 'wp_filter_nohtml_kses',
		)
	);
	$manager->register_control(
		'stm_state_car_two_letter_admin',
		array(
			'type' => 'hidden',
			'section' => 'stm_options',
			'label' => esc_html__('State Two letter', 'stm_vehicles_listing'),
			'attr' => array('class' => 'widefat', 'id' => 'stm_state_car_two_letter_admin')
		)
	);
	$manager->register_setting(
		'stm_state_car_two_letter_admin',
		array(
			'sanitize_callback' => 'wp_filter_nohtml_kses',
		)
	);


	$manager->register_control(
		'stm_city_car_admin',
		array(
			'type' => 'hidden',
			'section' => 'stm_options',
			'label' => esc_html__('Alternate location', 'stm_vehicles_listing'),
			'attr' => array('class' => 'widefat', 'id' => 'stm_city_car_admin')
		)
	);
	$manager->register_setting(
		'stm_city_car_admin',
		array(
			'sanitize_callback' => 'wp_filter_nohtml_kses',
		)
	);

}

add_action('template_redirect', 'stm_add_badge_sold_vehicle');
function stm_add_badge_sold_vehicle() {
	$post = false;
	if ( isset( $_GET['stm_unmark_as_sold_car'] ) ) {
		update_post_meta($_GET['stm_unmark_as_sold_car'],'badge_text','');
		delete_post_meta($_GET['stm_unmark_as_sold_car'],'special_car');
		$post = get_post($_GET['stm_unmark_as_sold_car']);
	} elseif ( isset( $_GET['stm_mark_as_sold_car'] ) ) {
		update_post_meta($_GET['stm_mark_as_sold_car'],'badge_text',__("Sold", "motors"));
		update_post_meta($_GET['stm_mark_as_sold_car'],'special_car', 'on');
		$post = get_post($_GET['stm_mark_as_sold_car']);
	}
	if(function_exists('algolia_post_to_record') && $post){
		wp_schedule_single_event( time(), 'algolia_update_post_event', array($post->ID, $post, true) );
	}
}


// Remove default counter
remove_action( 'wp', 'stm_single_car_counter' );
//add_action('wp', 'stm_single_car_counter_unique');
//function stm_single_car_counter_unique() {
//	if ( is_singular( stm_listings_post_type() ) || is_singular( 'post' ) ) {
//		stm_increase_rating(get_the_ID());
//	}
//}

function stm_custom_increase_rating( $post_id )
{
	$current_rating = intval( get_post_meta( $post_id, 'stm_custom_car_views', true ) );
	if ( empty( $current_rating ) ) {
		update_post_meta( $post_id, 'stm_custom_car_views', 1 );
	} else {
		$current_rating = $current_rating + 1;
		update_post_meta( $post_id, 'stm_custom_car_views', $current_rating );
	}
}


add_action('wp_footer', function(){
	if(is_singular('listings')){
		//Add counter for listing
		 ?>
		<script>
			jQuery.ajax({
			    type: 'GET',
			    url: ajaxurl,
			    data: 'action=stm_custom_increase_rating&id='+'<?php echo get_the_ID() ?>',
			    contentType: 'application/json;',
			    dataType: 'json',
			    cache: false,
			});
		</script>
		<?php
	}
	// Hide RECAPTCHA on pages
	if(!is_page(6315) && !is_page(6329) && !is_page(6310) && !is_page(8587)){
		?>
		<style>
			.grecaptcha-badge{
				display: none;
			}
		</style>
		<?php
	}
});

add_action('wp_ajax_stm_custom_increase_rating', 'stm_custom_increase_rating_call');
add_action('wp_ajax_nopriv_stm_custom_increase_rating', 'stm_custom_increase_rating_call');
function stm_custom_increase_rating_call() {
	$listing_id = !empty($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
	if($listing_id){
		stm_custom_increase_rating($listing_id);
	}
	die;
}


remove_action('manage_subscription_posts_columns', array('Subscriptio', 'manage_subscription_list_columns'));
add_action('manage_subscription_posts_columns', 'stm_manage_subscription_list_columns');
function stm_manage_subscription_list_columns($columns) {
	$new_columns = array();

	foreach ($columns as $column_key => $column) {
		$allowed_columns = array(
			'cb',
		);

		if (in_array($column_key, $allowed_columns)) {
			$new_columns[$column_key] = $column;
		}
	}

	$new_columns['id'] = __('ID', 'subscriptio');
	$new_columns['status'] = __('Status', 'subscriptio');
	$new_columns['subscriptio_product'] = __('Product', 'subscriptio');
	$new_columns['listing'] = __('Listing', 'subscriptio');
	$new_columns['subscriptio_user'] = __('User', 'subscriptio');
	$new_columns['recurring_amount'] = __('Recurring', 'subscriptio');
	$new_columns['last_order'] = __('Last Order', 'subscriptio');
	$new_columns['started'] = __('Started', 'subscriptio');
	$new_columns['payment_due'] = __('Payment Due', 'subscriptio');
	$new_columns['expires'] = __('Expires', 'subscriptio');

	return $new_columns;

}

add_action('manage_subscription_posts_custom_column', 'stm_manage_subscription_list_column_values', 10, 2);
function stm_manage_subscription_list_column_values($column, $post_id)
{
	$listing = stm_get_listing_by_subscription($post_id);
	switch ($column) {
	case 'listing':
		echo '<a href="'.get_edit_post_link($listing->ID).'">'.$listing->post_title.'</a>';
		break;
	default:
		break;
	}
}


add_filter('stm_listing_media_upload_size', function($file_size){
	return 1024 * 10000;
}, 100);

 //Remove company name field - note that STM has custom override in /inc/woocommerce.php

function wc_remove_checkout_fields( $fields ) {

    // Billing fields
    unset( $fields['billing']['billing_company'] );

    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'wc_remove_checkout_fields' );


//added function_exists check to this so site doesn't get critical error if CF7 plugin is disabled
if ( function_exists ( 'wpcf7_add_shortcode' ) ) {
wpcf7_add_shortcode('listing_url', 'wpcf7_listing_url_shortcode_handler', true);
}
function wpcf7_listing_url_shortcode_handler($tag) {
	if(!is_single()) return '';
	$html = '<input type="hidden" name="listing_url" value="' . get_permalink(get_the_id()) . '" />';
	return $html;
}


function stm_get_car_meta_datas($label) {
	$label_meta = get_post_meta(get_the_id(),$label['slug'],true);
	$data_meta_array = explode(',',$label_meta);
	$datas = array();

	if(!empty($data_meta_array)){
		foreach($data_meta_array as $data_meta_single) {
			$data_meta = get_term_by('slug', $data_meta_single, $label['slug']);
			if(!empty($data_meta->name)) {
				$datas[] = esc_attr($data_meta->name);
			}
		}
	}
	return $datas;
}


function stm_get_taxes_by_item($item) {
	$filter_cats = [];
	$taxonomy = str_replace(' ', '', $item);
	$taxonomies = explode(',', $taxonomy);
	if (!empty($taxonomies)) {
		foreach ($taxonomies as $categories) {
			if (!empty($categories)) {
				$filter_cats[] = explode('|', $categories);
			}
		}
	}
	return $filter_cats;
}


function get_permalink_listing($post_id = 0) {
	if(!$post_id) return '';

	$post = get_post($post_id);
	$post_name = $post->post_name;
	if(empty($post_name)) $post_name = sanitize_title($post->post_title);
	$post_type = $post->post_type;
	return get_site_url() . "/$post_type/$post_name-$post_id/";
}


add_filter('wp_mail', 'stm_skipped_mail_by_subject_key');
function stm_skipped_mail_by_subject_key($atts) {
	if($atts['subject'] == 'skipped_mail') $atts['to'] = '';
	return $atts;
}
