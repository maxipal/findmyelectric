<?php

function stm_put_log($name, $val = null, $target = 'LOG') {
	$path = get_stylesheet_directory() . "/logs/$name.log";
	if(is_array($val) || is_object($val)){
		$val = var_export($val, true);
	}
	$val = date('d.m.Y H:i:s', time()) . ' - ' . $target . ': ' . $val . "\n";
	file_put_contents($path, $val, FILE_APPEND);
}


function algolia_post_to_record(WP_Post $post) {
	$tags = array_map(
		function (WP_Term $term) {
			return $term->name;
		},
		wp_get_post_terms( $post->ID, 'post_tag' )
	);
	$feature = array_map(
		function (WP_Term $term) {
			return $term->name;
		},
		wp_get_post_terms( $post->ID, 'stm_additional_features' )
	);
	$tags = array_merge($tags, $feature);
	$img_id = get_post_meta($post->ID, '_thumbnail_id', true);
	$gallery = get_post_meta($post->ID, 'gallery',true);
	$size = 'stm-img-796-466';
	$thumbnail[] = wp_get_attachment_image_src($img_id, $size)[0];
	$gallery_photos = getPostGalleryUrls($gallery, $size);
	$car_photos = array_merge($thumbnail,$gallery_photos);
	/** @var WP_Term $ext_color */
	$ext_color = wp_get_post_terms($post->ID, 'exterior-color');
	$ext_color = reset( $ext_color );
	$ext_colors = [ $ext_color->name ];
	if ( $ext_color->parent ) {
		array_unshift( $ext_colors, get_term( $ext_color->parent )->name );
	}

	$subscription = stm_get_plan_listing($post->ID);
	$plan_free = (int)get_theme_mod('free_plan', 9819 );
	$order = 5;
	$featured = false;
	if(!empty($subscription['product_id'])){
		if($plan_free == $subscription['product_id']) $order = 10;
		if(stm_is_standard($subscription['product_id'])) $order = 20;
		if(stm_is_featured($subscription['product_id'])){
			$order = 30;
			$featured = true;
		}
	}

	$location = get_post_meta($post->ID, 'stm_city_car_admin', true);
	if(is_admin()){
		$location = get_post_meta($post->ID, 'stm_car_location', true);
		update_post_meta($post->ID, 'stm_city_car_admin', $location);
	}
	$sold = get_post_meta($post->ID, 'car_mark_as_sold', true);
//	$featured = get_post_meta($post->ID, 'special_car', true);
	if(!empty($sold) && $sold == 'on') $sold = 1;
	else $sold = 0;
//	if(!empty($featured) && $featured == 'on' && !$sold) $featured = true;
//	else $featured = false;

	$val = [
		'objectID' => implode('#', [$post->post_type, $post->ID]),
		'title' => $post->post_title,
		'author' => [
			'id' => $post->post_author,
			'name' => get_user_by( 'ID', $post->post_author )->display_name,
		],
		'excerpt' => $post->post_excerpt,
		'content' => strip_tags($post->post_content),
		'tags' => $tags,
		'url' => get_post_permalink($post->ID),
		'post_id' => $post->ID,
		'sold' => $sold,
		'featured' => $featured,
		'buy_order' => $order,
		'for_faceting' => [
			'year' =>  get_post_meta($post->ID, 'model-year', true),
			'model' => wp_get_post_terms($post->ID, 'model', array('fields' => 'names'))[0],
			'modelTrim' => wp_get_post_terms($post->ID, 'model-specific-trim', array('fields' => 'names'))[0],
			'extColor' => $ext_colors,
			'intColor' => wp_get_post_terms($post->ID, 'interior-color', array('fields' => 'names'))[0],
			'autopilotSoftware' => wp_get_post_terms($post->ID, 'autopilot-software', array('fields' => 'names'))[0],
			'autopilotHardware' => wp_get_post_terms($post->ID, 'autopilot-hardware', array('fields' => 'names'))[0],
			'battery' => wp_get_post_terms($post->ID, 'battery', array('fields' => 'names'))[0],
			'drive' => wp_get_post_terms($post->ID, 'drive', array('fields' => 'names'))[0],
			'performance' => wp_get_post_terms($post->ID, 'performance', array('fields' => 'names'))[0],
			'seating' => wp_get_post_terms($post->ID, 'seating', array('fields' => 'names'))[0],
			'dash' => wp_get_post_terms($post->ID, 'dash', array('fields' => 'names'))[0],
			'wheels' => wp_get_post_terms($post->ID, 'wheels', array('fields' => 'names'))[0],
			'sellerType' => wp_get_post_terms($post->ID, 'seller-type', array('fields' => 'names'))[0],
			'sold' => $sold,
			'modifications' => wp_get_post_terms($post->ID, 'modifications', array('fields' => 'names'))[0],
			'titleType' => wp_get_post_terms($post->ID, 'title-type', array('fields' => 'names'))[0],
		],
		'extColor' => $ext_color->name,
		'thumbnail' => wp_get_attachment_image_src($img_id, 'stm-img-796-466')[0],
		'price' => (float)get_post_meta($post->ID, 'price', true),
		'mileage' => (float)str_replace(',', '', get_post_meta($post->ID, 'mileage', true)),
		'date' => (float)get_the_date('U', $post->ID),
		'_geoloc' => [
			'lat' => (float)get_post_meta($post->ID, 'stm_lat_car_admin', true),
			'lng' => (float)get_post_meta($post->ID, 'stm_lng_car_admin', true),
		],
		'state_two' => get_post_meta($post->ID, 'stm_state_car_two_letter_admin', true), //remove if messed up
		'city_single' => $location, //remove if messed up
		'state' => get_post_meta($post->ID, 'stm_state_car_admin', true),
		'galleryUrls' => $car_photos,
		'model' => wp_get_post_terms($post->ID, 'model', array('fields' => 'names'))[0],
	];
	stm_put_log('algolia_post_to_record', $val);
	return $val;
}
add_filter('post_to_record', 'algolia_post_to_record');


add_action( 'save_post_listings', 'my_activation', 10, 3 );
function my_activation($id, WP_Post $post, $update) {
	stm_put_log('algolia_wp_schedule_single_event', $id, "WORK SCHEDULE ({$post->post_status})");
	$timeout = 5; // Timeout on upload image via front, without timeout on delete post (Fix undelete post from algolia)
	if('trash' == $post->post_status) $timeout = 0;
	$res = wp_schedule_single_event(
		time() + $timeout,
		'algolia_update_post_event',
		array($id, $post, $update)
	);
	if($res !== false) stm_put_log('algolia_wp_schedule_single_event', $id, "WORKED SCHEDULE");
	else stm_put_log('algolia_wp_schedule_single_event', $id, "CANCELLED SCHEDULE");
}

add_action( 'algolia_update_post_event','algolia_update_post', 10, 3 );
function algolia_update_post($id, WP_Post $post, $update) {

	if (wp_is_post_revision( $id) || wp_is_post_autosave( $id )) {
		return $post;
	}

	global $algolia;

	$record = (array) apply_filters('post_to_record', $post);
	stm_put_log('algolia_update_post', $record, "RECORD");

	if (! isset($record['objectID'])) {
		$record['objectID'] = implode('#', [$post->post_type, $post->ID]);
	}
	stm_put_log('algolia_update_post', $record['objectID'], "RECORD ID");

	$index = $algolia->initIndex(
		apply_filters('algolia_index_name', 'listings')
	);

//	$index->setSettings([
//		'searchableAttributes' => [
//			'sold',
//			'featured'
//		],
//		'replicas' => [
//			'listings_date_dsc',
//			'listings_featured_asc'
//		]
//	]);

	if ('trash' == $post->post_status || 'draft' == $post->post_status || 'pending' == $post->post_status) {
		$res = $index->deleteObject($record['objectID']);
		stm_put_log('algolia_delete_post', $record['objectID'], "DELETE OBJECT");
		stm_put_log('algolia_delete_post', $res, "RESPONSE");
	} else {
		$res = $index->saveObject($record);
		stm_put_log('algolia_update_post', $record['objectID'], "SAVE OBJECT");
		stm_put_log('algolia_update_post', $res, "OBJECT");
	}
	return $post;
}



function algolia_update_post_meta($meta_id, $object_id, $meta_key, $_meta_value) {
	global $algolia;
	$indexedMetaKeys = ['seo_description', 'seo_title', 'price', 'mileage', 'title' ];
	stm_put_log('algolia_update_post_meta', [$object_id, $meta_key, $_meta_value], "BEGIN");

	if (in_array($meta_key, $indexedMetaKeys)) {
		$index = $algolia->initIndex(
			apply_filters('algolia_index_name', 'listings')
		);

		$index->partialUpdateObject([
			'objectID' => 'listings#'.$object_id,
			$meta_key => $_meta_value,
		]);
		stm_put_log('algolia_update_post_meta', ['objectID' => 'listings#'.$object_id,$meta_key => $_meta_value], "partialUpdateObject");
	}
}

add_action('update_post_meta', 'algolia_update_post_meta', 10, 4);


function algolia_terms_hierarchy() {
	$taxonomies = get_object_taxonomies( 'listings', '' );
	$taxonomies = wp_filter_object_list( $taxonomies, [ 'hierarchical' => true ] );

	$terms = get_terms( [ 'taxonomy' => array_keys( $taxonomies ), 'hide_empty' => false ] );
	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	$result = [];
	foreach ( array_keys( $taxonomies ) as $taxonomy ) {
		$result[ $taxonomy ] = [];
		foreach ( _get_term_hierarchy( $taxonomy ) as $parent_id => $children_ids ) {
			$parent = $terms[ $parent_id ];
			foreach ( $children_ids as $term_id ) {
				$result[ $taxonomy ][ $terms[ $term_id ] ] = $parent;
			}
		}
	}

	return $result;
}


function algolia_reindex_listings_callback() {
	global $algolia;
	$index = $algolia->initIndex(
		apply_filters('algolia_index_name', 'listings')
	);

	$index->clearObjects()->wait();

	$count = 0;

	$posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'listings',
		'post_status' => 'publish',
	]);

	if (! $posts->have_posts()) {
		exit;
	}

	$records = [];
	foreach ($posts->posts as $post) {

		$record = (array) apply_filters('post_to_record', $post);

		if (! isset($record['objectID'])) {
			$record['objectID'] = implode('#', [$post->post_type, $post->ID]);
		}

		$records[] = $record;
		$count++;
	}

	$index->saveObjects($records);

	return $records;
}

// Delete attachments on delete post
add_action( 'before_delete_post', 'stm_delete_attach_listings', 5 );
function stm_delete_attach_listings( $post_id ){
	if(get_post_type($post_id) !== 'listings') return;
	$featId = get_post_thumbnail_id($post_id);
	$attachIds = get_post_meta($post_id, "gallery");
	if(!empty($featId)) {
		wp_delete_attachment($featId, true);
	}
	if(isset($attachIds[0])) {
		foreach ($attachIds[0] as $k => $val) {
			wp_delete_attachment($val, true);
		}
	}
}
