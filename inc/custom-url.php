<?php
//Change /author/ to /profile/ in URL string globally
add_action('init', 'custom_author_base');
/*Custom utl structure for all packages page*/
add_filter('request', 'include_all_packages_page');


function custom_author_base()
{
    global $wp_rewrite;
    $wp_rewrite->author_base = 'profile';
}

function all_packages_page(){
    $all_packages_page_id =  (int) get_theme_mod('all_packages');
    $all_packages_page_slug = get_post($all_packages_page_id)->post_name;
    //return 'add-listing/'. $all_packages_page_slug;
    return 'add-listing/packages';
}

function include_all_packages_page($query_vars)
{
    $uri_parts = [];
    $request_uri = explode('/', $_SERVER['REQUEST_URI']);

    foreach ($request_uri as $key => $value){
        if(!$value) {
            continue;
        }
        $uri_parts[] = $value;
    }

    if( count($uri_parts) <= 1  ){
        return $query_vars;
    }

    $all_packages_page_id =  (int) get_theme_mod('all_packages');
    $all_packages_page_slug = get_post($all_packages_page_id)->post_name;


    if( !empty($query_vars['pagename']) && $query_vars['pagename'] == 'add-listing/packages' ||
		!empty($query_vars['attachment']) && $query_vars['attachment'] == 'packages' ){
        $query_vars = [];
        $query_vars['pagename'] = $all_packages_page_slug;
    }


    return $query_vars;
}

add_action('init', 'custom_rewrite_additional_features');
function custom_rewrite_additional_features() {
	add_rewrite_rule('^additional_features/([^/]+)/?$', 'index.php?taxonomy=stm_additional_features&term=$matches[1]', 'top');
}

add_action('template_redirect', function(){
	if(isset($_REQUEST['taxonomy']) && $_REQUEST['taxonomy'] == 'stm_additional_features'){
		if(!empty($_REQUEST['term'])){
			wp_redirect('/additional_features/'.$_REQUEST['term']."/", 301);
		}
	}
});
