<?php

/*adding customize options*/

add_action('init', function () {
    if (class_exists('STM_Customizer')) {
        STM_Customizer::setSection('aircraft_showroom_options', array(
            'title' => esc_html__('Packages Settings', 'motors-child'),
            'priority' => 30,
            'fields' => array(
                'all_packages' => array(
                    'label' => esc_html__('All packages page', 'motors-child'),
                    'type' => 'stm-post-type',
                    'post_type' => 'page',
                    'default' => ''
                ),
				'free_to_standard' => array(
					'label' => esc_html__('From Free to Standard page', 'motors-child'),
					'type' => 'stm-post-type',
					'post_type' => 'page',
					'default' => ''
				),
				'standard_to_featured' => array(
					'label' => esc_html__('From Standard to Featured page', 'motors-child'),
					'type' => 'stm-post-type',
					'post_type' => 'page',
					'default' => ''
				),
//                'premium_plan' => array(
//	                'label' => esc_html__('Choose Premium Yearly plan', 'motors-child'),
//	                'type' => 'stm-post-type',
//	                'post_type' => 'product',
//	                'default' => ''
//                ),
//                'month_plan' => array(
//	                'label' => esc_html__('Choose Premium Monthly plan', 'motors-child'),
//	                'type' => 'stm-post-type',
//	                'post_type' => 'product',
//	                'default' => ''
//                ),
				'free_plan' => array(
					'label' => esc_html__('Choose Free plan', 'motors-child'),
					'type' => 'stm-post-type',
					'post_type' => 'product',
					'default' => ''
				),
				'single_plan' => array(
					'label' => esc_html__('Choose Single plan', 'motors-child'),
					'type' => 'stm-post-type',
					'post_type' => 'product',
					'default' => ''
				),
				'featured_plan' => array(
	                'label' => esc_html__('Choose Featured plan', 'motors-child'),
	                'type' => 'stm-post-type',
	                'post_type' => 'product',
	                'default' => ''
                ),
				'featured_plan_ex' => array(
					'label' => esc_html__('Choose Featured plan From Standard', 'motors-child'),
					'type' => 'stm-post-type',
					'post_type' => 'product',
					'default' => ''
				),
            )
        ));
	    STM_Customizer::setSection('custom_site_settings', array(
		    'title' => esc_html__('Custom Settings', 'motors-child'),
		    'priority' => 35,
		    'fields' => array(
			    'user_redirect_page' => array(
				    'label' => esc_html__('Redirect Page for non-logged users', 'stm_vehicles_listing'),
				    'type' => 'stm-post-type',
				    'post_type' => 'page',
				    'default' => ''
			    ),
				'custom_login_page' => array(
					'label' => esc_html__('Login Page', 'stm_vehicles_listing'),
					'type' => 'stm-post-type',
					'post_type' => 'page',
					'default' => ''
				),
				'custom_register_page' => array(
					'label' => esc_html__('Register page', 'stm_vehicles_listing'),
					'type' => 'stm-post-type',
					'post_type' => 'page',
					'default' => ''
				),
				'edit_listing_page' => array(
					'label' => esc_html__('Edit Listing page', 'stm_vehicles_listing'),
					'type' => 'stm-post-type',
					'post_type' => 'page',
					'default' => ''
				),
		    )
	    ));
    }
}, 1000);


