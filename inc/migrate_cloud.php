<?php

add_action('template_redirect', function(){
	if(isset($_REQUEST['soca'])){
		global $wpdb;

		$sql = "
		SELECT *
		FROM `wp_postmeta`
		WHERE CONVERT(`meta_value` USING utf8mb4) LIKE '%https://www.findmyelectric.com/wp-content/uploads/%'";

		$rows = $wpdb->get_results($sql, ARRAY_A);
		if($rows){
			foreach ($rows as $row) {
				$value = $row['meta_value'];
				preg_match_all(
					"/https:\/\/www\.findmyelectric\.com\/wp-content\/uploads\/([\/0-9A-Za-z .-]+)\?id=([0-9]+)/",
					$value,$matches);
				if(!empty($matches[0])){
					$links = $matches[0];
					$ids = $matches[2];
					foreach ($links as $key => $link) {
						$attach = get_post_meta($ids[$key], '_wp_attachment_metadata', true);
						if(!empty($attach['s3']['url'])){
							$value = str_replace($link, $attach['s3']['url'], $value);
							//var_dump($value);
						}
					}
					//$sql = $wpdb->prepare("UPDATE wp_postmeta SET meta_value=%s WHERE meta_id=%d", $value, $row['meta_id']);
					//$res = $wpdb->query($sql);
					//var_dump($res);
					//var_dump($sql);
				}
			}
		}

		// Revolution sliders
		$sql = "SELECT * FROM wp_revslider_slides";
		$rows = $wpdb->get_results($sql, ARRAY_A);
		if($rows){
			foreach ($rows as $row) {
				$value = json_decode($row['params'], ARRAY_A);

				if(!empty($value['bg']['imageId'])){
					$attach = get_post_meta($value['bg']['imageId'], '_wp_attachment_metadata', true);
					if(!empty($attach['s3']['url'])){
						$value['bg']['image'] = $attach['s3']['url'];
						//var_dump($value);
						$sql = $wpdb->prepare("UPDATE wp_revslider_slides SET params=%s WHERE id=%d", json_encode($value), $row['id']);
						$res = $wpdb->query($sql);
						var_dump($res);
					}

				}


			}
		}
		die;
	}
});
