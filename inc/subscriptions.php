<?php

$GLOBALS['wp_filter'];

add_action( "init", "delete_functions" );
function delete_functions(){
   remove_action( "subscriptio_status_changed", "stm_updateListingsStatus",100 );
}

function stm_get_plan_listing_alt($listing_id = 0) {
	global $wpdb;
	$sql = $wpdb->prepare("
		SELECT
			p.order_id,
			p.order_item_id as item_id,
			p.order_item_name as product_title,
			max( CASE WHEN pm.meta_key = '_product_id' and p.order_item_id = pm.order_item_id THEN pm.meta_value END ) as product_id,
			max( CASE WHEN pm.meta_key = 'listing_id' and p.order_item_id = pm.order_item_id THEN pm.meta_value END ) as listing_id
		FROM
			wp_woocommerce_order_items as p,
			wp_woocommerce_order_itemmeta as pm
		WHERE 
			order_item_type = 'line_item' 
			AND
			p.order_item_id = pm.order_item_id
			AND 
			p.order_item_id 
			IN (SELECT 
				order_item_id
				FROM wp_woocommerce_order_itemmeta
				WHERE meta_key='listing_id' AND meta_value=%d)
		 GROUP BY
			p.order_item_id
		ORDER BY p.order_item_id DESC
				", $listing_id);

	$res = $wpdb->get_results($sql, ARRAY_A);
	$order = [];
	if(count($res)){
		$order = $res[0];
	}

	if(!empty($order['order_id'])){
		$sql = $wpdb->prepare("
			SELECT 
				post_id as subscription_id,
				max( CASE WHEN wp_postmeta.meta_key = 'started' THEN wp_postmeta.meta_value END ) as started,
				max( CASE WHEN wp_postmeta.meta_key = 'started_readable' THEN wp_postmeta.meta_value END ) as started_readable,
				max( CASE WHEN wp_postmeta.meta_key = 'payment_due' THEN wp_postmeta.meta_value END ) as payment_due,
				max( CASE WHEN wp_postmeta.meta_key = 'payment_due_readable' THEN wp_postmeta.meta_value END ) as payment_due_readable
			FROM wp_postmeta
			WHERE post_id IN (
				SELECT 
				post_id
				FROM wp_postmeta
				WHERE meta_key='paid_by_orders' AND meta_value=15345
			)
		", $order['order_id']);

		$temp = $wpdb->get_results($sql, ARRAY_A);
		if(!empty($temp[0]['started'])){
			$order = array_merge($order, $temp[0]);
		}
	}
	return $order;
}

function stm_get_listing_by_subscription($subscription_id) {
	$subscription = new Subscriptio_Subscription($subscription_id);

	$order_ids = $subscription->all_order_ids;
	foreach ($order_ids as $order_id) {
		$order = wc_get_order($order_id);
		if(!$order) return false;
		$items = $order->get_items();
		foreach ($items as $item_id => $item) {
			$lid = (int) wc_get_order_item_meta($item_id, 'listing_id');
			if($lid) return get_post($lid);
		}
	}
}

function stm_get_plan_listing($listing_id = 0){
	if(!$listing_id) return false;
	$listing_id = (int)$listing_id;
	$plan = stm_get_plan_listing_alt($listing_id);
	if($plan){
		update_post_meta($listing_id, 'listing_plan', $plan);
		return $plan;
	}else{
		return [];
	}
	// Get all products
	$listing = get_post($listing_id);
	$author_id = $listing->post_author;
	$subscriptions = Subscriptio_User::find_subscriptions(false, $author_id);

	if($subscriptions){
		foreach ($subscriptions as $sub_id => $subscription) {
			$order_ids = $subscription->all_order_ids;
			foreach ($order_ids as $order_id) {
				$order = wc_get_order( $order_id );
				if($order){
					$items = $order->get_items();
					foreach ( $items as $item_id => $item ) {
						$lid = (int)wc_get_order_item_meta( $item_id, 'listing_id' );
						if($listing_id === $lid){
							$plan = [
								'product_title' => $item->get_name(),
								'product_id' => $item ['product_id'],
								'order_id' => $order_id,
								'item_id' => $item_id,
								'subscription_id' => $sub_id,
								'payment_due' => $subscription->payment_due,
								'payment_due_readable' => $subscription->payment_due_readable,
							];
							return $plan;
						}
					}
				}
			}

		}
	}

	return false;
}

// Save listing ID in cookie
add_action( 'wp', 'stm_redirect_to_checkout_listing' );
function stm_redirect_to_checkout_listing() {
	if ( !empty( $_GET['lid'] ) ) {
		setcookie('upgrade_listing', $_GET['lid'], time()+3600, '/');
		if(!empty( $_GET['upgrade'] )){
			setcookie('upgrade_confirm', $_GET['upgrade'], time()+3600, '/');
		}
		$subscription = stm_get_plan_listing($_GET['lid']);
		if(!empty($subscription['product_id'])){
			$s_plan_free = (int)get_theme_mod('free_plan', 9819 );
			$standard_page = (int)get_theme_mod('free_to_standard', 9819 );
			$featured_page = (int)get_theme_mod('standard_to_featured', 6043 );
			if($subscription['product_id'] == $s_plan_free){
				wp_redirect(get_permalink($standard_page));
			}
			if(stm_is_standard($subscription['product_id'])){
				wp_redirect(get_permalink($featured_page));
			}
		}
	}
}

// Add listing ID in cart meta
add_filter( 'woocommerce_add_cart_item_data', 'stm_add_listing_id_checkout', 10, 3 );
function stm_add_listing_id_checkout( $cart_item_data, $product_id, $variation_id ) {
	if( !empty( $_COOKIE['upgrade_listing'] ) ) {
		$listing = get_post((int)$_COOKIE['upgrade_listing']);
		if($listing){
			$cart_item_data['listing_id'] = $listing->ID;
			$cart_item_data['listing_title'] = $listing->post_title;
		}
	}
	return $cart_item_data;
}

// Add listing ID to order
add_action( 'woocommerce_checkout_create_order_line_item', 'stm_add_listing_id_order', 10, 4 );
function stm_add_listing_id_order( $item, $cart_item_key, $values, $order ) {
	if( isset( $values['listing_id'] ) ) {
		$item->add_meta_data( __( 'Listing ID', 'motors' ),	$values['listing_id'] );
		$item->add_meta_data( __( 'Listing Name', 'motors' ),	$values['listing_title'] );
		unset($values['listing_id']);
	}
}

// Add listing to email
//add_filter( 'woocommerce_order_item_name', 'stm_add_listing_id_order_mail', 10, 2 );
function stm_add_listing_id_order_mail( $product_name, $item ) {
	if( isset( $item['listing_title'] ) ) {
		$product_name .= sprintf(
			'<ul><li>%s: %s</li></ul>',
			__( 'Listing', 'motors' ),
			esc_html( $item['listing_title'] )
		);
	}
	return $product_name;
}

add_filter( 'woocommerce_get_item_data', 'stm_add_listing_id_cart', 10, 2 );
function stm_add_listing_id_cart( $item_data, $cart_item_data ) {
	if( isset( $cart_item_data['listing_id'] ) ) {
		$item_data[] = array(
			'key' => __( 'Listing', 'motors' ),
			'value' => wc_clean( $cart_item_data['listing_title'] ),
			'display' => ''
		);
	}
	return $item_data;
}


//user can have only one active sub
function is_user_have_premium_plan(){

	$user_id = get_current_user_id();
	$month_plan = (int)get_theme_mod('month_plan');
	$year_plan = (int)get_theme_mod('premium_plan');
	$free_plan = (int)get_theme_mod('free_plan');
	$single_plan = (int)get_theme_mod('single_plan');
	$featured_plan = (int)get_theme_mod('featured_plan');
	$featured_plan_ex = (int)get_theme_mod('featured_plan_ex');



	$plans_ids = [$month_plan, $year_plan, $single_plan, $featured_plan, $featured_plan_ex, $free_plan];
	$subscription = stm_user_active_subscriptions(false, $user_id);

	if($subscription['product_id'] && in_array($subscription['product_id'], $plans_ids)){
		return true;
	}

	return false;
}

function stm_is_free_plan($user_id = 0) {
	if(!$user_id) $user_id = get_current_user_id();
	if(!$user_id) return false;
	$free_plan = (int)get_theme_mod('free_plan');
	$current_user = wp_get_current_user();
	$customer_email = $current_user->user_email;
	if(function_exists('wc_customer_bought_product') && wc_customer_bought_product($customer_email, $user_id, $free_plan)){
		return true;
	}
	return false;
}

function stm_is_featured($product_id) {
	$product_id = (int) $product_id;
	$featured_plan = (int)get_theme_mod('featured_plan');
	$featured_plan_ex = (int)get_theme_mod('featured_plan_ex');
	if($product_id == $featured_plan || $product_id == $featured_plan_ex) return true;
	return false;
}

function stm_is_standard( $product_id ) {
	$product_id = (int) $product_id;
	$standard_plan = (int)get_theme_mod('single_plan');
	if($product_id == $standard_plan) return true;
	return false;
}

function stm_is_free( $product_id ) {
	$product_id = (int) $product_id;
	$standard_plan = (int)get_theme_mod('free_plan');
	if($product_id == $standard_plan) return true;
	return false;
}

function stm_is_expired_listing($listing_id) {
	$error = new WP_Error();
	if(!$listing_id){
		$error->add('post_id', __("Empty listing ID", "motors"));
		return $error;
	}

	$subscription_id = get_post_meta($listing_id, 'subscription_id', true);
	// User haven`t subscriptions
	if(empty($subscription_id)){
		return 0;// Get free or buy subscription
	}
	$listing = get_post($listing_id);
	if(!$listing){
		$error->add('post_not_found', __("Listing not found", "motors"));
		return $error;
	}
	$author_id = $listing->post_author;
	$active_subscriptions = stm_user_active_subscriptions(false, $author_id );
	if(!$active_subscriptions){// Haven`t subscriptions
		if(stm_is_free_plan($author_id)){// Have free subscription
			return 1;// Free Subscription
		}else{
			return 0;// Get free or buy subscription
		}
	}else{
		return 2;// have bought subscription
	}
}
function stm_get_free_expired_date($user_id = 0) {
	if(!$user_id) $user_id = get_current_user_id();
	if(!$user_id) return false;
	$free_plan = (int)get_theme_mod('free_plan');
	// GET USER ORDERS (COMPLETED + PROCESSING)
	$customer_orders = get_posts( array(
		'numberposts' => -1,
		'meta_key'    => '_customer_user',
		'meta_value'  => $user_id,
		'post_type'   => wc_get_order_types(),
		'post_status' => array_keys( wc_get_is_paid_statuses() ),
	) );
	// LOOP THROUGH ORDERS AND GET PRODUCT IDS
	if ( ! $customer_orders ) return false;
	$temp = ['product' => [], 'order' => []];
	foreach ( $customer_orders as $customer_order ) {
		$order = wc_get_order( $customer_order->ID );
		$items = $order->get_items();
		foreach ( $items as $item ) {
			if($free_plan == $item->get_product_id())
				$temp['product'] = $item;
				$temp['order'] = $customer_order;
		}
	}
	return $temp;
}

remove_action('subscriptio_status_changed', 'stm_move_draft_over_limit');

add_action('subscriptio_status_changed', 'stm_set_set_unset_listings_featured', 1000, 3);
function stm_set_set_unset_listings_featured($subscription, $old_status, $new_status){
	if ( $new_status !== 'active' ) {
		stm_change_listing_status($subscription, false);
	}else{
		stm_change_listing_status($subscription, true);
	}
}

function stm_change_listing_status($subscription, $enable = true) {
	$order_ids = $subscription->all_order_ids;
	$free_plan = (int)get_theme_mod('free_plan');
	$time = current_time('mysql');
	$user = get_user_by('id', $subscription->user_id);
	$to = $user->user_email;
	$first_name = $user->first_name;
	foreach ($order_ids as $order_id) {
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		foreach ( $items as $item_id => $item ) {
			$lid = (int)wc_get_order_item_meta( $item_id, 'listing_id' );
			$listing_title = get_the_title($lid);
			$listing_url = get_the_permalink($lid);
			$free_upgrade_link = '';
			$featured_upgrade_link = '';
			$packages_page = get_home_url() ."/".all_packages_page() . "/";
			if($free_plan == $subscription->product_id){
				$standard_page = (int)get_theme_mod('free_to_standard', 9819 );
				$free_upgrade_link = esc_url(add_query_arg(array('lid' => $lid, 'upgrade' => $lid), get_permalink($standard_page)));
			}else{
				$featured_upgrade_link = esc_url(add_query_arg(array('lid' => $lid, 'upgrade' => $lid), $packages_page));
			}

			if($lid){
				if($enable){
					$status = 'pending';
					if(!empty($_COOKIE['upgrade_confirm'])){
						$status = 'publish';
					}
					if(get_post_status($lid) !== 'publish') $listing_url = get_permalink_listing($lid);
					if($free_plan == $subscription->product_id){
						update_post_meta($lid, 'status', 'free');
						wp_update_post(['ID' => $lid, 'post_status' => $status]);
					}elseif(stm_is_featured($subscription->product_id)){
						update_post_meta($lid, 'status', 'paid');
						update_post_meta($lid, 'special_car', 'on');
						update_post_meta($lid, 'badge_text', __("FEATURED", "motors"));
						wp_update_post([
							'ID' => $lid,
							'post_status' => $status,
							'post_date'     => $time,
							'post_date_gmt' => get_gmt_from_date( $time )
						]);
					}else{
						update_post_meta($lid, 'status', 'paid');
						wp_update_post([
							'ID' => $lid,
							'post_status' => $status,
							'post_date'     => $time,
							'post_date_gmt' => get_gmt_from_date( $time )
						]);
					}
					$prev_plan = stm_get_previous_plan($subscription, $lid);
					if($prev_plan->product_name){
						$args = array (
							'first_name' => $first_name,
							'listing_title' => $listing_title,
							'listing_url' => $listing_url,
							'old_package' => $prev_plan->product_name,
							'new_package' => $subscription->product_name,
						);
						$subject = generateSubjectView('upgrade_successful', $args);
						$body = generateTemplateView('upgrade_successful', $args);
						$body = str_replace(' Listing Upgrade', '', $body);
						$body = str_replace(' Listing', '', $body);
						add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
						wp_mail($to, $subject, $body);
						remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
					}else{
						$args = array (
							'first_name' => $first_name,
							'listing_title' => $listing_title,
							'listing_url' => $listing_url,
						);
						$subject = generateSubjectView('add_car_pending', $args);
						$body = generateTemplateView('add_car_pending', $args);
						add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
						if(apply_filters('stm_listings_notify_updated', true)) {
							wp_mail($to, $subject, apply_filters('stm_listing_saved_email_body', $body));
						}
						remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');

					}
				}else{
					if(stm_is_featured($subscription->product_id)){
						update_post_meta($lid, 'special_car', '');
						delete_post_meta($lid, 'badge_text');
					}
					update_post_meta($lid, 'status', 'expired');
					wp_update_post(['ID' => $lid, 'post_status' => 'pending']);

					if(stm_is_free($subscription->product_id)) {
						$args = array (
							'first_name' => $first_name,
							'listing_title' => $listing_title,
							'free_upgrade_link' => $free_upgrade_link,
						);
						$subject = generateSubjectView('free_expired', $args);
						$body = generateTemplateView('free_expired', $args);
						add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
						wp_mail($to, $subject, $body);
						remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');

					}elseif(stm_is_standard($subscription->product_id)) {
						$args = [
							'first_name' => $first_name,
							'listing_title' => $listing_title,
							'featured_upgrade_link' => $featured_upgrade_link,
						];
						$subject = generateSubjectView('std_expired', $args);
						$body = generateTemplateView('std_expired', $args);
						add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
						wp_mail($to, $subject, $body);
						remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
					}
				}
			}
		}
	}
	return;
}

function stm_get_previous_plan($current_sub, $lid) {
	$subscriptions = Subscriptio_User::find_subscriptions(false, $current_sub->user_id);
	$last_subscription = [];
	foreach ($subscriptions as $subscription) {
		if($subscription->id == $current_sub->id) continue;
		if(!empty($subscription->renewal_all_items_meta)){
			foreach ($subscription->renewal_all_items_meta as $item) {
				$listing_id = (int)$item['item_meta']['listing_id'];
				if($listing_id == $lid){
					$last_subscription[] = $subscription;
				}
			}
		}
	}
	if(!empty($last_subscription[0])) return $last_subscription[0];
	return false;
}


function stm_set_featured_listings_status ($subscription, $enable = true) {
	$order_ids = $subscription->all_order_ids;
	if(!stm_is_featured($subscription->product_id)) return;
	foreach ($order_ids as $order_id) {
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		foreach ( $items as $item_id => $item ) {
			$lid = (int)wc_get_order_item_meta( $item_id, 'listing_id' );
			if($lid){
				if($enable){
					update_post_meta($lid, 'status', 'paid');

					update_post_meta($lid, 'special_car', 'on');
					update_post_meta($lid, 'badge_text', __("FEATURED", "motors"));

					wp_update_post(['ID' => $lid, 'post_status' => 'pending']);
				}else{
					update_post_meta($lid, 'status', 'expired');

					update_post_meta($lid, 'special_car', '');
					delete_post_meta($lid, 'badge_text');

					wp_update_post(['ID' => $lid, 'post_status' => 'pending']);
				}
			}
		}
	}
	return;
}

function stm_set_previous_listings_featured ($subscription, $enable = true) {
	$user_id = $subscription->user_id;
	$product_id = $subscription->product_id;
	if(!$user_id) return;

	$args = array(
		'post_type' => 'listings',
		'post_status' => 'any',
		'posts_per_page' => -1,
		'author' => $user_id
	);
	$featured = false;

	if(!empty($product_id)) {
		if (stm_is_featured($product_id)) {
			$featured = true;
		}
	}
	if(!$featured) $enable = false;

	$listings = get_posts($args);

	foreach ($listings as $listing) {
		if($enable){
			update_post_meta($listing->ID, 'special_car', 'on');
			update_post_meta($listing->ID, 'badge_text', __("FEATURED", "motors"));
		}else{
			update_post_meta($listing->ID, 'special_car', '');
			delete_post_meta($listing->ID, 'badge_text');
		}
		if(function_exists('algolia_post_to_record') && $listing){
			wp_schedule_single_event(time(),'algolia_update_post_event', array($listing->ID, $listing, true));
		}
	}
}

