<?php

//conditional redirect to disallow access to /packages/ and redirect to login page unless user is logged in
add_action( 'template_redirect', 'packages_redirect' );
function packages_redirect() {
	if($_SERVER['REQUEST_URI'] == "/packages/" && !is_user_logged_in()){
		$target_url = "/login/"; //https://www.findmyelectric.com/login/
		wp_redirect( $target_url );
		exit();
	}
}

//disallow access to login, login-list, and register for logged in users who are not admin
add_action( 'template_redirect', 'login_register_redirect' );
function login_register_redirect() {
	if ( is_user_logged_in() ) {
		if(!current_user_can('administrator')){
			if( is_page( array( 6315, 6310, 6329 )) ) {
				$target_url = "/profile/"; //https://www.findmyelectric.com/profile/
				wp_redirect($target_url);
				exit();
			}
		}
	}else{
		$add_car_page = (int) get_theme_mod('user_add_car_page');
		if (is_page($add_car_page)) {
			if (!is_user_logged_in()) {
				$redirect_page = (int)get_theme_mod('user_redirect_page', 1718);
				wp_redirect(get_permalink($redirect_page));
				exit();
			}
		}
	}
}

//conditional redirect for /profile/ to /profile/username/
add_action( 'template_redirect', 'profile_redirect' );
function profile_redirect() {

	global $current_user;
	if($_SERVER['REQUEST_URI'] == "/profile/" && is_user_logged_in()){
		wp_get_current_user();
		$base_url = "/profile/"; //"https://www.findmyelectric.com/profile/";
		$user_name =  $current_user->user_login;
		$last_slash = "/";
		$target_url = $base_url . $user_name . $last_slash;
		wp_redirect( $target_url );
		exit();
	}
}
