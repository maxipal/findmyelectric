<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );  ?>

<div id="tesla_pricing">
    <div class="container">
        <?php for($i = 1; $i <2; $i++):?>
            <div class="col-sm-3">
                <div class="plan-wrapper text-center">
                    <?php if($atts['pt_'. $i .'_title']):?>
                        <h4><?php esc_html_e($atts['pt_'. $i .'_title'], 'motors-child')?></h4>
                    <?php endif;?>
                    <?php if($atts['pt_'. $i .'_subtitle']):?>
                        <h5><?php esc_html_e($atts['pt_'. $i .'_subtitle'], 'motors-child')?></h5>
                    <?php endif;?>

                    <?php if($atts['pt_'. $i .'_price']):?>
                        <div class="price">
                            <?php esc_html_e($atts['pt_'. $i .'_price'], 'motors-child');?>
                        </div>
                    <?php endif;?>

                    <?php if($atts['pt_'. $i .'_features']):
                        $features = vc_param_group_parse_atts($atts['pt_'. $i .'_features']);  ?>
                        <div class="features">
                            <ul>
                                <?php foreach ($features as $key => $feature) :  ?>
                                    <li>
                                        <span class="feature_value"><?php esc_html_e($feature['pt_' . $i . '_feature_title']);?></span>
                                        <span class="feature_label"><?php esc_html_e($feature['pt_' . $i . '_feature_text']);?></span>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    <?php endif;?>
                    <?php if($atts['pt_' . $i . '_add_to_cart']):?>
                        <div class="select">
                            <button type="button" class="btn" data-product-id="<?php esc_html_e($atts['pt_' . $i . '_add_to_cart']);?>">
                                <?php esc_html_e('Select', 'motors-child');?>
                            </button>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        <?php endfor;?>

    </div>
</div>
