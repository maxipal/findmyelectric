<?php
wp_enqueue_style('stm-elfsight-css');
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$features = [];
for($i = 1; $i < 2; $i++){
	$temp = vc_param_group_parse_atts($atts['pt_'. $i .'_features']);
	foreach ($temp as $key => $f){
		$slug = sanitize_title(esc_html__($f['pt_' . $i . '_feature_text']));
		$features[$slug] = esc_html__($f['pt_' . $i . '_feature_text']);
	}
}

?>

<div class="elfsight-app eapps-pricing-table eapps-pricing-table-layout-table" id="eapps-pricing-table-1">
    <div class="eapps-pricing-table-inner">
        <div class="eapps-pricing-table-toggle-container"></div>
        <div class="eapps-pricing-table-columns-container">

            <div class="eapps-pricing-table-column-1 eapps-pricing-table-column eapps-pricing-table-column-head">
                <div class="eapps-pricing-table-column-head-mobile-inner"></div>
                <div class="eapps-pricing-table-column-inner">
                    <div class="eapps-pricing-table-column-title-container eapps-pricing-table-column-item " style="height: 30px;">
                        <div class="eapps-pricing-table-column-title">
                            <div class="eapps-pricing-table-column-title-text"><?php echo $features_title ?></div>
                        </div>
                    </div>
                    <div class="eapps-pricing-table-column-price-container eapps-pricing-table-column-item " style="height: 80px;"></div>
                    <div class="eapps-pricing-table-column-button-container eapps-pricing-table-column-item " style="height: 39px;"></div>
                    <div class="eapps-pricing-table-column-features-container eapps-pricing-table-column-item">
                        <div class="eapps-pricing-table-column-features eapps-pricing-table-column-features-undefined eapps-pricing-table-column-features-left">
                            <?php foreach($features as $feature): ?>
								<div class="eapps-pricing-table-column-features-item">
									<div class="eapps-pricing-table-column-features-item-inner">
										<span class="eapps-pricing-table-column-features-item-text"><?php echo $feature ?></span>
									</div>
								</div>
							<?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>



			<?php for($i = 1; $i <= 3; $i++):?>
				<?php
				$column = $i+1;
				if(!$atts['pt_' . $i . '_add_to_cart']) continue;
				$features_val = vc_param_group_parse_atts($atts['pt_'. $i .'_features']);
				$feature_chk = $atts['pt_' . $i . '_featured_chk'];
				$grey_btn = $atts['pt_' . $i . '_grey_btn'];
				?>

				<div class="eapps-pricing-table-column-<?php echo $column ?> eapps-pricing-table-column <?php if($feature_chk) echo 'eapps-pricing-table-column-featured' ?>">
					<div class="eapps-pricing-table-column-head-mobile-inner">
						<div class="eapps-pricing-table-column-title-container eapps-pricing-table-column-item ">
							<div class="eapps-pricing-table-column-title">
								<div class="eapps-pricing-table-column-title-text"><?php echo $features_title ?></div>
							</div>
						</div>
						<div class="eapps-pricing-table-column-price-container eapps-pricing-table-column-item "></div>
						<div class="eapps-pricing-table-column-button-container eapps-pricing-table-column-item "></div>
						<div class="eapps-pricing-table-column-features-container eapps-pricing-table-column-item">
							<div class="eapps-pricing-table-column-features eapps-pricing-table-column-features-undefined eapps-pricing-table-column-features-left">
								<?php foreach($features as $feature): ?>
									<div class="eapps-pricing-table-column-features-item">
										<div class="eapps-pricing-table-column-features-item-inner">
											<span class="eapps-pricing-table-column-features-item-text"><?php echo $feature ?></span>
										</div>
									</div>
								<?php endforeach ?>
							</div>
						</div>
					</div>

					<div class="eapps-pricing-table-column-inner">
						<div class="eapps-pricing-table-column-title-container eapps-pricing-table-column-item" style="height: 30px;">
							<div class="eapps-pricing-table-column-title">
								<div class="eapps-pricing-table-column-title-text">
									<?php esc_html_e($atts['pt_'. $i .'_title'], 'motors-child')?>
								</div>
							</div>
						</div>
						<div class="eapps-pricing-table-column-price-container eapps-pricing-table-column-item" style="height: 80px;">
							<div class="eapps-pricing-table-column-price">
								<div class="eapps-pricing-table-column-price-wrapper">
									<div class="eapps-pricing-table-column-price-text">
										<sup class="eapps-pricing-table-column-price-currency">
											$
										</sup>
										<span eapps-link="price" class="eapps-pricing-table-column-price-value">
											<?php esc_html_e($atts['pt_'. $i .'_price'], 'motors-child');?>
										</span>
									</div>
								</div>
								<div class="eapps-pricing-table-column-price-caption">
									<em><?php esc_html_e($atts['pt_'. $i .'_subtitle'], 'motors-child')?></em>
								</div>
							</div>
						</div>
						<div class="eapps-pricing-table-column-button-container eapps-pricing-table-column-item" style="height: 39px;">
							<button
								data-product-id="<?php esc_html_e($atts['pt_' . $i . '_add_to_cart']);?>"
								class="<?php if($grey_btn) echo 'grey_btn' ?> eapps-pricing-table-column-button eapps-pricing-table-column-button-undefined eapps-pricing-table-column-button-size-medium eapps-pricing-table-column-button-type-filled"
								target="_blank">
								<?php esc_html_e($atts['pt_'. $i .'_btn_caption'], 'motors-child')?>
							</button>
						</div>
						<div class="eapps-pricing-table-column-features-container eapps-pricing-table-column-item">
							<div class="eapps-pricing-table-column-features eapps-pricing-table-column-features-clean eapps-pricing-table-column-features-left">

								<?php foreach ($features_val as $key => $feature): ?>
									<div class="eapps-pricing-table-column-features-item eapps-pricing-table-column-features-item-with-icon">
										<div class="eapps-pricing-table-column-features-item-inner">
											<span class="eapps-pricing-table-column-features-item-icon-container">
												<?php if(wp_strip_all_tags($feature['pt_'.$i.'_feature_title']) == 'No'): ?>
													<img src="<?php echo get_stylesheet_directory_uri(). '/assets/images/no_elf.svg' ?>" alt="NO">
												<?php else: ?>
													<img src="<?php echo get_stylesheet_directory_uri(). '/assets/images/ok_elf.svg' ?>" alt="OK">
												<?php endif ?>
											</span>
											<span class="eapps-pricing-table-column-features-item-text"><?php echo $feature['pt_'.$i.'_feature_title'] ?></span>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>

					<?php if(!empty($atts['pt_' . $i . '_badge'])): ?>
						<div class="eapps-pricing-table-column-ribbon-container">
							<div class="eapps-pricing-table-column-ribbon">
								<?php esc_html_e($atts['pt_'. $i .'_badge'], 'motors-child')?>
							</div>
						</div>
					<?php endif ?>
				</div>
			<?php endfor;?>
        </div>
    </div>
</div>
