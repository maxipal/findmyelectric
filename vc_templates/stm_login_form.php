<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_enqueue_script('stm_recaptchav3');
wp_enqueue_script('stm-ajax');

$register_page = get_theme_mod('custom_register_page');

if (!$register_page) {
	$register_page = get_theme_mod( 'login_page', 1718);
}
?>

<div class="stm-login-register-form single-login-form<?php echo esc_attr($css_class); ?>">
	<?php if(!empty($_GET['user_id']) and !empty($_GET['hash_check'])): ?>
		<?php get_template_part('partials/user/private/password', 'recovery'); ?>
	<?php endif; ?>
	<div class="container">
		<h3><?php esc_html_e('Sign In', 'motors'); ?></h3>
		<?php if(get_theme_mod("site_demo_mode", false)): ?>
			<div style="background: #FFF; padding: 15px; margin-bottom: 15px;">
				<span style="width: 100%;">You can use these credentials for demo testing:</span>

				<div style="display: flex; flex-direction: row; margin-top: 10px;">
                    <span style="width: 40%;">
                        <b>Dealer:</b><br />
                        dealer<br />
                        dealer
                    </span>

					<span style="width: 40%;">
                        <b>User:</b><br />
                        demo<br />
                        demo
                    </span>
				</div>
			</div>
		<?php endif; ?>
		<div class="stm-login-form">
			<form method="post">
				<?php do_action( 'stm_before_signin_form' ) ?>
				<div class="form-group">
					<h4><?php esc_html_e('Username or Email', 'motors'); ?></h4>
					<input type="text" name="stm_user_login" placeholder="<?php esc_attr_e('Enter Username or Email', 'motors') ?>"/>
				</div>
				<div class="form-group">
					<h4><?php esc_html_e('Password', 'motors'); ?></h4>
					<input type="password" name="stm_user_password"  placeholder="<?php esc_attr_e('Enter Password', 'motors') ?>"/>
				</div>
				<div class="form-group form-checker">
					<label>
						<input type="checkbox" name="stm_remember_me" />
						<span><?php esc_html_e('Remember Me', 'motors'); ?></span>
					</label>
					<div class="stm-forgot-password">
						<a href="#">
							<?php esc_html_e('Forgot Password', 'motors'); ?>
						</a>
					</div>
				</div>
				<?php if(class_exists('PMXI_Plugin')) : ?><input type="hidden" name="current_lang" value="<?php echo ICL_LANGUAGE_CODE; ?>"/><?php endif; ?>

				<?php if($atts['redirect_page']):?>
					<input type="hidden" name="redirect_page" value="<?php echo esc_attr($atts['redirect_page'])?>">
				<?php endif;?>

				<input type="submit" value="<?php esc_html_e('Login', 'motors'); ?>"/>
				<span class="stm-listing-loader"><i class="stm-icon-load1"></i></span>
				<div class="stm-validation-message"></div>
				<?php do_action( 'stm_after_signin_form' ) ?>
			</form>
			<form method="post" class="stm_forgot_password_send">
				<div class="form-group">
					<h4><?php esc_html_e('Username or Email', 'motors'); ?></h4>
					<input type="hidden" name="stm_link_send_to" value="<?php echo stm_do_lmth(apply_filters('stm_get_global_server_val', "HTTP_HOST") . apply_filters('stm_get_global_server_val', "REQUEST_URI")); ?>" readonly/>
					<input type="text" name="stm_user_login" placeholder="<?php esc_attr_e('Enter Username or Email', 'motors') ?>"/>
					<input type="submit" value="<?php esc_attr_e('Send Password', 'motors'); ?>"/>
					<span class="stm-listing-loader"><i class="stm-icon-load1"></i></span>
					<div class="stm-validation-message"></div>
				</div>
			</form>
			<div style="text-align: center; margin: 15px 0; display: flex; flex-direction: column">
				<span>
					<?php esc_html_e('Don\'t  have an account?');	?>
				</span>
				<a href="<?php echo get_permalink(esc_attr($register_page))?>" style="margin-top: 5px;">
					<?php esc_html_e('Sign Up')?>
				</a>
			</div>
		</div>
		<?php if(is_listing() && defined('WORDPRESS_SOCIAL_LOGIN_ABS_PATH')): ?>
			<div class="stm-social-login-wrap">
				<?php do_action( 'wordpress_social_login' ); ?>
			</div>
		<?php endif; ?>



	</div>
</div>
