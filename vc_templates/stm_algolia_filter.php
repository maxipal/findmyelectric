<?php
wp_enqueue_script('algolia-search');
wp_enqueue_script('slide-panel');

$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);
$items = vc_param_group_parse_atts($atts['items']);
$preDefined = [];
foreach ($items as $item) {
	if(!empty($item['taxonomy'])){
		$temp = explode(' | ', $item['taxonomy']);
		$term = get_term_by('slug', $temp[0], $temp[1]);
		$preDefined[$temp[1]][] = $term->name;
	}
}
?>
<div
	class="row"
	id="app"
	v-cloak
	data-preDefined="<?php echo htmlspecialchars(json_encode($preDefined), ENT_QUOTES, 'UTF-8') ?>">
	<ais-instant-search
		index-name="listings_featured_asc"
		v-bind:search-client="searchClient"
		:search-function="preDefFunc"
		:routing="routing">

		<ais-configure
			:max-values-per-facet.camel="100"
			:get-ranking-info.camel="true"
			:around-lat-lng.camel="aroundLatLng"
			:around-radius.camel="radius">
		</ais-configure>

		<!-- DESKTOP FILTERS -->
		<div class="col-md-3 col-sm-3 sidebar-sm-mg-bt abd-full-modern-filters hidden-sm hidden-xs">
			<?php get_template_part('listings/filter/algolia/filters') ?>
		</div>
		<!-- END DESKTOP FILTERS -->

		<div class="search-panel col-md-9 col-sm-12" id="listings-result">

			<!-- FILTER ACTIONS -->
			<div class="stm-car-listing-sort-units stm-modern-filter-actions clearfix">
				<div class="stm-modern-filter-found-cars hidden-xs hidden-sm">
					<h4>
						<span class="orange">
							<ais-stats>
								<span slot-scope="{nbHits}">{{nbHits}}</span>
							</ais-stats>
						</span> Teslas Found
					</h4>

					<ais-toggle-refinement
						attribute="sold"
						on=""
						off="0"
						label="Include Sold Listings">
						<div slot-scope="{ value, refine, createURL }">
							<label>
								<div class="checker">
									<span :class="{checked: value.isRefined}">
										<input
											type="checkbox"
											@click.prevent="refine(value);" />
									</span>
								</div>
								<span>Include Sold Listings</span>
							</label>
						</div>
					</ais-toggle-refinement>

				</div>

				<!-- view by -->
				<!--<div class="stm-view-by hidden-xs hidden-sm">
					<a href="#" @click.prevent="view('grid');" class="stm-modern-view view-grid view-type ">
						<i class="stm-icon-grid"></i>
					</a>
					<a href="#" @click.prevent="view('list');" class="stm-modern-view view-list view-type active">
						<i class="stm-icon-list"></i>
					</a>
				</div>-->
				<!-- end view by -->

				<!-- sort by -->
				<div class="stm-sort-by-options clearfix">
					<span>Sort by:</span>
					<ais-sort-by
						:class-names="classNames.sortBy"
						:items="sortBy">

						<select class="no-select2" style="opacity: 1; visibility: visible;"
							slot-scope="{ items, currentRefinement, refine }"
							v-model.lazy="sortOption"
							@change="refine(sortOption);">
							<option
								v-for="item in items"
								:key="item.value"
								:value="item.value">

								<div>{{item.label}}</div>
							</option>
						</select>
					<ais-sort-by/>
				</div>
				<!-- end sort by -->
			</div>


			<!-- mobile filter controls -->
			<div id="tesla-results" class="stm-view-by visible-xs-block  sticky">
				<div class="filter-button js-cd-panel-trigger" data-panel="main" style="display: inline-block;">
					<i class="fa fa-sliders abd-modern-filter-icon-button"></i>
					<span>Filter</span>
				</div>
				<div class="stm-modern-filter-found-cars" style="display: inline-block; padding-bottom: 0;">
					<h4>
						<span class="orange">
							<ais-stats>
								<span slot-scope="{nbHits}">{{nbHits}}</span>
							</ais-stats>
						</span> Teslas Found
					</h4>
					<ais-toggle-refinement
						attribute="sold"
						on=""
						off="0"
						label="Include Sold Listings">
						<div slot-scope="{ value, refine, createURL }">
							<label>
								<div class="checker">
									<span :class="{checked: value.isRefined}">
										<input
											type="checkbox"
											@click.prevent="refine(value);" />
									</span>
								</div>
								<span>Include Sold Listings</span>
							</label>
						</div>
					</ais-toggle-refinement>
				</div>
			</div>

			<div class="stm-view-by visible-xs-block visible-sm-block sticky hidden">
				<div class="filter-button js-cd-panel-trigger" data-panel="main" style="display: inline-block;">
					<i class="fa fa-sliders abd-modern-filter-icon-button"></i>
					<span>Filter</span>
				</div>
				<div class="stm-modern-filter-found-cars" style="display: inline-block; padding-bottom: 0;">
					<h4>
						<span class="orange">
							<ais-stats>
								<span slot-scope="{nbHits}">{{nbHits}}</span>
							</ais-stats>
						</span> Teslas Found
					</h4>

					<ais-toggle-refinement
						attribute="sold"
						on=""
						off="0"
						label="Include Sold Listings">
						<div slot-scope="{ value, refine, createURL }">
							<label>
								<div class="checker">
									<span :class="{checked: value.isRefined}">
										<input
											type="checkbox"
											@click.prevent="refine(value);" />
									</span>
								</div>
								<span>Include Sold Listings</span>
							</label>
						</div>
					</ais-toggle-refinement>
				</div>
			</div>


			<!-- mobile filters -->
			<div class="cd-panel cd-panel--from-left js-cd-panel-main" style="z-index: 999;">
				<header class="cd-panel__header" style="display: flex;">
					<h5 class="found-teslas js-cd-close" style="padding: 15px 20px 15px 15px;">
						<i class="fa fa-chevron-left js-cd-close" aria-hidden="true"></i>
						<ais-stats>
							<span class="js-cd-close" slot-scope="{nbHits}">{{nbHits}}</span>
						</ais-stats>
						<span class="js-cd-close">
							Teslas
						</span>
					</h5>
					<ais-clear-refinements
						:excluded-attributes="excludedAtts">
						<div @click.prevent="refine"
								 class="ais-CurrentRefinements-list"
								 slot-scope="{ canRefine, refine, createURL }"
						>
							<span @click="clearRefs();" class="reset-filters ">
								Reset Filters
							</span>
						</div>
					</ais-clear-refinements>
					<div class="results-button js-cd-close">
						<span @click="onPageChange()" class="js-cd-close" style="line-height: 40px;">Show results</span>
					</div>
				</header>
				<div class="cd-panel__container">
					<div id="filter-menu" class="cd-panel__content col-md-3 col-sm-12 sidebar-sm-mg-bt abd-mobile-modern-filters" style="display: block !important;">
						<?php get_template_part('listings/filter/algolia/filters') ?>
					</div>
				</div>
			</div>
			<!-- end mobile filters -->


			<!-- end mobile filter controls -->
				<div class="modern-filter-badges" style="padding-top: 10px;">
					<ais-clear-refinements
						:excluded-attributes="excludedAtts">
						<ul
							class="ais-CurrentRefinements-list stm-filter-chosen-units-list"
							slot-scope="{ canRefine, refine, createURL }">
							<li class="ais-CurrentRefinements-item"
								@click.prevent="refine"
								v-if="canRefine"
								v-show="canClearAll"
								style="margin-bottom: 20px;"
								>
								<span @click="clearRefs();" class="ais-CurrentRefinements-category">
                  					Clear All
								</span>
							</li>
						</ul>
					</ais-clear-refinements>

					<ais-current-refinements
						:excluded-attributes="excludedAtts"
						:transform-items="displayRefinements">
						<template slot="item" slot-scope="{ item, refine, createURL }">
							<div style="display: flex; margin: 0 0 5px 0;">
								<div v-for="refinement in item.refinements"
									:key="[
										refinement.attribute,
										refinement.type,
										refinement.value,
										refinement.operator
									].join(':')"
									>

									<div v-if="refinement.attribute !== 'price' && refinement.attribute !== 'sold'"
										:class="'stm-filter-chosen-units-list'">
										{{ formLabel(refinement) }}
										<i
										@click.prevent="refine(refinement); removeActiveItem(refinement)"
										class="fa fa-close stm-clear-listing-one-unit"></i>
										<input type="hidden" name="refs" :value="refinement.value"/>
									</div>

									<div v-else-if="refinement.attribute !== 'sold'">
										<div v-if="refinement.attribute === 'price' && refinement.operator == '>='"
											v-show="showMinPrice"
											:class="'stm-filter-chosen-units-list price-badge'">
											{{ formLabel(refinement) }}
											<i
												@click.prevent="refine(refinement); removeActiveItem(refinement)"
												class="fa fa-close stm-clear-listing-one-unit"></i>
											<input type="hidden" name="refs" :value="refinement.value"/>
										</div>
										<div v-else
											v-show="showMaxPrice"
											:class="'stm-filter-chosen-units-list price-badge'">
												{{ formLabel(refinement) }}
												<i
													@click.prevent="refine(refinement); removeActiveItem(refinement)"
													class="fa fa-close stm-clear-listing-one-unit"></i>
												<input type="hidden" name="refs" :value="refinement.value"/>
										</div>

									</div>
								</div>
							</div>
						</template>
					</ais-current-refinements>
				</div>
			<!-- end current refinements(badges) -->


			<!-- END FILTER ACTIONS -->

			<!-- HITS -->
			<div v-if="viewType=='grid'">
				<?php get_template_part('listings/filter/algolia/algolia-grid-view') ?>
			</div>
			<div v-else>
				<?php get_template_part('listings/filter/algolia/algolia-list-view') ?>
			</div>
			<!-- END HITS -->
		<?php get_template_part('listings/filter/algolia/algolia-pagination') ?>
		</div>

	</ais-instant-search>
</div>
