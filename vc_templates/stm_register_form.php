<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$link = $atts['link'];

if(!empty($link)) {
	$link = vc_build_link( $link );
}

wp_enqueue_script('stm_recaptchav3');

$login_page = get_theme_mod('custom_login_page');
if (!$login_page) {
	$login_page = get_theme_mod( 'login_page', 1718);
}
?>
<div class="stm-login-register-form <?php echo esc_attr($css_class); ?>">
	<div class="container">
		<h3><?php esc_html_e('Sign Up', 'motors'); ?></h3>
		<div class="stm-register-form">
			<form method="post">
				<?php do_action( 'stm_before_signup_form' ) ?>
				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('First Name *', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_user_first_name" placeholder="<?php esc_attr_e('Enter First Name', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Last Name *', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_user_last_name" placeholder="<?php esc_attr_e('Enter Last Name', 'motors') ?>"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('Phone Number *', 'motors'); ?></h4>
						<input class="user_validated_field login-number" type="tel" name="stm_user_phone" placeholder="<?php esc_attr_e('Enter Phone', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Email *', 'motors'); ?></h4>
						<input class="user_validated_field" type="email" name="stm_user_mail" placeholder="<?php esc_attr_e('Enter Email', 'motors') ?>"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('Username *', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_nickname" placeholder="<?php esc_attr_e('Enter Username', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Password *', 'motors'); ?></h4>
						<div class="stm-show-password">
							<i class="fa fa-eye-slash"></i>
							<input id="psw" class="user_validated_field" type="password" name="stm_user_password"  placeholder="<?php esc_attr_e('Enter Password', 'motors') ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"/>
                            <div id="message">
                                <h3>Password must contain the following:</h3>
                                <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                                <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                                <p id="number" class="invalid">A <b>number</b></p>
                                <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                            </div>
						</div>
					</div>
				</div>

				<div class="form-group form-checker" style="display: flex; justify-content: space-between">
					<label class="stm_accept_terms">
						<input type="checkbox" name="stm_accept_terms" />
						<span>
              <?php esc_html_e('I agree to the', 'motors'); ?>
							<?php if(!empty($link) and !empty($link['url'])): ?>
								<a href="<?php echo esc_url($link['url']); ?>" target="_blank"><?php esc_html_e($link['title'], 'motors') ?></a>
							<?php endif; ?>
							</span>
					</label>
                    <div style="font-size: 15px;" class="have_account">
										<?php esc_html_e('Already have an account?'); ?>
																<a href="<?php echo get_permalink(esc_attr($login_page))?>">
											<?php esc_html_e('Login')?>
                        </a>
                    </div>
				</div>

				<div class="form-group form-group-submit clearfix">
					<input id="change-pas-btn"  type="submit" value="<?php esc_html_e('Sign up now!', 'motors'); ?>" disabled/>
                    <?php if($atts['redirect_page']):?>
                        <input type="hidden" name="redirect_page" value="<?php echo esc_attr($atts['redirect_page'])?>">
                    <?php endif;?>
					<span class="stm-listing-loader"><i class="stm-icon-load1"></i></span>
				</div>

				<div class="stm-validation-message"></div>
				<?php do_action( 'stm_after_signup_form' ) ?>

			</form>
		</div>
	</div>
</div>
